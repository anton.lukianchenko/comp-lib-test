import { registerRoot } from "remotion";
import { RemotionVideo } from "./Video";

export { registerRoot };
export default RemotionVideo;
