import React from "react";
export declare const Arc: React.FC<{
    progress: number;
    rotation: number;
    rotateProgress: number;
}>;
