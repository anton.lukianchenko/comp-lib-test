var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

var dist = {};

var multipleVersionsWarning = {};

Object.defineProperty(multipleVersionsWarning, "__esModule", { value: true });
multipleVersionsWarning.checkMultipleRemotionVersions = void 0;
const checkMultipleRemotionVersions = () => {
    if (typeof window === 'undefined') {
        return;
    }
    if (window.remotion_imported) {
        console.error('🚨 Multiple versions of Remotion detected. Multiple versions will cause conflicting React contexts and things may break in an unexpected way. Please check your dependency tree and make sure only one version of Remotion is on the page.');
    }
    window.remotion_imported = true;
};
multipleVersionsWarning.checkMultipleRemotionVersions = checkMultipleRemotionVersions;

var AbsoluteFill$1 = {};

var jsxRuntime = {exports: {}};

var reactJsxRuntime_production_min = {};

/*
object-assign
(c) Sindre Sorhus
@license MIT
*/
/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

var objectAssign = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};

var react = {exports: {}};

var react_production_min = {};

/** @license React v17.0.2
 * react.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var l=objectAssign,n$1=60103,p$1=60106;react_production_min.Fragment=60107;react_production_min.StrictMode=60108;react_production_min.Profiler=60114;var q$1=60109,r=60110,t=60112;react_production_min.Suspense=60113;var u=60115,v=60116;
if("function"===typeof Symbol&&Symbol.for){var w=Symbol.for;n$1=w("react.element");p$1=w("react.portal");react_production_min.Fragment=w("react.fragment");react_production_min.StrictMode=w("react.strict_mode");react_production_min.Profiler=w("react.profiler");q$1=w("react.provider");r=w("react.context");t=w("react.forward_ref");react_production_min.Suspense=w("react.suspense");u=w("react.memo");v=w("react.lazy");}var x="function"===typeof Symbol&&Symbol.iterator;
function y(a){if(null===a||"object"!==typeof a)return null;a=x&&a[x]||a["@@iterator"];return "function"===typeof a?a:null}function z(a){for(var b="https://reactjs.org/docs/error-decoder.html?invariant="+a,c=1;c<arguments.length;c++)b+="&args[]="+encodeURIComponent(arguments[c]);return "Minified React error #"+a+"; visit "+b+" for the full message or use the non-minified dev environment for full errors and additional helpful warnings."}
var A={isMounted:function(){return !1},enqueueForceUpdate:function(){},enqueueReplaceState:function(){},enqueueSetState:function(){}},B={};function C(a,b,c){this.props=a;this.context=b;this.refs=B;this.updater=c||A;}C.prototype.isReactComponent={};C.prototype.setState=function(a,b){if("object"!==typeof a&&"function"!==typeof a&&null!=a)throw Error(z(85));this.updater.enqueueSetState(this,a,b,"setState");};C.prototype.forceUpdate=function(a){this.updater.enqueueForceUpdate(this,a,"forceUpdate");};
function D(){}D.prototype=C.prototype;function E(a,b,c){this.props=a;this.context=b;this.refs=B;this.updater=c||A;}var F=E.prototype=new D;F.constructor=E;l(F,C.prototype);F.isPureReactComponent=!0;var G={current:null},H=Object.prototype.hasOwnProperty,I={key:!0,ref:!0,__self:!0,__source:!0};
function J(a,b,c){var e,d={},k=null,h=null;if(null!=b)for(e in void 0!==b.ref&&(h=b.ref),void 0!==b.key&&(k=""+b.key),b)H.call(b,e)&&!I.hasOwnProperty(e)&&(d[e]=b[e]);var g=arguments.length-2;if(1===g)d.children=c;else if(1<g){for(var f=Array(g),m=0;m<g;m++)f[m]=arguments[m+2];d.children=f;}if(a&&a.defaultProps)for(e in g=a.defaultProps,g)void 0===d[e]&&(d[e]=g[e]);return {$$typeof:n$1,type:a,key:k,ref:h,props:d,_owner:G.current}}
function K(a,b){return {$$typeof:n$1,type:a.type,key:b,ref:a.ref,props:a.props,_owner:a._owner}}function L(a){return "object"===typeof a&&null!==a&&a.$$typeof===n$1}function escape(a){var b={"=":"=0",":":"=2"};return "$"+a.replace(/[=:]/g,function(a){return b[a]})}var M=/\/+/g;function N(a,b){return "object"===typeof a&&null!==a&&null!=a.key?escape(""+a.key):b.toString(36)}
function O(a,b,c,e,d){var k=typeof a;if("undefined"===k||"boolean"===k)a=null;var h=!1;if(null===a)h=!0;else switch(k){case "string":case "number":h=!0;break;case "object":switch(a.$$typeof){case n$1:case p$1:h=!0;}}if(h)return h=a,d=d(h),a=""===e?"."+N(h,0):e,Array.isArray(d)?(c="",null!=a&&(c=a.replace(M,"$&/")+"/"),O(d,b,c,"",function(a){return a})):null!=d&&(L(d)&&(d=K(d,c+(!d.key||h&&h.key===d.key?"":(""+d.key).replace(M,"$&/")+"/")+a)),b.push(d)),1;h=0;e=""===e?".":e+":";if(Array.isArray(a))for(var g=
0;g<a.length;g++){k=a[g];var f=e+N(k,g);h+=O(k,b,c,f,d);}else if(f=y(a),"function"===typeof f)for(a=f.call(a),g=0;!(k=a.next()).done;)k=k.value,f=e+N(k,g++),h+=O(k,b,c,f,d);else if("object"===k)throw b=""+a,Error(z(31,"[object Object]"===b?"object with keys {"+Object.keys(a).join(", ")+"}":b));return h}function P(a,b,c){if(null==a)return a;var e=[],d=0;O(a,e,"","",function(a){return b.call(c,a,d++)});return e}
function Q(a){if(-1===a._status){var b=a._result;b=b();a._status=0;a._result=b;b.then(function(b){0===a._status&&(b=b.default,a._status=1,a._result=b);},function(b){0===a._status&&(a._status=2,a._result=b);});}if(1===a._status)return a._result;throw a._result;}var R={current:null};function S(){var a=R.current;if(null===a)throw Error(z(321));return a}var T={ReactCurrentDispatcher:R,ReactCurrentBatchConfig:{transition:0},ReactCurrentOwner:G,IsSomeRendererActing:{current:!1},assign:l};
react_production_min.Children={map:P,forEach:function(a,b,c){P(a,function(){b.apply(this,arguments);},c);},count:function(a){var b=0;P(a,function(){b++;});return b},toArray:function(a){return P(a,function(a){return a})||[]},only:function(a){if(!L(a))throw Error(z(143));return a}};react_production_min.Component=C;react_production_min.PureComponent=E;react_production_min.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED=T;
react_production_min.cloneElement=function(a,b,c){if(null===a||void 0===a)throw Error(z(267,a));var e=l({},a.props),d=a.key,k=a.ref,h=a._owner;if(null!=b){void 0!==b.ref&&(k=b.ref,h=G.current);void 0!==b.key&&(d=""+b.key);if(a.type&&a.type.defaultProps)var g=a.type.defaultProps;for(f in b)H.call(b,f)&&!I.hasOwnProperty(f)&&(e[f]=void 0===b[f]&&void 0!==g?g[f]:b[f]);}var f=arguments.length-2;if(1===f)e.children=c;else if(1<f){g=Array(f);for(var m=0;m<f;m++)g[m]=arguments[m+2];e.children=g;}return {$$typeof:n$1,type:a.type,
key:d,ref:k,props:e,_owner:h}};react_production_min.createContext=function(a,b){void 0===b&&(b=null);a={$$typeof:r,_calculateChangedBits:b,_currentValue:a,_currentValue2:a,_threadCount:0,Provider:null,Consumer:null};a.Provider={$$typeof:q$1,_context:a};return a.Consumer=a};react_production_min.createElement=J;react_production_min.createFactory=function(a){var b=J.bind(null,a);b.type=a;return b};react_production_min.createRef=function(){return {current:null}};react_production_min.forwardRef=function(a){return {$$typeof:t,render:a}};react_production_min.isValidElement=L;
react_production_min.lazy=function(a){return {$$typeof:v,_payload:{_status:-1,_result:a},_init:Q}};react_production_min.memo=function(a,b){return {$$typeof:u,type:a,compare:void 0===b?null:b}};react_production_min.useCallback=function(a,b){return S().useCallback(a,b)};react_production_min.useContext=function(a,b){return S().useContext(a,b)};react_production_min.useDebugValue=function(){};react_production_min.useEffect=function(a,b){return S().useEffect(a,b)};react_production_min.useImperativeHandle=function(a,b,c){return S().useImperativeHandle(a,b,c)};
react_production_min.useLayoutEffect=function(a,b){return S().useLayoutEffect(a,b)};react_production_min.useMemo=function(a,b){return S().useMemo(a,b)};react_production_min.useReducer=function(a,b,c){return S().useReducer(a,b,c)};react_production_min.useRef=function(a){return S().useRef(a)};react_production_min.useState=function(a){return S().useState(a)};react_production_min.version="17.0.2";

var react_development = {};

/** @license React v17.0.2
 * react.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

(function (exports) {

if (process.env.NODE_ENV !== "production") {
  (function() {

var _assign = objectAssign;

// TODO: this is special because it gets imported during build.
var ReactVersion = '17.0.2';

// ATTENTION
// When adding new symbols to this file,
// Please consider also adding to 'react-devtools-shared/src/backend/ReactSymbols'
// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var REACT_ELEMENT_TYPE = 0xeac7;
var REACT_PORTAL_TYPE = 0xeaca;
exports.Fragment = 0xeacb;
exports.StrictMode = 0xeacc;
exports.Profiler = 0xead2;
var REACT_PROVIDER_TYPE = 0xeacd;
var REACT_CONTEXT_TYPE = 0xeace;
var REACT_FORWARD_REF_TYPE = 0xead0;
exports.Suspense = 0xead1;
var REACT_SUSPENSE_LIST_TYPE = 0xead8;
var REACT_MEMO_TYPE = 0xead3;
var REACT_LAZY_TYPE = 0xead4;
var REACT_BLOCK_TYPE = 0xead9;
var REACT_SERVER_BLOCK_TYPE = 0xeada;
var REACT_FUNDAMENTAL_TYPE = 0xead5;
var REACT_DEBUG_TRACING_MODE_TYPE = 0xeae1;
var REACT_LEGACY_HIDDEN_TYPE = 0xeae3;

if (typeof Symbol === 'function' && Symbol.for) {
  var symbolFor = Symbol.for;
  REACT_ELEMENT_TYPE = symbolFor('react.element');
  REACT_PORTAL_TYPE = symbolFor('react.portal');
  exports.Fragment = symbolFor('react.fragment');
  exports.StrictMode = symbolFor('react.strict_mode');
  exports.Profiler = symbolFor('react.profiler');
  REACT_PROVIDER_TYPE = symbolFor('react.provider');
  REACT_CONTEXT_TYPE = symbolFor('react.context');
  REACT_FORWARD_REF_TYPE = symbolFor('react.forward_ref');
  exports.Suspense = symbolFor('react.suspense');
  REACT_SUSPENSE_LIST_TYPE = symbolFor('react.suspense_list');
  REACT_MEMO_TYPE = symbolFor('react.memo');
  REACT_LAZY_TYPE = symbolFor('react.lazy');
  REACT_BLOCK_TYPE = symbolFor('react.block');
  REACT_SERVER_BLOCK_TYPE = symbolFor('react.server.block');
  REACT_FUNDAMENTAL_TYPE = symbolFor('react.fundamental');
  symbolFor('react.scope');
  symbolFor('react.opaque.id');
  REACT_DEBUG_TRACING_MODE_TYPE = symbolFor('react.debug_trace_mode');
  symbolFor('react.offscreen');
  REACT_LEGACY_HIDDEN_TYPE = symbolFor('react.legacy_hidden');
}

var MAYBE_ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
var FAUX_ITERATOR_SYMBOL = '@@iterator';
function getIteratorFn(maybeIterable) {
  if (maybeIterable === null || typeof maybeIterable !== 'object') {
    return null;
  }

  var maybeIterator = MAYBE_ITERATOR_SYMBOL && maybeIterable[MAYBE_ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL];

  if (typeof maybeIterator === 'function') {
    return maybeIterator;
  }

  return null;
}

/**
 * Keeps track of the current dispatcher.
 */
var ReactCurrentDispatcher = {
  /**
   * @internal
   * @type {ReactComponent}
   */
  current: null
};

/**
 * Keeps track of the current batch's configuration such as how long an update
 * should suspend for if it needs to.
 */
var ReactCurrentBatchConfig = {
  transition: 0
};

/**
 * Keeps track of the current owner.
 *
 * The current owner is the component who should own any components that are
 * currently being constructed.
 */
var ReactCurrentOwner = {
  /**
   * @internal
   * @type {ReactComponent}
   */
  current: null
};

var ReactDebugCurrentFrame = {};
var currentExtraStackFrame = null;
function setExtraStackFrame(stack) {
  {
    currentExtraStackFrame = stack;
  }
}

{
  ReactDebugCurrentFrame.setExtraStackFrame = function (stack) {
    {
      currentExtraStackFrame = stack;
    }
  }; // Stack implementation injected by the current renderer.


  ReactDebugCurrentFrame.getCurrentStack = null;

  ReactDebugCurrentFrame.getStackAddendum = function () {
    var stack = ''; // Add an extra top frame while an element is being validated

    if (currentExtraStackFrame) {
      stack += currentExtraStackFrame;
    } // Delegate to the injected renderer-specific implementation


    var impl = ReactDebugCurrentFrame.getCurrentStack;

    if (impl) {
      stack += impl() || '';
    }

    return stack;
  };
}

/**
 * Used by act() to track whether you're inside an act() scope.
 */
var IsSomeRendererActing = {
  current: false
};

var ReactSharedInternals = {
  ReactCurrentDispatcher: ReactCurrentDispatcher,
  ReactCurrentBatchConfig: ReactCurrentBatchConfig,
  ReactCurrentOwner: ReactCurrentOwner,
  IsSomeRendererActing: IsSomeRendererActing,
  // Used by renderers to avoid bundling object-assign twice in UMD bundles:
  assign: _assign
};

{
  ReactSharedInternals.ReactDebugCurrentFrame = ReactDebugCurrentFrame;
}

// by calls to these methods by a Babel plugin.
//
// In PROD (or in packages without access to React internals),
// they are left as they are instead.

function warn(format) {
  {
    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    printWarning('warn', format, args);
  }
}
function error(format) {
  {
    for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    printWarning('error', format, args);
  }
}

function printWarning(level, format, args) {
  // When changing this logic, you might want to also
  // update consoleWithStackDev.www.js as well.
  {
    var ReactDebugCurrentFrame = ReactSharedInternals.ReactDebugCurrentFrame;
    var stack = ReactDebugCurrentFrame.getStackAddendum();

    if (stack !== '') {
      format += '%s';
      args = args.concat([stack]);
    }

    var argsWithFormat = args.map(function (item) {
      return '' + item;
    }); // Careful: RN currently depends on this prefix

    argsWithFormat.unshift('Warning: ' + format); // We intentionally don't use spread (or .apply) directly because it
    // breaks IE9: https://github.com/facebook/react/issues/13610
    // eslint-disable-next-line react-internal/no-production-logging

    Function.prototype.apply.call(console[level], console, argsWithFormat);
  }
}

var didWarnStateUpdateForUnmountedComponent = {};

function warnNoop(publicInstance, callerName) {
  {
    var _constructor = publicInstance.constructor;
    var componentName = _constructor && (_constructor.displayName || _constructor.name) || 'ReactClass';
    var warningKey = componentName + "." + callerName;

    if (didWarnStateUpdateForUnmountedComponent[warningKey]) {
      return;
    }

    error("Can't call %s on a component that is not yet mounted. " + 'This is a no-op, but it might indicate a bug in your application. ' + 'Instead, assign to `this.state` directly or define a `state = {};` ' + 'class property with the desired state in the %s component.', callerName, componentName);

    didWarnStateUpdateForUnmountedComponent[warningKey] = true;
  }
}
/**
 * This is the abstract API for an update queue.
 */


var ReactNoopUpdateQueue = {
  /**
   * Checks whether or not this composite component is mounted.
   * @param {ReactClass} publicInstance The instance we want to test.
   * @return {boolean} True if mounted, false otherwise.
   * @protected
   * @final
   */
  isMounted: function (publicInstance) {
    return false;
  },

  /**
   * Forces an update. This should only be invoked when it is known with
   * certainty that we are **not** in a DOM transaction.
   *
   * You may want to call this when you know that some deeper aspect of the
   * component's state has changed but `setState` was not called.
   *
   * This will not invoke `shouldComponentUpdate`, but it will invoke
   * `componentWillUpdate` and `componentDidUpdate`.
   *
   * @param {ReactClass} publicInstance The instance that should rerender.
   * @param {?function} callback Called after component is updated.
   * @param {?string} callerName name of the calling function in the public API.
   * @internal
   */
  enqueueForceUpdate: function (publicInstance, callback, callerName) {
    warnNoop(publicInstance, 'forceUpdate');
  },

  /**
   * Replaces all of the state. Always use this or `setState` to mutate state.
   * You should treat `this.state` as immutable.
   *
   * There is no guarantee that `this.state` will be immediately updated, so
   * accessing `this.state` after calling this method may return the old value.
   *
   * @param {ReactClass} publicInstance The instance that should rerender.
   * @param {object} completeState Next state.
   * @param {?function} callback Called after component is updated.
   * @param {?string} callerName name of the calling function in the public API.
   * @internal
   */
  enqueueReplaceState: function (publicInstance, completeState, callback, callerName) {
    warnNoop(publicInstance, 'replaceState');
  },

  /**
   * Sets a subset of the state. This only exists because _pendingState is
   * internal. This provides a merging strategy that is not available to deep
   * properties which is confusing. TODO: Expose pendingState or don't use it
   * during the merge.
   *
   * @param {ReactClass} publicInstance The instance that should rerender.
   * @param {object} partialState Next partial state to be merged with state.
   * @param {?function} callback Called after component is updated.
   * @param {?string} Name of the calling function in the public API.
   * @internal
   */
  enqueueSetState: function (publicInstance, partialState, callback, callerName) {
    warnNoop(publicInstance, 'setState');
  }
};

var emptyObject = {};

{
  Object.freeze(emptyObject);
}
/**
 * Base class helpers for the updating state of a component.
 */


function Component(props, context, updater) {
  this.props = props;
  this.context = context; // If a component has string refs, we will assign a different object later.

  this.refs = emptyObject; // We initialize the default updater but the real one gets injected by the
  // renderer.

  this.updater = updater || ReactNoopUpdateQueue;
}

Component.prototype.isReactComponent = {};
/**
 * Sets a subset of the state. Always use this to mutate
 * state. You should treat `this.state` as immutable.
 *
 * There is no guarantee that `this.state` will be immediately updated, so
 * accessing `this.state` after calling this method may return the old value.
 *
 * There is no guarantee that calls to `setState` will run synchronously,
 * as they may eventually be batched together.  You can provide an optional
 * callback that will be executed when the call to setState is actually
 * completed.
 *
 * When a function is provided to setState, it will be called at some point in
 * the future (not synchronously). It will be called with the up to date
 * component arguments (state, props, context). These values can be different
 * from this.* because your function may be called after receiveProps but before
 * shouldComponentUpdate, and this new state, props, and context will not yet be
 * assigned to this.
 *
 * @param {object|function} partialState Next partial state or function to
 *        produce next partial state to be merged with current state.
 * @param {?function} callback Called after state is updated.
 * @final
 * @protected
 */

Component.prototype.setState = function (partialState, callback) {
  if (!(typeof partialState === 'object' || typeof partialState === 'function' || partialState == null)) {
    {
      throw Error( "setState(...): takes an object of state variables to update or a function which returns an object of state variables." );
    }
  }

  this.updater.enqueueSetState(this, partialState, callback, 'setState');
};
/**
 * Forces an update. This should only be invoked when it is known with
 * certainty that we are **not** in a DOM transaction.
 *
 * You may want to call this when you know that some deeper aspect of the
 * component's state has changed but `setState` was not called.
 *
 * This will not invoke `shouldComponentUpdate`, but it will invoke
 * `componentWillUpdate` and `componentDidUpdate`.
 *
 * @param {?function} callback Called after update is complete.
 * @final
 * @protected
 */


Component.prototype.forceUpdate = function (callback) {
  this.updater.enqueueForceUpdate(this, callback, 'forceUpdate');
};
/**
 * Deprecated APIs. These APIs used to exist on classic React classes but since
 * we would like to deprecate them, we're not going to move them over to this
 * modern base class. Instead, we define a getter that warns if it's accessed.
 */


{
  var deprecatedAPIs = {
    isMounted: ['isMounted', 'Instead, make sure to clean up subscriptions and pending requests in ' + 'componentWillUnmount to prevent memory leaks.'],
    replaceState: ['replaceState', 'Refactor your code to use setState instead (see ' + 'https://github.com/facebook/react/issues/3236).']
  };

  var defineDeprecationWarning = function (methodName, info) {
    Object.defineProperty(Component.prototype, methodName, {
      get: function () {
        warn('%s(...) is deprecated in plain JavaScript React classes. %s', info[0], info[1]);

        return undefined;
      }
    });
  };

  for (var fnName in deprecatedAPIs) {
    if (deprecatedAPIs.hasOwnProperty(fnName)) {
      defineDeprecationWarning(fnName, deprecatedAPIs[fnName]);
    }
  }
}

function ComponentDummy() {}

ComponentDummy.prototype = Component.prototype;
/**
 * Convenience component with default shallow equality check for sCU.
 */

function PureComponent(props, context, updater) {
  this.props = props;
  this.context = context; // If a component has string refs, we will assign a different object later.

  this.refs = emptyObject;
  this.updater = updater || ReactNoopUpdateQueue;
}

var pureComponentPrototype = PureComponent.prototype = new ComponentDummy();
pureComponentPrototype.constructor = PureComponent; // Avoid an extra prototype jump for these methods.

_assign(pureComponentPrototype, Component.prototype);

pureComponentPrototype.isPureReactComponent = true;

// an immutable object with a single mutable value
function createRef() {
  var refObject = {
    current: null
  };

  {
    Object.seal(refObject);
  }

  return refObject;
}

function getWrappedName(outerType, innerType, wrapperName) {
  var functionName = innerType.displayName || innerType.name || '';
  return outerType.displayName || (functionName !== '' ? wrapperName + "(" + functionName + ")" : wrapperName);
}

function getContextName(type) {
  return type.displayName || 'Context';
}

function getComponentName(type) {
  if (type == null) {
    // Host root, text node or just invalid type.
    return null;
  }

  {
    if (typeof type.tag === 'number') {
      error('Received an unexpected object in getComponentName(). ' + 'This is likely a bug in React. Please file an issue.');
    }
  }

  if (typeof type === 'function') {
    return type.displayName || type.name || null;
  }

  if (typeof type === 'string') {
    return type;
  }

  switch (type) {
    case exports.Fragment:
      return 'Fragment';

    case REACT_PORTAL_TYPE:
      return 'Portal';

    case exports.Profiler:
      return 'Profiler';

    case exports.StrictMode:
      return 'StrictMode';

    case exports.Suspense:
      return 'Suspense';

    case REACT_SUSPENSE_LIST_TYPE:
      return 'SuspenseList';
  }

  if (typeof type === 'object') {
    switch (type.$$typeof) {
      case REACT_CONTEXT_TYPE:
        var context = type;
        return getContextName(context) + '.Consumer';

      case REACT_PROVIDER_TYPE:
        var provider = type;
        return getContextName(provider._context) + '.Provider';

      case REACT_FORWARD_REF_TYPE:
        return getWrappedName(type, type.render, 'ForwardRef');

      case REACT_MEMO_TYPE:
        return getComponentName(type.type);

      case REACT_BLOCK_TYPE:
        return getComponentName(type._render);

      case REACT_LAZY_TYPE:
        {
          var lazyComponent = type;
          var payload = lazyComponent._payload;
          var init = lazyComponent._init;

          try {
            return getComponentName(init(payload));
          } catch (x) {
            return null;
          }
        }
    }
  }

  return null;
}

var hasOwnProperty = Object.prototype.hasOwnProperty;
var RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true
};
var specialPropKeyWarningShown, specialPropRefWarningShown, didWarnAboutStringRefs;

{
  didWarnAboutStringRefs = {};
}

function hasValidRef(config) {
  {
    if (hasOwnProperty.call(config, 'ref')) {
      var getter = Object.getOwnPropertyDescriptor(config, 'ref').get;

      if (getter && getter.isReactWarning) {
        return false;
      }
    }
  }

  return config.ref !== undefined;
}

function hasValidKey(config) {
  {
    if (hasOwnProperty.call(config, 'key')) {
      var getter = Object.getOwnPropertyDescriptor(config, 'key').get;

      if (getter && getter.isReactWarning) {
        return false;
      }
    }
  }

  return config.key !== undefined;
}

function defineKeyPropWarningGetter(props, displayName) {
  var warnAboutAccessingKey = function () {
    {
      if (!specialPropKeyWarningShown) {
        specialPropKeyWarningShown = true;

        error('%s: `key` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://reactjs.org/link/special-props)', displayName);
      }
    }
  };

  warnAboutAccessingKey.isReactWarning = true;
  Object.defineProperty(props, 'key', {
    get: warnAboutAccessingKey,
    configurable: true
  });
}

function defineRefPropWarningGetter(props, displayName) {
  var warnAboutAccessingRef = function () {
    {
      if (!specialPropRefWarningShown) {
        specialPropRefWarningShown = true;

        error('%s: `ref` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://reactjs.org/link/special-props)', displayName);
      }
    }
  };

  warnAboutAccessingRef.isReactWarning = true;
  Object.defineProperty(props, 'ref', {
    get: warnAboutAccessingRef,
    configurable: true
  });
}

function warnIfStringRefCannotBeAutoConverted(config) {
  {
    if (typeof config.ref === 'string' && ReactCurrentOwner.current && config.__self && ReactCurrentOwner.current.stateNode !== config.__self) {
      var componentName = getComponentName(ReactCurrentOwner.current.type);

      if (!didWarnAboutStringRefs[componentName]) {
        error('Component "%s" contains the string ref "%s". ' + 'Support for string refs will be removed in a future major release. ' + 'This case cannot be automatically converted to an arrow function. ' + 'We ask you to manually fix this case by using useRef() or createRef() instead. ' + 'Learn more about using refs safely here: ' + 'https://reactjs.org/link/strict-mode-string-ref', componentName, config.ref);

        didWarnAboutStringRefs[componentName] = true;
      }
    }
  }
}
/**
 * Factory method to create a new React element. This no longer adheres to
 * the class pattern, so do not use new to call it. Also, instanceof check
 * will not work. Instead test $$typeof field against Symbol.for('react.element') to check
 * if something is a React Element.
 *
 * @param {*} type
 * @param {*} props
 * @param {*} key
 * @param {string|object} ref
 * @param {*} owner
 * @param {*} self A *temporary* helper to detect places where `this` is
 * different from the `owner` when React.createElement is called, so that we
 * can warn. We want to get rid of owner and replace string `ref`s with arrow
 * functions, and as long as `this` and owner are the same, there will be no
 * change in behavior.
 * @param {*} source An annotation object (added by a transpiler or otherwise)
 * indicating filename, line number, and/or other information.
 * @internal
 */


var ReactElement = function (type, key, ref, self, source, owner, props) {
  var element = {
    // This tag allows us to uniquely identify this as a React Element
    $$typeof: REACT_ELEMENT_TYPE,
    // Built-in properties that belong on the element
    type: type,
    key: key,
    ref: ref,
    props: props,
    // Record the component responsible for creating this element.
    _owner: owner
  };

  {
    // The validation flag is currently mutative. We put it on
    // an external backing store so that we can freeze the whole object.
    // This can be replaced with a WeakMap once they are implemented in
    // commonly used development environments.
    element._store = {}; // To make comparing ReactElements easier for testing purposes, we make
    // the validation flag non-enumerable (where possible, which should
    // include every environment we run tests in), so the test framework
    // ignores it.

    Object.defineProperty(element._store, 'validated', {
      configurable: false,
      enumerable: false,
      writable: true,
      value: false
    }); // self and source are DEV only properties.

    Object.defineProperty(element, '_self', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: self
    }); // Two elements created in two different places should be considered
    // equal for testing purposes and therefore we hide it from enumeration.

    Object.defineProperty(element, '_source', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: source
    });

    if (Object.freeze) {
      Object.freeze(element.props);
      Object.freeze(element);
    }
  }

  return element;
};
/**
 * Create and return a new ReactElement of the given type.
 * See https://reactjs.org/docs/react-api.html#createelement
 */

function createElement(type, config, children) {
  var propName; // Reserved names are extracted

  var props = {};
  var key = null;
  var ref = null;
  var self = null;
  var source = null;

  if (config != null) {
    if (hasValidRef(config)) {
      ref = config.ref;

      {
        warnIfStringRefCannotBeAutoConverted(config);
      }
    }

    if (hasValidKey(config)) {
      key = '' + config.key;
    }

    self = config.__self === undefined ? null : config.__self;
    source = config.__source === undefined ? null : config.__source; // Remaining properties are added to a new props object

    for (propName in config) {
      if (hasOwnProperty.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
        props[propName] = config[propName];
      }
    }
  } // Children can be more than one argument, and those are transferred onto
  // the newly allocated props object.


  var childrenLength = arguments.length - 2;

  if (childrenLength === 1) {
    props.children = children;
  } else if (childrenLength > 1) {
    var childArray = Array(childrenLength);

    for (var i = 0; i < childrenLength; i++) {
      childArray[i] = arguments[i + 2];
    }

    {
      if (Object.freeze) {
        Object.freeze(childArray);
      }
    }

    props.children = childArray;
  } // Resolve default props


  if (type && type.defaultProps) {
    var defaultProps = type.defaultProps;

    for (propName in defaultProps) {
      if (props[propName] === undefined) {
        props[propName] = defaultProps[propName];
      }
    }
  }

  {
    if (key || ref) {
      var displayName = typeof type === 'function' ? type.displayName || type.name || 'Unknown' : type;

      if (key) {
        defineKeyPropWarningGetter(props, displayName);
      }

      if (ref) {
        defineRefPropWarningGetter(props, displayName);
      }
    }
  }

  return ReactElement(type, key, ref, self, source, ReactCurrentOwner.current, props);
}
function cloneAndReplaceKey(oldElement, newKey) {
  var newElement = ReactElement(oldElement.type, newKey, oldElement.ref, oldElement._self, oldElement._source, oldElement._owner, oldElement.props);
  return newElement;
}
/**
 * Clone and return a new ReactElement using element as the starting point.
 * See https://reactjs.org/docs/react-api.html#cloneelement
 */

function cloneElement(element, config, children) {
  if (!!(element === null || element === undefined)) {
    {
      throw Error( "React.cloneElement(...): The argument must be a React element, but you passed " + element + "." );
    }
  }

  var propName; // Original props are copied

  var props = _assign({}, element.props); // Reserved names are extracted


  var key = element.key;
  var ref = element.ref; // Self is preserved since the owner is preserved.

  var self = element._self; // Source is preserved since cloneElement is unlikely to be targeted by a
  // transpiler, and the original source is probably a better indicator of the
  // true owner.

  var source = element._source; // Owner will be preserved, unless ref is overridden

  var owner = element._owner;

  if (config != null) {
    if (hasValidRef(config)) {
      // Silently steal the ref from the parent.
      ref = config.ref;
      owner = ReactCurrentOwner.current;
    }

    if (hasValidKey(config)) {
      key = '' + config.key;
    } // Remaining properties override existing props


    var defaultProps;

    if (element.type && element.type.defaultProps) {
      defaultProps = element.type.defaultProps;
    }

    for (propName in config) {
      if (hasOwnProperty.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
        if (config[propName] === undefined && defaultProps !== undefined) {
          // Resolve default props
          props[propName] = defaultProps[propName];
        } else {
          props[propName] = config[propName];
        }
      }
    }
  } // Children can be more than one argument, and those are transferred onto
  // the newly allocated props object.


  var childrenLength = arguments.length - 2;

  if (childrenLength === 1) {
    props.children = children;
  } else if (childrenLength > 1) {
    var childArray = Array(childrenLength);

    for (var i = 0; i < childrenLength; i++) {
      childArray[i] = arguments[i + 2];
    }

    props.children = childArray;
  }

  return ReactElement(element.type, key, ref, self, source, owner, props);
}
/**
 * Verifies the object is a ReactElement.
 * See https://reactjs.org/docs/react-api.html#isvalidelement
 * @param {?object} object
 * @return {boolean} True if `object` is a ReactElement.
 * @final
 */

function isValidElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}

var SEPARATOR = '.';
var SUBSEPARATOR = ':';
/**
 * Escape and wrap key so it is safe to use as a reactid
 *
 * @param {string} key to be escaped.
 * @return {string} the escaped key.
 */

function escape(key) {
  var escapeRegex = /[=:]/g;
  var escaperLookup = {
    '=': '=0',
    ':': '=2'
  };
  var escapedString = key.replace(escapeRegex, function (match) {
    return escaperLookup[match];
  });
  return '$' + escapedString;
}
/**
 * TODO: Test that a single child and an array with one item have the same key
 * pattern.
 */


var didWarnAboutMaps = false;
var userProvidedKeyEscapeRegex = /\/+/g;

function escapeUserProvidedKey(text) {
  return text.replace(userProvidedKeyEscapeRegex, '$&/');
}
/**
 * Generate a key string that identifies a element within a set.
 *
 * @param {*} element A element that could contain a manual key.
 * @param {number} index Index that is used if a manual key is not provided.
 * @return {string}
 */


function getElementKey(element, index) {
  // Do some typechecking here since we call this blindly. We want to ensure
  // that we don't block potential future ES APIs.
  if (typeof element === 'object' && element !== null && element.key != null) {
    // Explicit key
    return escape('' + element.key);
  } // Implicit key determined by the index in the set


  return index.toString(36);
}

function mapIntoArray(children, array, escapedPrefix, nameSoFar, callback) {
  var type = typeof children;

  if (type === 'undefined' || type === 'boolean') {
    // All of the above are perceived as null.
    children = null;
  }

  var invokeCallback = false;

  if (children === null) {
    invokeCallback = true;
  } else {
    switch (type) {
      case 'string':
      case 'number':
        invokeCallback = true;
        break;

      case 'object':
        switch (children.$$typeof) {
          case REACT_ELEMENT_TYPE:
          case REACT_PORTAL_TYPE:
            invokeCallback = true;
        }

    }
  }

  if (invokeCallback) {
    var _child = children;
    var mappedChild = callback(_child); // If it's the only child, treat the name as if it was wrapped in an array
    // so that it's consistent if the number of children grows:

    var childKey = nameSoFar === '' ? SEPARATOR + getElementKey(_child, 0) : nameSoFar;

    if (Array.isArray(mappedChild)) {
      var escapedChildKey = '';

      if (childKey != null) {
        escapedChildKey = escapeUserProvidedKey(childKey) + '/';
      }

      mapIntoArray(mappedChild, array, escapedChildKey, '', function (c) {
        return c;
      });
    } else if (mappedChild != null) {
      if (isValidElement(mappedChild)) {
        mappedChild = cloneAndReplaceKey(mappedChild, // Keep both the (mapped) and old keys if they differ, just as
        // traverseAllChildren used to do for objects as children
        escapedPrefix + ( // $FlowFixMe Flow incorrectly thinks React.Portal doesn't have a key
        mappedChild.key && (!_child || _child.key !== mappedChild.key) ? // $FlowFixMe Flow incorrectly thinks existing element's key can be a number
        escapeUserProvidedKey('' + mappedChild.key) + '/' : '') + childKey);
      }

      array.push(mappedChild);
    }

    return 1;
  }

  var child;
  var nextName;
  var subtreeCount = 0; // Count of children found in the current subtree.

  var nextNamePrefix = nameSoFar === '' ? SEPARATOR : nameSoFar + SUBSEPARATOR;

  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      child = children[i];
      nextName = nextNamePrefix + getElementKey(child, i);
      subtreeCount += mapIntoArray(child, array, escapedPrefix, nextName, callback);
    }
  } else {
    var iteratorFn = getIteratorFn(children);

    if (typeof iteratorFn === 'function') {
      var iterableChildren = children;

      {
        // Warn about using Maps as children
        if (iteratorFn === iterableChildren.entries) {
          if (!didWarnAboutMaps) {
            warn('Using Maps as children is not supported. ' + 'Use an array of keyed ReactElements instead.');
          }

          didWarnAboutMaps = true;
        }
      }

      var iterator = iteratorFn.call(iterableChildren);
      var step;
      var ii = 0;

      while (!(step = iterator.next()).done) {
        child = step.value;
        nextName = nextNamePrefix + getElementKey(child, ii++);
        subtreeCount += mapIntoArray(child, array, escapedPrefix, nextName, callback);
      }
    } else if (type === 'object') {
      var childrenString = '' + children;

      {
        {
          throw Error( "Objects are not valid as a React child (found: " + (childrenString === '[object Object]' ? 'object with keys {' + Object.keys(children).join(', ') + '}' : childrenString) + "). If you meant to render a collection of children, use an array instead." );
        }
      }
    }
  }

  return subtreeCount;
}

/**
 * Maps children that are typically specified as `props.children`.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrenmap
 *
 * The provided mapFunction(child, index) will be called for each
 * leaf child.
 *
 * @param {?*} children Children tree container.
 * @param {function(*, int)} func The map function.
 * @param {*} context Context for mapFunction.
 * @return {object} Object containing the ordered map of results.
 */
function mapChildren(children, func, context) {
  if (children == null) {
    return children;
  }

  var result = [];
  var count = 0;
  mapIntoArray(children, result, '', '', function (child) {
    return func.call(context, child, count++);
  });
  return result;
}
/**
 * Count the number of children that are typically specified as
 * `props.children`.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrencount
 *
 * @param {?*} children Children tree container.
 * @return {number} The number of children.
 */


function countChildren(children) {
  var n = 0;
  mapChildren(children, function () {
    n++; // Don't return anything
  });
  return n;
}

/**
 * Iterates through children that are typically specified as `props.children`.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrenforeach
 *
 * The provided forEachFunc(child, index) will be called for each
 * leaf child.
 *
 * @param {?*} children Children tree container.
 * @param {function(*, int)} forEachFunc
 * @param {*} forEachContext Context for forEachContext.
 */
function forEachChildren(children, forEachFunc, forEachContext) {
  mapChildren(children, function () {
    forEachFunc.apply(this, arguments); // Don't return anything.
  }, forEachContext);
}
/**
 * Flatten a children object (typically specified as `props.children`) and
 * return an array with appropriately re-keyed children.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrentoarray
 */


function toArray(children) {
  return mapChildren(children, function (child) {
    return child;
  }) || [];
}
/**
 * Returns the first child in a collection of children and verifies that there
 * is only one child in the collection.
 *
 * See https://reactjs.org/docs/react-api.html#reactchildrenonly
 *
 * The current implementation of this function assumes that a single child gets
 * passed without a wrapper, but the purpose of this helper function is to
 * abstract away the particular structure of children.
 *
 * @param {?object} children Child collection structure.
 * @return {ReactElement} The first and only `ReactElement` contained in the
 * structure.
 */


function onlyChild(children) {
  if (!isValidElement(children)) {
    {
      throw Error( "React.Children.only expected to receive a single React element child." );
    }
  }

  return children;
}

function createContext(defaultValue, calculateChangedBits) {
  if (calculateChangedBits === undefined) {
    calculateChangedBits = null;
  } else {
    {
      if (calculateChangedBits !== null && typeof calculateChangedBits !== 'function') {
        error('createContext: Expected the optional second argument to be a ' + 'function. Instead received: %s', calculateChangedBits);
      }
    }
  }

  var context = {
    $$typeof: REACT_CONTEXT_TYPE,
    _calculateChangedBits: calculateChangedBits,
    // As a workaround to support multiple concurrent renderers, we categorize
    // some renderers as primary and others as secondary. We only expect
    // there to be two concurrent renderers at most: React Native (primary) and
    // Fabric (secondary); React DOM (primary) and React ART (secondary).
    // Secondary renderers store their context values on separate fields.
    _currentValue: defaultValue,
    _currentValue2: defaultValue,
    // Used to track how many concurrent renderers this context currently
    // supports within in a single renderer. Such as parallel server rendering.
    _threadCount: 0,
    // These are circular
    Provider: null,
    Consumer: null
  };
  context.Provider = {
    $$typeof: REACT_PROVIDER_TYPE,
    _context: context
  };
  var hasWarnedAboutUsingNestedContextConsumers = false;
  var hasWarnedAboutUsingConsumerProvider = false;
  var hasWarnedAboutDisplayNameOnConsumer = false;

  {
    // A separate object, but proxies back to the original context object for
    // backwards compatibility. It has a different $$typeof, so we can properly
    // warn for the incorrect usage of Context as a Consumer.
    var Consumer = {
      $$typeof: REACT_CONTEXT_TYPE,
      _context: context,
      _calculateChangedBits: context._calculateChangedBits
    }; // $FlowFixMe: Flow complains about not setting a value, which is intentional here

    Object.defineProperties(Consumer, {
      Provider: {
        get: function () {
          if (!hasWarnedAboutUsingConsumerProvider) {
            hasWarnedAboutUsingConsumerProvider = true;

            error('Rendering <Context.Consumer.Provider> is not supported and will be removed in ' + 'a future major release. Did you mean to render <Context.Provider> instead?');
          }

          return context.Provider;
        },
        set: function (_Provider) {
          context.Provider = _Provider;
        }
      },
      _currentValue: {
        get: function () {
          return context._currentValue;
        },
        set: function (_currentValue) {
          context._currentValue = _currentValue;
        }
      },
      _currentValue2: {
        get: function () {
          return context._currentValue2;
        },
        set: function (_currentValue2) {
          context._currentValue2 = _currentValue2;
        }
      },
      _threadCount: {
        get: function () {
          return context._threadCount;
        },
        set: function (_threadCount) {
          context._threadCount = _threadCount;
        }
      },
      Consumer: {
        get: function () {
          if (!hasWarnedAboutUsingNestedContextConsumers) {
            hasWarnedAboutUsingNestedContextConsumers = true;

            error('Rendering <Context.Consumer.Consumer> is not supported and will be removed in ' + 'a future major release. Did you mean to render <Context.Consumer> instead?');
          }

          return context.Consumer;
        }
      },
      displayName: {
        get: function () {
          return context.displayName;
        },
        set: function (displayName) {
          if (!hasWarnedAboutDisplayNameOnConsumer) {
            warn('Setting `displayName` on Context.Consumer has no effect. ' + "You should set it directly on the context with Context.displayName = '%s'.", displayName);

            hasWarnedAboutDisplayNameOnConsumer = true;
          }
        }
      }
    }); // $FlowFixMe: Flow complains about missing properties because it doesn't understand defineProperty

    context.Consumer = Consumer;
  }

  {
    context._currentRenderer = null;
    context._currentRenderer2 = null;
  }

  return context;
}

var Uninitialized = -1;
var Pending = 0;
var Resolved = 1;
var Rejected = 2;

function lazyInitializer(payload) {
  if (payload._status === Uninitialized) {
    var ctor = payload._result;
    var thenable = ctor(); // Transition to the next state.

    var pending = payload;
    pending._status = Pending;
    pending._result = thenable;
    thenable.then(function (moduleObject) {
      if (payload._status === Pending) {
        var defaultExport = moduleObject.default;

        {
          if (defaultExport === undefined) {
            error('lazy: Expected the result of a dynamic import() call. ' + 'Instead received: %s\n\nYour code should look like: \n  ' + // Break up imports to avoid accidentally parsing them as dependencies.
            'const MyComponent = lazy(() => imp' + "ort('./MyComponent'))", moduleObject);
          }
        } // Transition to the next state.


        var resolved = payload;
        resolved._status = Resolved;
        resolved._result = defaultExport;
      }
    }, function (error) {
      if (payload._status === Pending) {
        // Transition to the next state.
        var rejected = payload;
        rejected._status = Rejected;
        rejected._result = error;
      }
    });
  }

  if (payload._status === Resolved) {
    return payload._result;
  } else {
    throw payload._result;
  }
}

function lazy(ctor) {
  var payload = {
    // We use these fields to store the result.
    _status: -1,
    _result: ctor
  };
  var lazyType = {
    $$typeof: REACT_LAZY_TYPE,
    _payload: payload,
    _init: lazyInitializer
  };

  {
    // In production, this would just set it on the object.
    var defaultProps;
    var propTypes; // $FlowFixMe

    Object.defineProperties(lazyType, {
      defaultProps: {
        configurable: true,
        get: function () {
          return defaultProps;
        },
        set: function (newDefaultProps) {
          error('React.lazy(...): It is not supported to assign `defaultProps` to ' + 'a lazy component import. Either specify them where the component ' + 'is defined, or create a wrapping component around it.');

          defaultProps = newDefaultProps; // Match production behavior more closely:
          // $FlowFixMe

          Object.defineProperty(lazyType, 'defaultProps', {
            enumerable: true
          });
        }
      },
      propTypes: {
        configurable: true,
        get: function () {
          return propTypes;
        },
        set: function (newPropTypes) {
          error('React.lazy(...): It is not supported to assign `propTypes` to ' + 'a lazy component import. Either specify them where the component ' + 'is defined, or create a wrapping component around it.');

          propTypes = newPropTypes; // Match production behavior more closely:
          // $FlowFixMe

          Object.defineProperty(lazyType, 'propTypes', {
            enumerable: true
          });
        }
      }
    });
  }

  return lazyType;
}

function forwardRef(render) {
  {
    if (render != null && render.$$typeof === REACT_MEMO_TYPE) {
      error('forwardRef requires a render function but received a `memo` ' + 'component. Instead of forwardRef(memo(...)), use ' + 'memo(forwardRef(...)).');
    } else if (typeof render !== 'function') {
      error('forwardRef requires a render function but was given %s.', render === null ? 'null' : typeof render);
    } else {
      if (render.length !== 0 && render.length !== 2) {
        error('forwardRef render functions accept exactly two parameters: props and ref. %s', render.length === 1 ? 'Did you forget to use the ref parameter?' : 'Any additional parameter will be undefined.');
      }
    }

    if (render != null) {
      if (render.defaultProps != null || render.propTypes != null) {
        error('forwardRef render functions do not support propTypes or defaultProps. ' + 'Did you accidentally pass a React component?');
      }
    }
  }

  var elementType = {
    $$typeof: REACT_FORWARD_REF_TYPE,
    render: render
  };

  {
    var ownName;
    Object.defineProperty(elementType, 'displayName', {
      enumerable: false,
      configurable: true,
      get: function () {
        return ownName;
      },
      set: function (name) {
        ownName = name;

        if (render.displayName == null) {
          render.displayName = name;
        }
      }
    });
  }

  return elementType;
}

// Filter certain DOM attributes (e.g. src, href) if their values are empty strings.

var enableScopeAPI = false; // Experimental Create Event Handle API.

function isValidElementType(type) {
  if (typeof type === 'string' || typeof type === 'function') {
    return true;
  } // Note: typeof might be other than 'symbol' or 'number' (e.g. if it's a polyfill).


  if (type === exports.Fragment || type === exports.Profiler || type === REACT_DEBUG_TRACING_MODE_TYPE || type === exports.StrictMode || type === exports.Suspense || type === REACT_SUSPENSE_LIST_TYPE || type === REACT_LEGACY_HIDDEN_TYPE || enableScopeAPI ) {
    return true;
  }

  if (typeof type === 'object' && type !== null) {
    if (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_BLOCK_TYPE || type[0] === REACT_SERVER_BLOCK_TYPE) {
      return true;
    }
  }

  return false;
}

function memo(type, compare) {
  {
    if (!isValidElementType(type)) {
      error('memo: The first argument must be a component. Instead ' + 'received: %s', type === null ? 'null' : typeof type);
    }
  }

  var elementType = {
    $$typeof: REACT_MEMO_TYPE,
    type: type,
    compare: compare === undefined ? null : compare
  };

  {
    var ownName;
    Object.defineProperty(elementType, 'displayName', {
      enumerable: false,
      configurable: true,
      get: function () {
        return ownName;
      },
      set: function (name) {
        ownName = name;

        if (type.displayName == null) {
          type.displayName = name;
        }
      }
    });
  }

  return elementType;
}

function resolveDispatcher() {
  var dispatcher = ReactCurrentDispatcher.current;

  if (!(dispatcher !== null)) {
    {
      throw Error( "Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for one of the following reasons:\n1. You might have mismatching versions of React and the renderer (such as React DOM)\n2. You might be breaking the Rules of Hooks\n3. You might have more than one copy of React in the same app\nSee https://reactjs.org/link/invalid-hook-call for tips about how to debug and fix this problem." );
    }
  }

  return dispatcher;
}

function useContext(Context, unstable_observedBits) {
  var dispatcher = resolveDispatcher();

  {
    if (unstable_observedBits !== undefined) {
      error('useContext() second argument is reserved for future ' + 'use in React. Passing it is not supported. ' + 'You passed: %s.%s', unstable_observedBits, typeof unstable_observedBits === 'number' && Array.isArray(arguments[2]) ? '\n\nDid you call array.map(useContext)? ' + 'Calling Hooks inside a loop is not supported. ' + 'Learn more at https://reactjs.org/link/rules-of-hooks' : '');
    } // TODO: add a more generic warning for invalid values.


    if (Context._context !== undefined) {
      var realContext = Context._context; // Don't deduplicate because this legitimately causes bugs
      // and nobody should be using this in existing code.

      if (realContext.Consumer === Context) {
        error('Calling useContext(Context.Consumer) is not supported, may cause bugs, and will be ' + 'removed in a future major release. Did you mean to call useContext(Context) instead?');
      } else if (realContext.Provider === Context) {
        error('Calling useContext(Context.Provider) is not supported. ' + 'Did you mean to call useContext(Context) instead?');
      }
    }
  }

  return dispatcher.useContext(Context, unstable_observedBits);
}
function useState(initialState) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useState(initialState);
}
function useReducer(reducer, initialArg, init) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useReducer(reducer, initialArg, init);
}
function useRef(initialValue) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useRef(initialValue);
}
function useEffect(create, deps) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useEffect(create, deps);
}
function useLayoutEffect(create, deps) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useLayoutEffect(create, deps);
}
function useCallback(callback, deps) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useCallback(callback, deps);
}
function useMemo(create, deps) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useMemo(create, deps);
}
function useImperativeHandle(ref, create, deps) {
  var dispatcher = resolveDispatcher();
  return dispatcher.useImperativeHandle(ref, create, deps);
}
function useDebugValue(value, formatterFn) {
  {
    var dispatcher = resolveDispatcher();
    return dispatcher.useDebugValue(value, formatterFn);
  }
}

// Helpers to patch console.logs to avoid logging during side-effect free
// replaying on render function. This currently only patches the object
// lazily which won't cover if the log function was extracted eagerly.
// We could also eagerly patch the method.
var disabledDepth = 0;
var prevLog;
var prevInfo;
var prevWarn;
var prevError;
var prevGroup;
var prevGroupCollapsed;
var prevGroupEnd;

function disabledLog() {}

disabledLog.__reactDisabledLog = true;
function disableLogs() {
  {
    if (disabledDepth === 0) {
      /* eslint-disable react-internal/no-production-logging */
      prevLog = console.log;
      prevInfo = console.info;
      prevWarn = console.warn;
      prevError = console.error;
      prevGroup = console.group;
      prevGroupCollapsed = console.groupCollapsed;
      prevGroupEnd = console.groupEnd; // https://github.com/facebook/react/issues/19099

      var props = {
        configurable: true,
        enumerable: true,
        value: disabledLog,
        writable: true
      }; // $FlowFixMe Flow thinks console is immutable.

      Object.defineProperties(console, {
        info: props,
        log: props,
        warn: props,
        error: props,
        group: props,
        groupCollapsed: props,
        groupEnd: props
      });
      /* eslint-enable react-internal/no-production-logging */
    }

    disabledDepth++;
  }
}
function reenableLogs() {
  {
    disabledDepth--;

    if (disabledDepth === 0) {
      /* eslint-disable react-internal/no-production-logging */
      var props = {
        configurable: true,
        enumerable: true,
        writable: true
      }; // $FlowFixMe Flow thinks console is immutable.

      Object.defineProperties(console, {
        log: _assign({}, props, {
          value: prevLog
        }),
        info: _assign({}, props, {
          value: prevInfo
        }),
        warn: _assign({}, props, {
          value: prevWarn
        }),
        error: _assign({}, props, {
          value: prevError
        }),
        group: _assign({}, props, {
          value: prevGroup
        }),
        groupCollapsed: _assign({}, props, {
          value: prevGroupCollapsed
        }),
        groupEnd: _assign({}, props, {
          value: prevGroupEnd
        })
      });
      /* eslint-enable react-internal/no-production-logging */
    }

    if (disabledDepth < 0) {
      error('disabledDepth fell below zero. ' + 'This is a bug in React. Please file an issue.');
    }
  }
}

var ReactCurrentDispatcher$1 = ReactSharedInternals.ReactCurrentDispatcher;
var prefix;
function describeBuiltInComponentFrame(name, source, ownerFn) {
  {
    if (prefix === undefined) {
      // Extract the VM specific prefix used by each line.
      try {
        throw Error();
      } catch (x) {
        var match = x.stack.trim().match(/\n( *(at )?)/);
        prefix = match && match[1] || '';
      }
    } // We use the prefix to ensure our stacks line up with native stack frames.


    return '\n' + prefix + name;
  }
}
var reentry = false;
var componentFrameCache;

{
  var PossiblyWeakMap = typeof WeakMap === 'function' ? WeakMap : Map;
  componentFrameCache = new PossiblyWeakMap();
}

function describeNativeComponentFrame(fn, construct) {
  // If something asked for a stack inside a fake render, it should get ignored.
  if (!fn || reentry) {
    return '';
  }

  {
    var frame = componentFrameCache.get(fn);

    if (frame !== undefined) {
      return frame;
    }
  }

  var control;
  reentry = true;
  var previousPrepareStackTrace = Error.prepareStackTrace; // $FlowFixMe It does accept undefined.

  Error.prepareStackTrace = undefined;
  var previousDispatcher;

  {
    previousDispatcher = ReactCurrentDispatcher$1.current; // Set the dispatcher in DEV because this might be call in the render function
    // for warnings.

    ReactCurrentDispatcher$1.current = null;
    disableLogs();
  }

  try {
    // This should throw.
    if (construct) {
      // Something should be setting the props in the constructor.
      var Fake = function () {
        throw Error();
      }; // $FlowFixMe


      Object.defineProperty(Fake.prototype, 'props', {
        set: function () {
          // We use a throwing setter instead of frozen or non-writable props
          // because that won't throw in a non-strict mode function.
          throw Error();
        }
      });

      if (typeof Reflect === 'object' && Reflect.construct) {
        // We construct a different control for this case to include any extra
        // frames added by the construct call.
        try {
          Reflect.construct(Fake, []);
        } catch (x) {
          control = x;
        }

        Reflect.construct(fn, [], Fake);
      } else {
        try {
          Fake.call();
        } catch (x) {
          control = x;
        }

        fn.call(Fake.prototype);
      }
    } else {
      try {
        throw Error();
      } catch (x) {
        control = x;
      }

      fn();
    }
  } catch (sample) {
    // This is inlined manually because closure doesn't do it for us.
    if (sample && control && typeof sample.stack === 'string') {
      // This extracts the first frame from the sample that isn't also in the control.
      // Skipping one frame that we assume is the frame that calls the two.
      var sampleLines = sample.stack.split('\n');
      var controlLines = control.stack.split('\n');
      var s = sampleLines.length - 1;
      var c = controlLines.length - 1;

      while (s >= 1 && c >= 0 && sampleLines[s] !== controlLines[c]) {
        // We expect at least one stack frame to be shared.
        // Typically this will be the root most one. However, stack frames may be
        // cut off due to maximum stack limits. In this case, one maybe cut off
        // earlier than the other. We assume that the sample is longer or the same
        // and there for cut off earlier. So we should find the root most frame in
        // the sample somewhere in the control.
        c--;
      }

      for (; s >= 1 && c >= 0; s--, c--) {
        // Next we find the first one that isn't the same which should be the
        // frame that called our sample function and the control.
        if (sampleLines[s] !== controlLines[c]) {
          // In V8, the first line is describing the message but other VMs don't.
          // If we're about to return the first line, and the control is also on the same
          // line, that's a pretty good indicator that our sample threw at same line as
          // the control. I.e. before we entered the sample frame. So we ignore this result.
          // This can happen if you passed a class to function component, or non-function.
          if (s !== 1 || c !== 1) {
            do {
              s--;
              c--; // We may still have similar intermediate frames from the construct call.
              // The next one that isn't the same should be our match though.

              if (c < 0 || sampleLines[s] !== controlLines[c]) {
                // V8 adds a "new" prefix for native classes. Let's remove it to make it prettier.
                var _frame = '\n' + sampleLines[s].replace(' at new ', ' at ');

                {
                  if (typeof fn === 'function') {
                    componentFrameCache.set(fn, _frame);
                  }
                } // Return the line we found.


                return _frame;
              }
            } while (s >= 1 && c >= 0);
          }

          break;
        }
      }
    }
  } finally {
    reentry = false;

    {
      ReactCurrentDispatcher$1.current = previousDispatcher;
      reenableLogs();
    }

    Error.prepareStackTrace = previousPrepareStackTrace;
  } // Fallback to just using the name if we couldn't make it throw.


  var name = fn ? fn.displayName || fn.name : '';
  var syntheticFrame = name ? describeBuiltInComponentFrame(name) : '';

  {
    if (typeof fn === 'function') {
      componentFrameCache.set(fn, syntheticFrame);
    }
  }

  return syntheticFrame;
}
function describeFunctionComponentFrame(fn, source, ownerFn) {
  {
    return describeNativeComponentFrame(fn, false);
  }
}

function shouldConstruct(Component) {
  var prototype = Component.prototype;
  return !!(prototype && prototype.isReactComponent);
}

function describeUnknownElementTypeFrameInDEV(type, source, ownerFn) {

  if (type == null) {
    return '';
  }

  if (typeof type === 'function') {
    {
      return describeNativeComponentFrame(type, shouldConstruct(type));
    }
  }

  if (typeof type === 'string') {
    return describeBuiltInComponentFrame(type);
  }

  switch (type) {
    case exports.Suspense:
      return describeBuiltInComponentFrame('Suspense');

    case REACT_SUSPENSE_LIST_TYPE:
      return describeBuiltInComponentFrame('SuspenseList');
  }

  if (typeof type === 'object') {
    switch (type.$$typeof) {
      case REACT_FORWARD_REF_TYPE:
        return describeFunctionComponentFrame(type.render);

      case REACT_MEMO_TYPE:
        // Memo may contain any component type so we recursively resolve it.
        return describeUnknownElementTypeFrameInDEV(type.type, source, ownerFn);

      case REACT_BLOCK_TYPE:
        return describeFunctionComponentFrame(type._render);

      case REACT_LAZY_TYPE:
        {
          var lazyComponent = type;
          var payload = lazyComponent._payload;
          var init = lazyComponent._init;

          try {
            // Lazy may contain any component type so we recursively resolve it.
            return describeUnknownElementTypeFrameInDEV(init(payload), source, ownerFn);
          } catch (x) {}
        }
    }
  }

  return '';
}

var loggedTypeFailures = {};
var ReactDebugCurrentFrame$1 = ReactSharedInternals.ReactDebugCurrentFrame;

function setCurrentlyValidatingElement(element) {
  {
    if (element) {
      var owner = element._owner;
      var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
      ReactDebugCurrentFrame$1.setExtraStackFrame(stack);
    } else {
      ReactDebugCurrentFrame$1.setExtraStackFrame(null);
    }
  }
}

function checkPropTypes(typeSpecs, values, location, componentName, element) {
  {
    // $FlowFixMe This is okay but Flow doesn't know it.
    var has = Function.call.bind(Object.prototype.hasOwnProperty);

    for (var typeSpecName in typeSpecs) {
      if (has(typeSpecs, typeSpecName)) {
        var error$1 = void 0; // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.

        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error((componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' + 'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.' + 'This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.');
            err.name = 'Invariant Violation';
            throw err;
          }

          error$1 = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED');
        } catch (ex) {
          error$1 = ex;
        }

        if (error$1 && !(error$1 instanceof Error)) {
          setCurrentlyValidatingElement(element);

          error('%s: type specification of %s' + ' `%s` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a %s. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).', componentName || 'React class', location, typeSpecName, typeof error$1);

          setCurrentlyValidatingElement(null);
        }

        if (error$1 instanceof Error && !(error$1.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error$1.message] = true;
          setCurrentlyValidatingElement(element);

          error('Failed %s type: %s', location, error$1.message);

          setCurrentlyValidatingElement(null);
        }
      }
    }
  }
}

function setCurrentlyValidatingElement$1(element) {
  {
    if (element) {
      var owner = element._owner;
      var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
      setExtraStackFrame(stack);
    } else {
      setExtraStackFrame(null);
    }
  }
}

var propTypesMisspellWarningShown;

{
  propTypesMisspellWarningShown = false;
}

function getDeclarationErrorAddendum() {
  if (ReactCurrentOwner.current) {
    var name = getComponentName(ReactCurrentOwner.current.type);

    if (name) {
      return '\n\nCheck the render method of `' + name + '`.';
    }
  }

  return '';
}

function getSourceInfoErrorAddendum(source) {
  if (source !== undefined) {
    var fileName = source.fileName.replace(/^.*[\\\/]/, '');
    var lineNumber = source.lineNumber;
    return '\n\nCheck your code at ' + fileName + ':' + lineNumber + '.';
  }

  return '';
}

function getSourceInfoErrorAddendumForProps(elementProps) {
  if (elementProps !== null && elementProps !== undefined) {
    return getSourceInfoErrorAddendum(elementProps.__source);
  }

  return '';
}
/**
 * Warn if there's no key explicitly set on dynamic arrays of children or
 * object keys are not valid. This allows us to keep track of children between
 * updates.
 */


var ownerHasKeyUseWarning = {};

function getCurrentComponentErrorInfo(parentType) {
  var info = getDeclarationErrorAddendum();

  if (!info) {
    var parentName = typeof parentType === 'string' ? parentType : parentType.displayName || parentType.name;

    if (parentName) {
      info = "\n\nCheck the top-level render call using <" + parentName + ">.";
    }
  }

  return info;
}
/**
 * Warn if the element doesn't have an explicit key assigned to it.
 * This element is in an array. The array could grow and shrink or be
 * reordered. All children that haven't already been validated are required to
 * have a "key" property assigned to it. Error statuses are cached so a warning
 * will only be shown once.
 *
 * @internal
 * @param {ReactElement} element Element that requires a key.
 * @param {*} parentType element's parent's type.
 */


function validateExplicitKey(element, parentType) {
  if (!element._store || element._store.validated || element.key != null) {
    return;
  }

  element._store.validated = true;
  var currentComponentErrorInfo = getCurrentComponentErrorInfo(parentType);

  if (ownerHasKeyUseWarning[currentComponentErrorInfo]) {
    return;
  }

  ownerHasKeyUseWarning[currentComponentErrorInfo] = true; // Usually the current owner is the offender, but if it accepts children as a
  // property, it may be the creator of the child that's responsible for
  // assigning it a key.

  var childOwner = '';

  if (element && element._owner && element._owner !== ReactCurrentOwner.current) {
    // Give the component that originally created this child.
    childOwner = " It was passed a child from " + getComponentName(element._owner.type) + ".";
  }

  {
    setCurrentlyValidatingElement$1(element);

    error('Each child in a list should have a unique "key" prop.' + '%s%s See https://reactjs.org/link/warning-keys for more information.', currentComponentErrorInfo, childOwner);

    setCurrentlyValidatingElement$1(null);
  }
}
/**
 * Ensure that every element either is passed in a static location, in an
 * array with an explicit keys property defined, or in an object literal
 * with valid key property.
 *
 * @internal
 * @param {ReactNode} node Statically passed child of any type.
 * @param {*} parentType node's parent's type.
 */


function validateChildKeys(node, parentType) {
  if (typeof node !== 'object') {
    return;
  }

  if (Array.isArray(node)) {
    for (var i = 0; i < node.length; i++) {
      var child = node[i];

      if (isValidElement(child)) {
        validateExplicitKey(child, parentType);
      }
    }
  } else if (isValidElement(node)) {
    // This element was passed in a valid location.
    if (node._store) {
      node._store.validated = true;
    }
  } else if (node) {
    var iteratorFn = getIteratorFn(node);

    if (typeof iteratorFn === 'function') {
      // Entry iterators used to provide implicit keys,
      // but now we print a separate warning for them later.
      if (iteratorFn !== node.entries) {
        var iterator = iteratorFn.call(node);
        var step;

        while (!(step = iterator.next()).done) {
          if (isValidElement(step.value)) {
            validateExplicitKey(step.value, parentType);
          }
        }
      }
    }
  }
}
/**
 * Given an element, validate that its props follow the propTypes definition,
 * provided by the type.
 *
 * @param {ReactElement} element
 */


function validatePropTypes(element) {
  {
    var type = element.type;

    if (type === null || type === undefined || typeof type === 'string') {
      return;
    }

    var propTypes;

    if (typeof type === 'function') {
      propTypes = type.propTypes;
    } else if (typeof type === 'object' && (type.$$typeof === REACT_FORWARD_REF_TYPE || // Note: Memo only checks outer props here.
    // Inner props are checked in the reconciler.
    type.$$typeof === REACT_MEMO_TYPE)) {
      propTypes = type.propTypes;
    } else {
      return;
    }

    if (propTypes) {
      // Intentionally inside to avoid triggering lazy initializers:
      var name = getComponentName(type);
      checkPropTypes(propTypes, element.props, 'prop', name, element);
    } else if (type.PropTypes !== undefined && !propTypesMisspellWarningShown) {
      propTypesMisspellWarningShown = true; // Intentionally inside to avoid triggering lazy initializers:

      var _name = getComponentName(type);

      error('Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?', _name || 'Unknown');
    }

    if (typeof type.getDefaultProps === 'function' && !type.getDefaultProps.isReactClassApproved) {
      error('getDefaultProps is only used on classic React.createClass ' + 'definitions. Use a static property named `defaultProps` instead.');
    }
  }
}
/**
 * Given a fragment, validate that it can only be provided with fragment props
 * @param {ReactElement} fragment
 */


function validateFragmentProps(fragment) {
  {
    var keys = Object.keys(fragment.props);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];

      if (key !== 'children' && key !== 'key') {
        setCurrentlyValidatingElement$1(fragment);

        error('Invalid prop `%s` supplied to `React.Fragment`. ' + 'React.Fragment can only have `key` and `children` props.', key);

        setCurrentlyValidatingElement$1(null);
        break;
      }
    }

    if (fragment.ref !== null) {
      setCurrentlyValidatingElement$1(fragment);

      error('Invalid attribute `ref` supplied to `React.Fragment`.');

      setCurrentlyValidatingElement$1(null);
    }
  }
}
function createElementWithValidation(type, props, children) {
  var validType = isValidElementType(type); // We warn in this case but don't throw. We expect the element creation to
  // succeed and there will likely be errors in render.

  if (!validType) {
    var info = '';

    if (type === undefined || typeof type === 'object' && type !== null && Object.keys(type).length === 0) {
      info += ' You likely forgot to export your component from the file ' + "it's defined in, or you might have mixed up default and named imports.";
    }

    var sourceInfo = getSourceInfoErrorAddendumForProps(props);

    if (sourceInfo) {
      info += sourceInfo;
    } else {
      info += getDeclarationErrorAddendum();
    }

    var typeString;

    if (type === null) {
      typeString = 'null';
    } else if (Array.isArray(type)) {
      typeString = 'array';
    } else if (type !== undefined && type.$$typeof === REACT_ELEMENT_TYPE) {
      typeString = "<" + (getComponentName(type.type) || 'Unknown') + " />";
      info = ' Did you accidentally export a JSX literal instead of a component?';
    } else {
      typeString = typeof type;
    }

    {
      error('React.createElement: type is invalid -- expected a string (for ' + 'built-in components) or a class/function (for composite ' + 'components) but got: %s.%s', typeString, info);
    }
  }

  var element = createElement.apply(this, arguments); // The result can be nullish if a mock or a custom function is used.
  // TODO: Drop this when these are no longer allowed as the type argument.

  if (element == null) {
    return element;
  } // Skip key warning if the type isn't valid since our key validation logic
  // doesn't expect a non-string/function type and can throw confusing errors.
  // We don't want exception behavior to differ between dev and prod.
  // (Rendering will throw with a helpful message and as soon as the type is
  // fixed, the key warnings will appear.)


  if (validType) {
    for (var i = 2; i < arguments.length; i++) {
      validateChildKeys(arguments[i], type);
    }
  }

  if (type === exports.Fragment) {
    validateFragmentProps(element);
  } else {
    validatePropTypes(element);
  }

  return element;
}
var didWarnAboutDeprecatedCreateFactory = false;
function createFactoryWithValidation(type) {
  var validatedFactory = createElementWithValidation.bind(null, type);
  validatedFactory.type = type;

  {
    if (!didWarnAboutDeprecatedCreateFactory) {
      didWarnAboutDeprecatedCreateFactory = true;

      warn('React.createFactory() is deprecated and will be removed in ' + 'a future major release. Consider using JSX ' + 'or use React.createElement() directly instead.');
    } // Legacy hook: remove it


    Object.defineProperty(validatedFactory, 'type', {
      enumerable: false,
      get: function () {
        warn('Factory.type is deprecated. Access the class directly ' + 'before passing it to createFactory.');

        Object.defineProperty(this, 'type', {
          value: type
        });
        return type;
      }
    });
  }

  return validatedFactory;
}
function cloneElementWithValidation(element, props, children) {
  var newElement = cloneElement.apply(this, arguments);

  for (var i = 2; i < arguments.length; i++) {
    validateChildKeys(arguments[i], newElement.type);
  }

  validatePropTypes(newElement);
  return newElement;
}

{

  try {
    var frozenObject = Object.freeze({});
    /* eslint-disable no-new */

    new Map([[frozenObject, null]]);
    new Set([frozenObject]);
    /* eslint-enable no-new */
  } catch (e) {
  }
}

var createElement$1 =  createElementWithValidation ;
var cloneElement$1 =  cloneElementWithValidation ;
var createFactory =  createFactoryWithValidation ;
var Children = {
  map: mapChildren,
  forEach: forEachChildren,
  count: countChildren,
  toArray: toArray,
  only: onlyChild
};

exports.Children = Children;
exports.Component = Component;
exports.PureComponent = PureComponent;
exports.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = ReactSharedInternals;
exports.cloneElement = cloneElement$1;
exports.createContext = createContext;
exports.createElement = createElement$1;
exports.createFactory = createFactory;
exports.createRef = createRef;
exports.forwardRef = forwardRef;
exports.isValidElement = isValidElement;
exports.lazy = lazy;
exports.memo = memo;
exports.useCallback = useCallback;
exports.useContext = useContext;
exports.useDebugValue = useDebugValue;
exports.useEffect = useEffect;
exports.useImperativeHandle = useImperativeHandle;
exports.useLayoutEffect = useLayoutEffect;
exports.useMemo = useMemo;
exports.useReducer = useReducer;
exports.useRef = useRef;
exports.useState = useState;
exports.version = ReactVersion;
  })();
}
}(react_development));

if (process.env.NODE_ENV === 'production') {
  react.exports = react_production_min;
} else {
  react.exports = react_development;
}

var React = react.exports;

/** @license React v17.0.2
 * react-jsx-runtime.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var f=react.exports,g=60103;reactJsxRuntime_production_min.Fragment=60107;if("function"===typeof Symbol&&Symbol.for){var h=Symbol.for;g=h("react.element");reactJsxRuntime_production_min.Fragment=h("react.fragment");}var m=f.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED.ReactCurrentOwner,n=Object.prototype.hasOwnProperty,p={key:!0,ref:!0,__self:!0,__source:!0};
function q(c,a,k){var b,d={},e=null,l=null;void 0!==k&&(e=""+k);void 0!==a.key&&(e=""+a.key);void 0!==a.ref&&(l=a.ref);for(b in a)n.call(a,b)&&!p.hasOwnProperty(b)&&(d[b]=a[b]);if(c&&c.defaultProps)for(b in a=c.defaultProps,a)void 0===d[b]&&(d[b]=a[b]);return {$$typeof:g,type:c,key:e,ref:l,props:d,_owner:m.current}}reactJsxRuntime_production_min.jsx=q;reactJsxRuntime_production_min.jsxs=q;

var reactJsxRuntime_development = {};

/** @license React v17.0.2
 * react-jsx-runtime.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

(function (exports) {

if (process.env.NODE_ENV !== "production") {
  (function() {

var React = react.exports;
var _assign = objectAssign;

// ATTENTION
// When adding new symbols to this file,
// Please consider also adding to 'react-devtools-shared/src/backend/ReactSymbols'
// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var REACT_ELEMENT_TYPE = 0xeac7;
var REACT_PORTAL_TYPE = 0xeaca;
exports.Fragment = 0xeacb;
var REACT_STRICT_MODE_TYPE = 0xeacc;
var REACT_PROFILER_TYPE = 0xead2;
var REACT_PROVIDER_TYPE = 0xeacd;
var REACT_CONTEXT_TYPE = 0xeace;
var REACT_FORWARD_REF_TYPE = 0xead0;
var REACT_SUSPENSE_TYPE = 0xead1;
var REACT_SUSPENSE_LIST_TYPE = 0xead8;
var REACT_MEMO_TYPE = 0xead3;
var REACT_LAZY_TYPE = 0xead4;
var REACT_BLOCK_TYPE = 0xead9;
var REACT_SERVER_BLOCK_TYPE = 0xeada;
var REACT_FUNDAMENTAL_TYPE = 0xead5;
var REACT_DEBUG_TRACING_MODE_TYPE = 0xeae1;
var REACT_LEGACY_HIDDEN_TYPE = 0xeae3;

if (typeof Symbol === 'function' && Symbol.for) {
  var symbolFor = Symbol.for;
  REACT_ELEMENT_TYPE = symbolFor('react.element');
  REACT_PORTAL_TYPE = symbolFor('react.portal');
  exports.Fragment = symbolFor('react.fragment');
  REACT_STRICT_MODE_TYPE = symbolFor('react.strict_mode');
  REACT_PROFILER_TYPE = symbolFor('react.profiler');
  REACT_PROVIDER_TYPE = symbolFor('react.provider');
  REACT_CONTEXT_TYPE = symbolFor('react.context');
  REACT_FORWARD_REF_TYPE = symbolFor('react.forward_ref');
  REACT_SUSPENSE_TYPE = symbolFor('react.suspense');
  REACT_SUSPENSE_LIST_TYPE = symbolFor('react.suspense_list');
  REACT_MEMO_TYPE = symbolFor('react.memo');
  REACT_LAZY_TYPE = symbolFor('react.lazy');
  REACT_BLOCK_TYPE = symbolFor('react.block');
  REACT_SERVER_BLOCK_TYPE = symbolFor('react.server.block');
  REACT_FUNDAMENTAL_TYPE = symbolFor('react.fundamental');
  symbolFor('react.scope');
  symbolFor('react.opaque.id');
  REACT_DEBUG_TRACING_MODE_TYPE = symbolFor('react.debug_trace_mode');
  symbolFor('react.offscreen');
  REACT_LEGACY_HIDDEN_TYPE = symbolFor('react.legacy_hidden');
}

var MAYBE_ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
var FAUX_ITERATOR_SYMBOL = '@@iterator';
function getIteratorFn(maybeIterable) {
  if (maybeIterable === null || typeof maybeIterable !== 'object') {
    return null;
  }

  var maybeIterator = MAYBE_ITERATOR_SYMBOL && maybeIterable[MAYBE_ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL];

  if (typeof maybeIterator === 'function') {
    return maybeIterator;
  }

  return null;
}

var ReactSharedInternals = React.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;

function error(format) {
  {
    for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    printWarning('error', format, args);
  }
}

function printWarning(level, format, args) {
  // When changing this logic, you might want to also
  // update consoleWithStackDev.www.js as well.
  {
    var ReactDebugCurrentFrame = ReactSharedInternals.ReactDebugCurrentFrame;
    var stack = ReactDebugCurrentFrame.getStackAddendum();

    if (stack !== '') {
      format += '%s';
      args = args.concat([stack]);
    }

    var argsWithFormat = args.map(function (item) {
      return '' + item;
    }); // Careful: RN currently depends on this prefix

    argsWithFormat.unshift('Warning: ' + format); // We intentionally don't use spread (or .apply) directly because it
    // breaks IE9: https://github.com/facebook/react/issues/13610
    // eslint-disable-next-line react-internal/no-production-logging

    Function.prototype.apply.call(console[level], console, argsWithFormat);
  }
}

// Filter certain DOM attributes (e.g. src, href) if their values are empty strings.

var enableScopeAPI = false; // Experimental Create Event Handle API.

function isValidElementType(type) {
  if (typeof type === 'string' || typeof type === 'function') {
    return true;
  } // Note: typeof might be other than 'symbol' or 'number' (e.g. if it's a polyfill).


  if (type === exports.Fragment || type === REACT_PROFILER_TYPE || type === REACT_DEBUG_TRACING_MODE_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || type === REACT_LEGACY_HIDDEN_TYPE || enableScopeAPI ) {
    return true;
  }

  if (typeof type === 'object' && type !== null) {
    if (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_BLOCK_TYPE || type[0] === REACT_SERVER_BLOCK_TYPE) {
      return true;
    }
  }

  return false;
}

function getWrappedName(outerType, innerType, wrapperName) {
  var functionName = innerType.displayName || innerType.name || '';
  return outerType.displayName || (functionName !== '' ? wrapperName + "(" + functionName + ")" : wrapperName);
}

function getContextName(type) {
  return type.displayName || 'Context';
}

function getComponentName(type) {
  if (type == null) {
    // Host root, text node or just invalid type.
    return null;
  }

  {
    if (typeof type.tag === 'number') {
      error('Received an unexpected object in getComponentName(). ' + 'This is likely a bug in React. Please file an issue.');
    }
  }

  if (typeof type === 'function') {
    return type.displayName || type.name || null;
  }

  if (typeof type === 'string') {
    return type;
  }

  switch (type) {
    case exports.Fragment:
      return 'Fragment';

    case REACT_PORTAL_TYPE:
      return 'Portal';

    case REACT_PROFILER_TYPE:
      return 'Profiler';

    case REACT_STRICT_MODE_TYPE:
      return 'StrictMode';

    case REACT_SUSPENSE_TYPE:
      return 'Suspense';

    case REACT_SUSPENSE_LIST_TYPE:
      return 'SuspenseList';
  }

  if (typeof type === 'object') {
    switch (type.$$typeof) {
      case REACT_CONTEXT_TYPE:
        var context = type;
        return getContextName(context) + '.Consumer';

      case REACT_PROVIDER_TYPE:
        var provider = type;
        return getContextName(provider._context) + '.Provider';

      case REACT_FORWARD_REF_TYPE:
        return getWrappedName(type, type.render, 'ForwardRef');

      case REACT_MEMO_TYPE:
        return getComponentName(type.type);

      case REACT_BLOCK_TYPE:
        return getComponentName(type._render);

      case REACT_LAZY_TYPE:
        {
          var lazyComponent = type;
          var payload = lazyComponent._payload;
          var init = lazyComponent._init;

          try {
            return getComponentName(init(payload));
          } catch (x) {
            return null;
          }
        }
    }
  }

  return null;
}

// Helpers to patch console.logs to avoid logging during side-effect free
// replaying on render function. This currently only patches the object
// lazily which won't cover if the log function was extracted eagerly.
// We could also eagerly patch the method.
var disabledDepth = 0;
var prevLog;
var prevInfo;
var prevWarn;
var prevError;
var prevGroup;
var prevGroupCollapsed;
var prevGroupEnd;

function disabledLog() {}

disabledLog.__reactDisabledLog = true;
function disableLogs() {
  {
    if (disabledDepth === 0) {
      /* eslint-disable react-internal/no-production-logging */
      prevLog = console.log;
      prevInfo = console.info;
      prevWarn = console.warn;
      prevError = console.error;
      prevGroup = console.group;
      prevGroupCollapsed = console.groupCollapsed;
      prevGroupEnd = console.groupEnd; // https://github.com/facebook/react/issues/19099

      var props = {
        configurable: true,
        enumerable: true,
        value: disabledLog,
        writable: true
      }; // $FlowFixMe Flow thinks console is immutable.

      Object.defineProperties(console, {
        info: props,
        log: props,
        warn: props,
        error: props,
        group: props,
        groupCollapsed: props,
        groupEnd: props
      });
      /* eslint-enable react-internal/no-production-logging */
    }

    disabledDepth++;
  }
}
function reenableLogs() {
  {
    disabledDepth--;

    if (disabledDepth === 0) {
      /* eslint-disable react-internal/no-production-logging */
      var props = {
        configurable: true,
        enumerable: true,
        writable: true
      }; // $FlowFixMe Flow thinks console is immutable.

      Object.defineProperties(console, {
        log: _assign({}, props, {
          value: prevLog
        }),
        info: _assign({}, props, {
          value: prevInfo
        }),
        warn: _assign({}, props, {
          value: prevWarn
        }),
        error: _assign({}, props, {
          value: prevError
        }),
        group: _assign({}, props, {
          value: prevGroup
        }),
        groupCollapsed: _assign({}, props, {
          value: prevGroupCollapsed
        }),
        groupEnd: _assign({}, props, {
          value: prevGroupEnd
        })
      });
      /* eslint-enable react-internal/no-production-logging */
    }

    if (disabledDepth < 0) {
      error('disabledDepth fell below zero. ' + 'This is a bug in React. Please file an issue.');
    }
  }
}

var ReactCurrentDispatcher = ReactSharedInternals.ReactCurrentDispatcher;
var prefix;
function describeBuiltInComponentFrame(name, source, ownerFn) {
  {
    if (prefix === undefined) {
      // Extract the VM specific prefix used by each line.
      try {
        throw Error();
      } catch (x) {
        var match = x.stack.trim().match(/\n( *(at )?)/);
        prefix = match && match[1] || '';
      }
    } // We use the prefix to ensure our stacks line up with native stack frames.


    return '\n' + prefix + name;
  }
}
var reentry = false;
var componentFrameCache;

{
  var PossiblyWeakMap = typeof WeakMap === 'function' ? WeakMap : Map;
  componentFrameCache = new PossiblyWeakMap();
}

function describeNativeComponentFrame(fn, construct) {
  // If something asked for a stack inside a fake render, it should get ignored.
  if (!fn || reentry) {
    return '';
  }

  {
    var frame = componentFrameCache.get(fn);

    if (frame !== undefined) {
      return frame;
    }
  }

  var control;
  reentry = true;
  var previousPrepareStackTrace = Error.prepareStackTrace; // $FlowFixMe It does accept undefined.

  Error.prepareStackTrace = undefined;
  var previousDispatcher;

  {
    previousDispatcher = ReactCurrentDispatcher.current; // Set the dispatcher in DEV because this might be call in the render function
    // for warnings.

    ReactCurrentDispatcher.current = null;
    disableLogs();
  }

  try {
    // This should throw.
    if (construct) {
      // Something should be setting the props in the constructor.
      var Fake = function () {
        throw Error();
      }; // $FlowFixMe


      Object.defineProperty(Fake.prototype, 'props', {
        set: function () {
          // We use a throwing setter instead of frozen or non-writable props
          // because that won't throw in a non-strict mode function.
          throw Error();
        }
      });

      if (typeof Reflect === 'object' && Reflect.construct) {
        // We construct a different control for this case to include any extra
        // frames added by the construct call.
        try {
          Reflect.construct(Fake, []);
        } catch (x) {
          control = x;
        }

        Reflect.construct(fn, [], Fake);
      } else {
        try {
          Fake.call();
        } catch (x) {
          control = x;
        }

        fn.call(Fake.prototype);
      }
    } else {
      try {
        throw Error();
      } catch (x) {
        control = x;
      }

      fn();
    }
  } catch (sample) {
    // This is inlined manually because closure doesn't do it for us.
    if (sample && control && typeof sample.stack === 'string') {
      // This extracts the first frame from the sample that isn't also in the control.
      // Skipping one frame that we assume is the frame that calls the two.
      var sampleLines = sample.stack.split('\n');
      var controlLines = control.stack.split('\n');
      var s = sampleLines.length - 1;
      var c = controlLines.length - 1;

      while (s >= 1 && c >= 0 && sampleLines[s] !== controlLines[c]) {
        // We expect at least one stack frame to be shared.
        // Typically this will be the root most one. However, stack frames may be
        // cut off due to maximum stack limits. In this case, one maybe cut off
        // earlier than the other. We assume that the sample is longer or the same
        // and there for cut off earlier. So we should find the root most frame in
        // the sample somewhere in the control.
        c--;
      }

      for (; s >= 1 && c >= 0; s--, c--) {
        // Next we find the first one that isn't the same which should be the
        // frame that called our sample function and the control.
        if (sampleLines[s] !== controlLines[c]) {
          // In V8, the first line is describing the message but other VMs don't.
          // If we're about to return the first line, and the control is also on the same
          // line, that's a pretty good indicator that our sample threw at same line as
          // the control. I.e. before we entered the sample frame. So we ignore this result.
          // This can happen if you passed a class to function component, or non-function.
          if (s !== 1 || c !== 1) {
            do {
              s--;
              c--; // We may still have similar intermediate frames from the construct call.
              // The next one that isn't the same should be our match though.

              if (c < 0 || sampleLines[s] !== controlLines[c]) {
                // V8 adds a "new" prefix for native classes. Let's remove it to make it prettier.
                var _frame = '\n' + sampleLines[s].replace(' at new ', ' at ');

                {
                  if (typeof fn === 'function') {
                    componentFrameCache.set(fn, _frame);
                  }
                } // Return the line we found.


                return _frame;
              }
            } while (s >= 1 && c >= 0);
          }

          break;
        }
      }
    }
  } finally {
    reentry = false;

    {
      ReactCurrentDispatcher.current = previousDispatcher;
      reenableLogs();
    }

    Error.prepareStackTrace = previousPrepareStackTrace;
  } // Fallback to just using the name if we couldn't make it throw.


  var name = fn ? fn.displayName || fn.name : '';
  var syntheticFrame = name ? describeBuiltInComponentFrame(name) : '';

  {
    if (typeof fn === 'function') {
      componentFrameCache.set(fn, syntheticFrame);
    }
  }

  return syntheticFrame;
}
function describeFunctionComponentFrame(fn, source, ownerFn) {
  {
    return describeNativeComponentFrame(fn, false);
  }
}

function shouldConstruct(Component) {
  var prototype = Component.prototype;
  return !!(prototype && prototype.isReactComponent);
}

function describeUnknownElementTypeFrameInDEV(type, source, ownerFn) {

  if (type == null) {
    return '';
  }

  if (typeof type === 'function') {
    {
      return describeNativeComponentFrame(type, shouldConstruct(type));
    }
  }

  if (typeof type === 'string') {
    return describeBuiltInComponentFrame(type);
  }

  switch (type) {
    case REACT_SUSPENSE_TYPE:
      return describeBuiltInComponentFrame('Suspense');

    case REACT_SUSPENSE_LIST_TYPE:
      return describeBuiltInComponentFrame('SuspenseList');
  }

  if (typeof type === 'object') {
    switch (type.$$typeof) {
      case REACT_FORWARD_REF_TYPE:
        return describeFunctionComponentFrame(type.render);

      case REACT_MEMO_TYPE:
        // Memo may contain any component type so we recursively resolve it.
        return describeUnknownElementTypeFrameInDEV(type.type, source, ownerFn);

      case REACT_BLOCK_TYPE:
        return describeFunctionComponentFrame(type._render);

      case REACT_LAZY_TYPE:
        {
          var lazyComponent = type;
          var payload = lazyComponent._payload;
          var init = lazyComponent._init;

          try {
            // Lazy may contain any component type so we recursively resolve it.
            return describeUnknownElementTypeFrameInDEV(init(payload), source, ownerFn);
          } catch (x) {}
        }
    }
  }

  return '';
}

var loggedTypeFailures = {};
var ReactDebugCurrentFrame = ReactSharedInternals.ReactDebugCurrentFrame;

function setCurrentlyValidatingElement(element) {
  {
    if (element) {
      var owner = element._owner;
      var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
      ReactDebugCurrentFrame.setExtraStackFrame(stack);
    } else {
      ReactDebugCurrentFrame.setExtraStackFrame(null);
    }
  }
}

function checkPropTypes(typeSpecs, values, location, componentName, element) {
  {
    // $FlowFixMe This is okay but Flow doesn't know it.
    var has = Function.call.bind(Object.prototype.hasOwnProperty);

    for (var typeSpecName in typeSpecs) {
      if (has(typeSpecs, typeSpecName)) {
        var error$1 = void 0; // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.

        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error((componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' + 'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.' + 'This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.');
            err.name = 'Invariant Violation';
            throw err;
          }

          error$1 = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED');
        } catch (ex) {
          error$1 = ex;
        }

        if (error$1 && !(error$1 instanceof Error)) {
          setCurrentlyValidatingElement(element);

          error('%s: type specification of %s' + ' `%s` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a %s. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).', componentName || 'React class', location, typeSpecName, typeof error$1);

          setCurrentlyValidatingElement(null);
        }

        if (error$1 instanceof Error && !(error$1.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error$1.message] = true;
          setCurrentlyValidatingElement(element);

          error('Failed %s type: %s', location, error$1.message);

          setCurrentlyValidatingElement(null);
        }
      }
    }
  }
}

var ReactCurrentOwner = ReactSharedInternals.ReactCurrentOwner;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true
};
var specialPropKeyWarningShown;
var specialPropRefWarningShown;
var didWarnAboutStringRefs;

{
  didWarnAboutStringRefs = {};
}

function hasValidRef(config) {
  {
    if (hasOwnProperty.call(config, 'ref')) {
      var getter = Object.getOwnPropertyDescriptor(config, 'ref').get;

      if (getter && getter.isReactWarning) {
        return false;
      }
    }
  }

  return config.ref !== undefined;
}

function hasValidKey(config) {
  {
    if (hasOwnProperty.call(config, 'key')) {
      var getter = Object.getOwnPropertyDescriptor(config, 'key').get;

      if (getter && getter.isReactWarning) {
        return false;
      }
    }
  }

  return config.key !== undefined;
}

function warnIfStringRefCannotBeAutoConverted(config, self) {
  {
    if (typeof config.ref === 'string' && ReactCurrentOwner.current && self && ReactCurrentOwner.current.stateNode !== self) {
      var componentName = getComponentName(ReactCurrentOwner.current.type);

      if (!didWarnAboutStringRefs[componentName]) {
        error('Component "%s" contains the string ref "%s". ' + 'Support for string refs will be removed in a future major release. ' + 'This case cannot be automatically converted to an arrow function. ' + 'We ask you to manually fix this case by using useRef() or createRef() instead. ' + 'Learn more about using refs safely here: ' + 'https://reactjs.org/link/strict-mode-string-ref', getComponentName(ReactCurrentOwner.current.type), config.ref);

        didWarnAboutStringRefs[componentName] = true;
      }
    }
  }
}

function defineKeyPropWarningGetter(props, displayName) {
  {
    var warnAboutAccessingKey = function () {
      if (!specialPropKeyWarningShown) {
        specialPropKeyWarningShown = true;

        error('%s: `key` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://reactjs.org/link/special-props)', displayName);
      }
    };

    warnAboutAccessingKey.isReactWarning = true;
    Object.defineProperty(props, 'key', {
      get: warnAboutAccessingKey,
      configurable: true
    });
  }
}

function defineRefPropWarningGetter(props, displayName) {
  {
    var warnAboutAccessingRef = function () {
      if (!specialPropRefWarningShown) {
        specialPropRefWarningShown = true;

        error('%s: `ref` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://reactjs.org/link/special-props)', displayName);
      }
    };

    warnAboutAccessingRef.isReactWarning = true;
    Object.defineProperty(props, 'ref', {
      get: warnAboutAccessingRef,
      configurable: true
    });
  }
}
/**
 * Factory method to create a new React element. This no longer adheres to
 * the class pattern, so do not use new to call it. Also, instanceof check
 * will not work. Instead test $$typeof field against Symbol.for('react.element') to check
 * if something is a React Element.
 *
 * @param {*} type
 * @param {*} props
 * @param {*} key
 * @param {string|object} ref
 * @param {*} owner
 * @param {*} self A *temporary* helper to detect places where `this` is
 * different from the `owner` when React.createElement is called, so that we
 * can warn. We want to get rid of owner and replace string `ref`s with arrow
 * functions, and as long as `this` and owner are the same, there will be no
 * change in behavior.
 * @param {*} source An annotation object (added by a transpiler or otherwise)
 * indicating filename, line number, and/or other information.
 * @internal
 */


var ReactElement = function (type, key, ref, self, source, owner, props) {
  var element = {
    // This tag allows us to uniquely identify this as a React Element
    $$typeof: REACT_ELEMENT_TYPE,
    // Built-in properties that belong on the element
    type: type,
    key: key,
    ref: ref,
    props: props,
    // Record the component responsible for creating this element.
    _owner: owner
  };

  {
    // The validation flag is currently mutative. We put it on
    // an external backing store so that we can freeze the whole object.
    // This can be replaced with a WeakMap once they are implemented in
    // commonly used development environments.
    element._store = {}; // To make comparing ReactElements easier for testing purposes, we make
    // the validation flag non-enumerable (where possible, which should
    // include every environment we run tests in), so the test framework
    // ignores it.

    Object.defineProperty(element._store, 'validated', {
      configurable: false,
      enumerable: false,
      writable: true,
      value: false
    }); // self and source are DEV only properties.

    Object.defineProperty(element, '_self', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: self
    }); // Two elements created in two different places should be considered
    // equal for testing purposes and therefore we hide it from enumeration.

    Object.defineProperty(element, '_source', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: source
    });

    if (Object.freeze) {
      Object.freeze(element.props);
      Object.freeze(element);
    }
  }

  return element;
};
/**
 * https://github.com/reactjs/rfcs/pull/107
 * @param {*} type
 * @param {object} props
 * @param {string} key
 */

function jsxDEV(type, config, maybeKey, source, self) {
  {
    var propName; // Reserved names are extracted

    var props = {};
    var key = null;
    var ref = null; // Currently, key can be spread in as a prop. This causes a potential
    // issue if key is also explicitly declared (ie. <div {...props} key="Hi" />
    // or <div key="Hi" {...props} /> ). We want to deprecate key spread,
    // but as an intermediary step, we will use jsxDEV for everything except
    // <div {...props} key="Hi" />, because we aren't currently able to tell if
    // key is explicitly declared to be undefined or not.

    if (maybeKey !== undefined) {
      key = '' + maybeKey;
    }

    if (hasValidKey(config)) {
      key = '' + config.key;
    }

    if (hasValidRef(config)) {
      ref = config.ref;
      warnIfStringRefCannotBeAutoConverted(config, self);
    } // Remaining properties are added to a new props object


    for (propName in config) {
      if (hasOwnProperty.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
        props[propName] = config[propName];
      }
    } // Resolve default props


    if (type && type.defaultProps) {
      var defaultProps = type.defaultProps;

      for (propName in defaultProps) {
        if (props[propName] === undefined) {
          props[propName] = defaultProps[propName];
        }
      }
    }

    if (key || ref) {
      var displayName = typeof type === 'function' ? type.displayName || type.name || 'Unknown' : type;

      if (key) {
        defineKeyPropWarningGetter(props, displayName);
      }

      if (ref) {
        defineRefPropWarningGetter(props, displayName);
      }
    }

    return ReactElement(type, key, ref, self, source, ReactCurrentOwner.current, props);
  }
}

var ReactCurrentOwner$1 = ReactSharedInternals.ReactCurrentOwner;
var ReactDebugCurrentFrame$1 = ReactSharedInternals.ReactDebugCurrentFrame;

function setCurrentlyValidatingElement$1(element) {
  {
    if (element) {
      var owner = element._owner;
      var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
      ReactDebugCurrentFrame$1.setExtraStackFrame(stack);
    } else {
      ReactDebugCurrentFrame$1.setExtraStackFrame(null);
    }
  }
}

var propTypesMisspellWarningShown;

{
  propTypesMisspellWarningShown = false;
}
/**
 * Verifies the object is a ReactElement.
 * See https://reactjs.org/docs/react-api.html#isvalidelement
 * @param {?object} object
 * @return {boolean} True if `object` is a ReactElement.
 * @final
 */

function isValidElement(object) {
  {
    return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
  }
}

function getDeclarationErrorAddendum() {
  {
    if (ReactCurrentOwner$1.current) {
      var name = getComponentName(ReactCurrentOwner$1.current.type);

      if (name) {
        return '\n\nCheck the render method of `' + name + '`.';
      }
    }

    return '';
  }
}

function getSourceInfoErrorAddendum(source) {
  {
    if (source !== undefined) {
      var fileName = source.fileName.replace(/^.*[\\\/]/, '');
      var lineNumber = source.lineNumber;
      return '\n\nCheck your code at ' + fileName + ':' + lineNumber + '.';
    }

    return '';
  }
}
/**
 * Warn if there's no key explicitly set on dynamic arrays of children or
 * object keys are not valid. This allows us to keep track of children between
 * updates.
 */


var ownerHasKeyUseWarning = {};

function getCurrentComponentErrorInfo(parentType) {
  {
    var info = getDeclarationErrorAddendum();

    if (!info) {
      var parentName = typeof parentType === 'string' ? parentType : parentType.displayName || parentType.name;

      if (parentName) {
        info = "\n\nCheck the top-level render call using <" + parentName + ">.";
      }
    }

    return info;
  }
}
/**
 * Warn if the element doesn't have an explicit key assigned to it.
 * This element is in an array. The array could grow and shrink or be
 * reordered. All children that haven't already been validated are required to
 * have a "key" property assigned to it. Error statuses are cached so a warning
 * will only be shown once.
 *
 * @internal
 * @param {ReactElement} element Element that requires a key.
 * @param {*} parentType element's parent's type.
 */


function validateExplicitKey(element, parentType) {
  {
    if (!element._store || element._store.validated || element.key != null) {
      return;
    }

    element._store.validated = true;
    var currentComponentErrorInfo = getCurrentComponentErrorInfo(parentType);

    if (ownerHasKeyUseWarning[currentComponentErrorInfo]) {
      return;
    }

    ownerHasKeyUseWarning[currentComponentErrorInfo] = true; // Usually the current owner is the offender, but if it accepts children as a
    // property, it may be the creator of the child that's responsible for
    // assigning it a key.

    var childOwner = '';

    if (element && element._owner && element._owner !== ReactCurrentOwner$1.current) {
      // Give the component that originally created this child.
      childOwner = " It was passed a child from " + getComponentName(element._owner.type) + ".";
    }

    setCurrentlyValidatingElement$1(element);

    error('Each child in a list should have a unique "key" prop.' + '%s%s See https://reactjs.org/link/warning-keys for more information.', currentComponentErrorInfo, childOwner);

    setCurrentlyValidatingElement$1(null);
  }
}
/**
 * Ensure that every element either is passed in a static location, in an
 * array with an explicit keys property defined, or in an object literal
 * with valid key property.
 *
 * @internal
 * @param {ReactNode} node Statically passed child of any type.
 * @param {*} parentType node's parent's type.
 */


function validateChildKeys(node, parentType) {
  {
    if (typeof node !== 'object') {
      return;
    }

    if (Array.isArray(node)) {
      for (var i = 0; i < node.length; i++) {
        var child = node[i];

        if (isValidElement(child)) {
          validateExplicitKey(child, parentType);
        }
      }
    } else if (isValidElement(node)) {
      // This element was passed in a valid location.
      if (node._store) {
        node._store.validated = true;
      }
    } else if (node) {
      var iteratorFn = getIteratorFn(node);

      if (typeof iteratorFn === 'function') {
        // Entry iterators used to provide implicit keys,
        // but now we print a separate warning for them later.
        if (iteratorFn !== node.entries) {
          var iterator = iteratorFn.call(node);
          var step;

          while (!(step = iterator.next()).done) {
            if (isValidElement(step.value)) {
              validateExplicitKey(step.value, parentType);
            }
          }
        }
      }
    }
  }
}
/**
 * Given an element, validate that its props follow the propTypes definition,
 * provided by the type.
 *
 * @param {ReactElement} element
 */


function validatePropTypes(element) {
  {
    var type = element.type;

    if (type === null || type === undefined || typeof type === 'string') {
      return;
    }

    var propTypes;

    if (typeof type === 'function') {
      propTypes = type.propTypes;
    } else if (typeof type === 'object' && (type.$$typeof === REACT_FORWARD_REF_TYPE || // Note: Memo only checks outer props here.
    // Inner props are checked in the reconciler.
    type.$$typeof === REACT_MEMO_TYPE)) {
      propTypes = type.propTypes;
    } else {
      return;
    }

    if (propTypes) {
      // Intentionally inside to avoid triggering lazy initializers:
      var name = getComponentName(type);
      checkPropTypes(propTypes, element.props, 'prop', name, element);
    } else if (type.PropTypes !== undefined && !propTypesMisspellWarningShown) {
      propTypesMisspellWarningShown = true; // Intentionally inside to avoid triggering lazy initializers:

      var _name = getComponentName(type);

      error('Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?', _name || 'Unknown');
    }

    if (typeof type.getDefaultProps === 'function' && !type.getDefaultProps.isReactClassApproved) {
      error('getDefaultProps is only used on classic React.createClass ' + 'definitions. Use a static property named `defaultProps` instead.');
    }
  }
}
/**
 * Given a fragment, validate that it can only be provided with fragment props
 * @param {ReactElement} fragment
 */


function validateFragmentProps(fragment) {
  {
    var keys = Object.keys(fragment.props);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];

      if (key !== 'children' && key !== 'key') {
        setCurrentlyValidatingElement$1(fragment);

        error('Invalid prop `%s` supplied to `React.Fragment`. ' + 'React.Fragment can only have `key` and `children` props.', key);

        setCurrentlyValidatingElement$1(null);
        break;
      }
    }

    if (fragment.ref !== null) {
      setCurrentlyValidatingElement$1(fragment);

      error('Invalid attribute `ref` supplied to `React.Fragment`.');

      setCurrentlyValidatingElement$1(null);
    }
  }
}

function jsxWithValidation(type, props, key, isStaticChildren, source, self) {
  {
    var validType = isValidElementType(type); // We warn in this case but don't throw. We expect the element creation to
    // succeed and there will likely be errors in render.

    if (!validType) {
      var info = '';

      if (type === undefined || typeof type === 'object' && type !== null && Object.keys(type).length === 0) {
        info += ' You likely forgot to export your component from the file ' + "it's defined in, or you might have mixed up default and named imports.";
      }

      var sourceInfo = getSourceInfoErrorAddendum(source);

      if (sourceInfo) {
        info += sourceInfo;
      } else {
        info += getDeclarationErrorAddendum();
      }

      var typeString;

      if (type === null) {
        typeString = 'null';
      } else if (Array.isArray(type)) {
        typeString = 'array';
      } else if (type !== undefined && type.$$typeof === REACT_ELEMENT_TYPE) {
        typeString = "<" + (getComponentName(type.type) || 'Unknown') + " />";
        info = ' Did you accidentally export a JSX literal instead of a component?';
      } else {
        typeString = typeof type;
      }

      error('React.jsx: type is invalid -- expected a string (for ' + 'built-in components) or a class/function (for composite ' + 'components) but got: %s.%s', typeString, info);
    }

    var element = jsxDEV(type, props, key, source, self); // The result can be nullish if a mock or a custom function is used.
    // TODO: Drop this when these are no longer allowed as the type argument.

    if (element == null) {
      return element;
    } // Skip key warning if the type isn't valid since our key validation logic
    // doesn't expect a non-string/function type and can throw confusing errors.
    // We don't want exception behavior to differ between dev and prod.
    // (Rendering will throw with a helpful message and as soon as the type is
    // fixed, the key warnings will appear.)


    if (validType) {
      var children = props.children;

      if (children !== undefined) {
        if (isStaticChildren) {
          if (Array.isArray(children)) {
            for (var i = 0; i < children.length; i++) {
              validateChildKeys(children[i], type);
            }

            if (Object.freeze) {
              Object.freeze(children);
            }
          } else {
            error('React.jsx: Static children should always be an array. ' + 'You are likely explicitly calling React.jsxs or React.jsxDEV. ' + 'Use the Babel transform instead.');
          }
        } else {
          validateChildKeys(children, type);
        }
      }
    }

    if (type === exports.Fragment) {
      validateFragmentProps(element);
    } else {
      validatePropTypes(element);
    }

    return element;
  }
} // These two functions exist to still get child warnings in dev
// even with the prod transform. This means that jsxDEV is purely
// opt-in behavior for better messages but that we won't stop
// giving you warnings if you use production apis.

function jsxWithValidationStatic(type, props, key) {
  {
    return jsxWithValidation(type, props, key, true);
  }
}
function jsxWithValidationDynamic(type, props, key) {
  {
    return jsxWithValidation(type, props, key, false);
  }
}

var jsx =  jsxWithValidationDynamic ; // we may want to special case jsxs internally to take advantage of static children.
// for now we can ship identical prod functions

var jsxs =  jsxWithValidationStatic ;

exports.jsx = jsx;
exports.jsxs = jsxs;
  })();
}
}(reactJsxRuntime_development));

if (process.env.NODE_ENV === 'production') {
  jsxRuntime.exports = reactJsxRuntime_production_min;
} else {
  jsxRuntime.exports = reactJsxRuntime_development;
}

Object.defineProperty(AbsoluteFill$1, "__esModule", { value: true });
AbsoluteFill$1.AbsoluteFill = void 0;
const jsx_runtime_1$b = jsxRuntime.exports;
const react_1$h = react.exports;
const AbsoluteFill = (props) => {
    const { style, ...other } = props;
    const actualStyle = (0, react_1$h.useMemo)(() => {
        return {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            ...style,
        };
    }, [style]);
    return (0, jsx_runtime_1$b.jsx)("div", Object.assign({ style: actualStyle }, other), void 0);
};
AbsoluteFill$1.AbsoluteFill = AbsoluteFill;

var audio = {};

var Audio = {};

var getEnvironment = {};

Object.defineProperty(getEnvironment, "__esModule", { value: true });
getEnvironment.getRemotionEnvironment = void 0;
const getRemotionEnvironment = () => {
    if (process.env.NODE_ENV === 'production') {
        if (typeof window !== 'undefined' && window.remotion_isPlayer) {
            return 'player-production';
        }
        return 'rendering';
    }
    // The Jest framework sets NODE_ENV as test.
    // Right now we don't need to treat it in a special
    // way which is good - defaulting to `rendering`.
    if (process.env.NODE_ENV === 'test') {
        return 'rendering';
    }
    if (typeof window !== 'undefined' && window.remotion_isPlayer) {
        return 'player-development';
    }
    return 'preview';
};
getEnvironment.getRemotionEnvironment = getRemotionEnvironment;

var sequencing = {};

var CompositionManager = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompositionManagerProvider = exports.CompositionManager = void 0;
const jsx_runtime_1 = jsxRuntime.exports;
const react_1 = react.exports;
exports.CompositionManager = (0, react_1.createContext)({
    compositions: [],
    registerComposition: () => undefined,
    unregisterComposition: () => undefined,
    currentComposition: null,
    setCurrentComposition: () => undefined,
    registerSequence: () => undefined,
    unregisterSequence: () => undefined,
    registerAsset: () => undefined,
    unregisterAsset: () => undefined,
    sequences: [],
    assets: [],
});
const CompositionManagerProvider = ({ children }) => {
    // Wontfix, expected to have
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const [compositions, setCompositions] = (0, react_1.useState)([]);
    const [currentComposition, setCurrentComposition] = (0, react_1.useState)(null);
    const [assets, setAssets] = (0, react_1.useState)([]);
    const [sequences, setSequences] = (0, react_1.useState)([]);
    const registerComposition = (0, react_1.useCallback)((comp) => {
        setCompositions((comps) => {
            if (comps.find((c) => c.id === comp.id)) {
                throw new Error(`Multiple composition with id ${comp.id} are registered.`);
            }
            return [...comps, comp].slice().sort((a, b) => a.nonce - b.nonce);
        });
    }, []);
    const registerSequence = (0, react_1.useCallback)((seq) => {
        setSequences((seqs) => {
            return [...seqs, seq];
        });
    }, []);
    const unregisterComposition = (0, react_1.useCallback)((id) => {
        setCompositions((comps) => {
            return comps.filter((c) => c.id !== id);
        });
    }, []);
    const unregisterSequence = (0, react_1.useCallback)((seq) => {
        setSequences((seqs) => seqs.filter((s) => s.id !== seq));
    }, []);
    const registerAsset = (0, react_1.useCallback)((asset) => {
        setAssets((assts) => {
            return [...assts, asset];
        });
    }, []);
    const unregisterAsset = (0, react_1.useCallback)((id) => {
        setAssets((assts) => {
            return assts.filter((a) => a.id !== id);
        });
    }, []);
    (0, react_1.useLayoutEffect)(() => {
        if (typeof window !== 'undefined') {
            window.remotion_collectAssets = () => {
                setAssets([]); // clear assets at next render
                return assets;
            };
        }
    }, [assets]);
    const contextValue = (0, react_1.useMemo)(() => {
        return {
            compositions,
            registerComposition,
            unregisterComposition,
            currentComposition,
            setCurrentComposition,
            registerSequence,
            unregisterSequence,
            registerAsset,
            unregisterAsset,
            sequences,
            assets,
        };
    }, [
        compositions,
        currentComposition,
        registerComposition,
        registerSequence,
        unregisterComposition,
        unregisterSequence,
        registerAsset,
        unregisterAsset,
        sequences,
        assets,
    ]);
    return ((0, jsx_runtime_1.jsx)(exports.CompositionManager.Provider, Object.assign({ value: contextValue }, { children: children }), void 0));
};
exports.CompositionManagerProvider = CompositionManagerProvider;

}(CompositionManager));

var getTimelineClipName = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTimelineClipName = void 0;
const react_1 = react.exports;
const getTimelineClipName = (children) => {
    var _a;
    const tree = (_a = react_1.Children.map(children, (ch) => {
        if (!(0, react_1.isValidElement)(ch)) {
            return null;
        }
        // Must be name, not ID
        const name = typeof ch.type !== 'string' && ch.type.name;
        if (name) {
            return name;
        }
        if (ch.props.children) {
            const chName = (0, exports.getTimelineClipName)(ch.props.children);
            return chName;
        }
        return null;
    })) === null || _a === void 0 ? void 0 : _a.filter(Boolean);
    return (tree === null || tree === void 0 ? void 0 : tree.length) ? tree[0] : '';
};
exports.getTimelineClipName = getTimelineClipName;

}(getTimelineClipName));

var nonce = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.useNonce = exports.NonceContext = void 0;
const react_1 = react.exports;
exports.NonceContext = (0, react_1.createContext)({
    getNonce: () => 0,
    fastRefreshes: 0,
});
const useNonce = () => {
    const context = (0, react_1.useContext)(exports.NonceContext);
    const [nonce, setNonce] = (0, react_1.useState)(() => context.getNonce());
    (0, react_1.useEffect)(() => {
        setNonce(context.getNonce);
    }, [context]);
    return nonce;
};
exports.useNonce = useNonce;

}(nonce));

var timelinePositionState = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.usePlayingState = exports.useTimelineSetFrame = exports.useTimelinePosition = exports.SetTimelineContext = exports.TimelineContext = void 0;
const react_1 = react.exports;
exports.TimelineContext = (0, react_1.createContext)({
    frame: 0,
    playing: false,
    playbackRate: 1,
    rootId: '',
    imperativePlaying: {
        current: false,
    },
    setPlaybackRate: () => {
        throw new Error('default');
    },
    audioAndVideoTags: { current: [] },
});
exports.SetTimelineContext = (0, react_1.createContext)({
    setFrame: () => {
        throw new Error('default');
    },
    setPlaying: () => {
        throw new Error('default');
    },
});
const useTimelinePosition = () => {
    const state = (0, react_1.useContext)(exports.TimelineContext);
    return state.frame;
};
exports.useTimelinePosition = useTimelinePosition;
const useTimelineSetFrame = () => {
    const { setFrame } = (0, react_1.useContext)(exports.SetTimelineContext);
    return setFrame;
};
exports.useTimelineSetFrame = useTimelineSetFrame;
const usePlayingState = () => {
    const { playing, imperativePlaying } = (0, react_1.useContext)(exports.TimelineContext);
    const { setPlaying } = (0, react_1.useContext)(exports.SetTimelineContext);
    return (0, react_1.useMemo)(() => [playing, setPlaying, imperativePlaying], [imperativePlaying, playing, setPlaying]);
};
exports.usePlayingState = usePlayingState;

}(timelinePositionState));

var useFrame = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.useCurrentFrame = exports.useAbsoluteCurrentFrame = void 0;
const react_1 = react.exports;
const sequencing_1 = sequencing;
const timeline_position_state_1 = timelinePositionState;
const useAbsoluteCurrentFrame = () => {
    const timelinePosition = (0, timeline_position_state_1.useTimelinePosition)();
    return timelinePosition;
};
exports.useAbsoluteCurrentFrame = useAbsoluteCurrentFrame;
const useCurrentFrame = () => {
    const frame = (0, exports.useAbsoluteCurrentFrame)();
    const context = (0, react_1.useContext)(sequencing_1.SequenceContext);
    const contextOffset = context
        ? context.cumulatedFrom + context.relativeFrom
        : 0;
    return frame - contextOffset;
};
exports.useCurrentFrame = useCurrentFrame;

}(useFrame));

var useUnsafeVideoConfig$1 = {};

var useVideo$1 = {};

Object.defineProperty(useVideo$1, "__esModule", { value: true });
useVideo$1.useVideo = void 0;
const react_1$g = react.exports;
const CompositionManager_1$6 = CompositionManager;
const useVideo = () => {
    const context = (0, react_1$g.useContext)(CompositionManager_1$6.CompositionManager);
    return (0, react_1$g.useMemo)(() => {
        var _a;
        return (_a = context.compositions.find((c) => {
            return c.id === context.currentComposition;
        })) !== null && _a !== void 0 ? _a : null;
    }, [context.compositions, context.currentComposition]);
};
useVideo$1.useVideo = useVideo;

Object.defineProperty(useUnsafeVideoConfig$1, "__esModule", { value: true });
useUnsafeVideoConfig$1.useUnsafeVideoConfig = void 0;
const react_1$f = react.exports;
const sequencing_1$6 = sequencing;
const use_video_1$1 = useVideo$1;
const useUnsafeVideoConfig = () => {
    var _a;
    const context = (0, react_1$f.useContext)(sequencing_1$6.SequenceContext);
    const ctxDuration = (_a = context === null || context === void 0 ? void 0 : context.durationInFrames) !== null && _a !== void 0 ? _a : null;
    const video = (0, use_video_1$1.useVideo)();
    return (0, react_1$f.useMemo)(() => {
        if (!video) {
            return null;
        }
        const { durationInFrames, fps, height, width } = video;
        return {
            width,
            height,
            fps,
            durationInFrames: ctxDuration !== null && ctxDuration !== void 0 ? ctxDuration : durationInFrames,
        };
    }, [ctxDuration, video]);
};
useUnsafeVideoConfig$1.useUnsafeVideoConfig = useUnsafeVideoConfig;

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.Sequence = exports.SequenceContext = void 0;
const jsx_runtime_1 = jsxRuntime.exports;
const react_1 = react.exports;
const CompositionManager_1 = CompositionManager;
const get_timeline_clip_name_1 = getTimelineClipName;
const nonce_1 = nonce;
const timeline_position_state_1 = timelinePositionState;
const use_frame_1 = useFrame;
const use_unsafe_video_config_1 = useUnsafeVideoConfig$1;
exports.SequenceContext = (0, react_1.createContext)(null);
const Sequence = ({ from, durationInFrames = Infinity, children, name, layout = 'absolute-fill', showInTimeline = true, showLoopTimesInTimeline, }) => {
    const [id] = (0, react_1.useState)(() => String(Math.random()));
    const parentSequence = (0, react_1.useContext)(exports.SequenceContext);
    const { rootId } = (0, react_1.useContext)(timeline_position_state_1.TimelineContext);
    const cumulatedFrom = parentSequence
        ? parentSequence.cumulatedFrom + parentSequence.relativeFrom
        : 0;
    const actualFrom = cumulatedFrom + from;
    const nonce = (0, nonce_1.useNonce)();
    if (layout !== 'absolute-fill' && layout !== 'none') {
        throw new TypeError(`The layout prop of <Sequence /> expects either "absolute-fill" or "none", but you passed: ${layout}`);
    }
    if (typeof durationInFrames !== 'number') {
        throw new TypeError(`You passed to durationInFrames an argument of type ${typeof durationInFrames}, but it must be a number.`);
    }
    if (durationInFrames <= 0) {
        throw new TypeError(`durationInFrames must be positive, but got ${durationInFrames}`);
    }
    // Infinity is non-integer but allowed!
    if (durationInFrames % 1 !== 0 && Number.isFinite(durationInFrames)) {
        throw new TypeError(`The "durationInFrames" of a sequence must be an integer, but got ${durationInFrames}.`);
    }
    if (typeof from !== 'number') {
        throw new TypeError(`You passed to the "from" props of your <Sequence> an argument of type ${typeof from}, but it must be a number.`);
    }
    if (from % 1 !== 0) {
        throw new TypeError(`The "from" prop of a sequence must be an integer, but got ${from}.`);
    }
    const absoluteFrame = (0, use_frame_1.useAbsoluteCurrentFrame)();
    const unsafeVideoConfig = (0, use_unsafe_video_config_1.useUnsafeVideoConfig)();
    const compositionDuration = unsafeVideoConfig
        ? unsafeVideoConfig.durationInFrames
        : 0;
    const actualDurationInFrames = Math.min(compositionDuration - from, parentSequence
        ? Math.min(parentSequence.durationInFrames +
            (parentSequence.cumulatedFrom + parentSequence.relativeFrom) -
            actualFrom, durationInFrames)
        : durationInFrames);
    const { registerSequence, unregisterSequence } = (0, react_1.useContext)(CompositionManager_1.CompositionManager);
    const contextValue = (0, react_1.useMemo)(() => {
        var _a;
        return {
            cumulatedFrom,
            relativeFrom: from,
            durationInFrames: actualDurationInFrames,
            parentFrom: (_a = parentSequence === null || parentSequence === void 0 ? void 0 : parentSequence.relativeFrom) !== null && _a !== void 0 ? _a : 0,
            id,
        };
    }, [
        cumulatedFrom,
        from,
        actualDurationInFrames,
        parentSequence === null || parentSequence === void 0 ? void 0 : parentSequence.relativeFrom,
        id,
    ]);
    const timelineClipName = (0, react_1.useMemo)(() => {
        return name !== null && name !== void 0 ? name : (0, get_timeline_clip_name_1.getTimelineClipName)(children);
    }, [children, name]);
    (0, react_1.useEffect)(() => {
        var _a;
        registerSequence({
            from,
            duration: actualDurationInFrames,
            id,
            displayName: timelineClipName,
            parent: (_a = parentSequence === null || parentSequence === void 0 ? void 0 : parentSequence.id) !== null && _a !== void 0 ? _a : null,
            type: 'sequence',
            rootId,
            showInTimeline,
            nonce,
            showLoopTimesInTimeline,
        });
        return () => {
            unregisterSequence(id);
        };
    }, [
        durationInFrames,
        actualFrom,
        id,
        name,
        registerSequence,
        timelineClipName,
        unregisterSequence,
        parentSequence === null || parentSequence === void 0 ? void 0 : parentSequence.id,
        actualDurationInFrames,
        rootId,
        from,
        showInTimeline,
        nonce,
        showLoopTimesInTimeline,
    ]);
    const endThreshold = (() => {
        return actualFrom + durationInFrames - 1;
    })();
    const content = absoluteFrame < actualFrom
        ? null
        : absoluteFrame > endThreshold
            ? null
            : children;
    return ((0, jsx_runtime_1.jsx)(exports.SequenceContext.Provider, Object.assign({ value: contextValue }, { children: content === null ? null : layout === 'absolute-fill' ? ((0, jsx_runtime_1.jsx)("div", Object.assign({ style: {
                position: 'absolute',
                display: 'flex',
                width: '100%',
                height: '100%',
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
            } }, { children: content }), void 0)) : (content) }), void 0));
};
exports.Sequence = Sequence;

}(sequencing));

var validateMediaProps$1 = {};

Object.defineProperty(validateMediaProps$1, "__esModule", { value: true });
validateMediaProps$1.validateMediaProps = void 0;
const validateMediaProps = (props, component) => {
    if (typeof props.volume !== 'number' &&
        typeof props.volume !== 'function' &&
        typeof props.volume !== 'undefined') {
        throw new TypeError(`You have passed a volume of type ${typeof props.volume} to your <${component} /> component. Volume must be a number or a function with the signature '(frame: number) => number' undefined.`);
    }
    if (typeof props.volume === 'number' && props.volume < 0) {
        throw new TypeError(`You have passed a volume below 0 to your <${component} /> component. Volume must be between 0 and 1`);
    }
    if (typeof props.playbackRate !== 'number' &&
        typeof props.playbackRate !== 'undefined') {
        throw new TypeError(`You have passed a playbackRate of type ${typeof props.playbackRate} to your <${component} /> component. Playback rate must a real number or undefined.`);
    }
    if (typeof props.playbackRate === 'number' &&
        (isNaN(props.playbackRate) ||
            !Number.isFinite(props.playbackRate) ||
            props.playbackRate <= 0)) {
        throw new TypeError(`You have passed a playbackRate of ${props.playbackRate} to your <${component} /> component. Playback rate must be a real number above 0.`);
    }
};
validateMediaProps$1.validateMediaProps = validateMediaProps;

var validateStartFromProps$1 = {};

Object.defineProperty(validateStartFromProps$1, "__esModule", { value: true });
validateStartFromProps$1.validateStartFromProps = void 0;
const validateStartFromProps = (startFrom, endAt) => {
    if (typeof startFrom !== 'undefined') {
        if (typeof startFrom !== 'number') {
            throw new TypeError(`type of startFrom prop must be a number, instead got type ${typeof startFrom}.`);
        }
        if (isNaN(startFrom) || startFrom === Infinity) {
            throw new TypeError('startFrom prop can not be NaN or Infinity.');
        }
        if (startFrom < 0) {
            throw new TypeError(`startFrom must be greater than equal to 0 instead got ${startFrom}.`);
        }
    }
    if (typeof endAt !== 'undefined') {
        if (typeof endAt !== 'number') {
            throw new TypeError(`type of endAt prop must be a number, instead got type ${typeof endAt}.`);
        }
        if (isNaN(endAt)) {
            throw new TypeError('endAt prop can not be NaN.');
        }
        if (endAt <= 0) {
            throw new TypeError(`endAt must be a positive number, instead got ${endAt}.`);
        }
    }
    if (endAt < startFrom) {
        throw new TypeError('endAt prop must be greater than startFrom prop.');
    }
};
validateStartFromProps$1.validateStartFromProps = validateStartFromProps;

var AudioForDevelopment = {};

var useMediaInTimeline$1 = {};

var useAudioFrame = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.useFrameForVolumeProp = exports.useMediaStartsAt = void 0;
const react_1 = react.exports;
const sequencing_1 = sequencing;
const use_frame_1 = useFrame;
const useMediaStartsAt = () => {
    var _a;
    const parentSequence = (0, react_1.useContext)(sequencing_1.SequenceContext);
    const startsAt = Math.min(0, (_a = parentSequence === null || parentSequence === void 0 ? void 0 : parentSequence.relativeFrom) !== null && _a !== void 0 ? _a : 0);
    return startsAt;
};
exports.useMediaStartsAt = useMediaStartsAt;
/**
 * When passing a function as the prop for `volume`,
 * we calculate the way more intuitive value for currentFrame
 */
const useFrameForVolumeProp = () => {
    const frame = (0, use_frame_1.useCurrentFrame)();
    const startsAt = (0, exports.useMediaStartsAt)();
    return frame + startsAt;
};
exports.useFrameForVolumeProp = useFrameForVolumeProp;

}(useAudioFrame));

var getAssetFileName = {};

Object.defineProperty(getAssetFileName, "__esModule", { value: true });
getAssetFileName.getAssetDisplayName = void 0;
const getAssetDisplayName = (filename) => {
    if (/data:|blob:/.test(filename.substring(0, 5))) {
        return 'Data URL';
    }
    const splitted = filename
        .split('/')
        .map((s) => s.split('\\'))
        .flat(1);
    return splitted[splitted.length - 1];
};
getAssetFileName.getAssetDisplayName = getAssetDisplayName;

var playAndHandleNotAllowedError$1 = {};

Object.defineProperty(playAndHandleNotAllowedError$1, "__esModule", { value: true });
playAndHandleNotAllowedError$1.playAndHandleNotAllowedError = void 0;
const playAndHandleNotAllowedError = (mediaRef, mediaType) => {
    const { current } = mediaRef;
    const prom = current === null || current === void 0 ? void 0 : current.play();
    if (prom === null || prom === void 0 ? void 0 : prom.catch) {
        prom === null || prom === void 0 ? void 0 : prom.catch((err) => {
            if (!current) {
                return;
            }
            // Pause was called after play in Chrome
            if (err.message.includes('request was interrupted by a call to pause')) {
                return;
            }
            // Pause was called after play in Safari
            if (err.message.includes('The operation was aborted.')) {
                return;
            }
            // Pause was called after play in Firefox
            if (err.message.includes('The fetching process for the media resource was aborted by the user agent')) {
                return;
            }
            console.log(`Could not play ${mediaType} due to following error: `, err);
            if (!current.muted) {
                console.log(`The video will be muted and we'll retry playing it.`, err);
                current.muted = true;
                current.play();
            }
        });
    }
};
playAndHandleNotAllowedError$1.playAndHandleNotAllowedError = playAndHandleNotAllowedError;

var useVideoConfig$1 = {};

Object.defineProperty(useVideoConfig$1, "__esModule", { value: true });
useVideoConfig$1.useVideoConfig = void 0;
const use_unsafe_video_config_1$2 = useUnsafeVideoConfig$1;
const useVideoConfig = () => {
    const videoConfig = (0, use_unsafe_video_config_1$2.useUnsafeVideoConfig)();
    if (!videoConfig) {
        throw new Error('No video config found. You are probably calling useVideoConfig() from a component which has not been registered as a <Composition />. See https://www.remotion.dev/docs/the-fundamentals#defining-compositions for more information.');
    }
    return videoConfig;
};
useVideoConfig$1.useVideoConfig = useVideoConfig;

var volumeProp = {};

Object.defineProperty(volumeProp, "__esModule", { value: true });
volumeProp.evaluateVolume = void 0;
const evaluateVolume = ({ frame, volume, mediaVolume = 1, }) => {
    if (typeof volume === 'number') {
        return Math.min(1, volume * mediaVolume);
    }
    if (typeof volume === 'undefined') {
        return Number(mediaVolume);
    }
    const evaluated = volume(frame) * mediaVolume;
    if (typeof evaluated !== 'number') {
        throw new TypeError(`You passed in a a function to the volume prop but it did not return a number but a value of type ${typeof evaluated} for frame ${frame}`);
    }
    if (Number.isNaN(evaluated)) {
        throw new TypeError(`You passed in a function to the volume prop but it returned NaN for frame ${frame}.`);
    }
    if (!Number.isFinite(evaluated)) {
        throw new TypeError(`You passed in a function to the volume prop but it returned a non-finite number for frame ${frame}.`);
    }
    return Math.min(1, evaluated);
};
volumeProp.evaluateVolume = evaluateVolume;

Object.defineProperty(useMediaInTimeline$1, "__esModule", { value: true });
useMediaInTimeline$1.useMediaInTimeline = void 0;
const react_1$e = react.exports;
const use_audio_frame_1$5 = useAudioFrame;
const CompositionManager_1$5 = CompositionManager;
const get_asset_file_name_1 = getAssetFileName;
const nonce_1$2 = nonce;
const play_and_handle_not_allowed_error_1$1 = playAndHandleNotAllowedError$1;
const sequencing_1$5 = sequencing;
const timeline_position_state_1$3 = timelinePositionState;
const use_video_config_1$1 = useVideoConfig$1;
const volume_prop_1$3 = volumeProp;
const didWarn = {};
const warnOnce = (message) => {
    if (didWarn[message]) {
        return;
    }
    console.warn(message);
    didWarn[message] = true;
};
const useMediaInTimeline = ({ volume, mediaVolume, mediaRef, src, mediaType, }) => {
    const videoConfig = (0, use_video_config_1$1.useVideoConfig)();
    const { rootId, audioAndVideoTags } = (0, react_1$e.useContext)(timeline_position_state_1$3.TimelineContext);
    const parentSequence = (0, react_1$e.useContext)(sequencing_1$5.SequenceContext);
    const actualFrom = parentSequence
        ? parentSequence.relativeFrom + parentSequence.cumulatedFrom
        : 0;
    const startsAt = (0, use_audio_frame_1$5.useMediaStartsAt)();
    const { registerSequence, unregisterSequence } = (0, react_1$e.useContext)(CompositionManager_1$5.CompositionManager);
    const [id] = (0, react_1$e.useState)(() => String(Math.random()));
    const [initialVolume] = (0, react_1$e.useState)(() => volume);
    const nonce = (0, nonce_1$2.useNonce)();
    const duration = (() => {
        return parentSequence
            ? Math.min(parentSequence.durationInFrames, videoConfig.durationInFrames)
            : videoConfig.durationInFrames;
    })();
    const doesVolumeChange = typeof volume === 'function';
    const volumes = (0, react_1$e.useMemo)(() => {
        if (typeof volume === 'number') {
            return volume;
        }
        return new Array(Math.max(0, duration + startsAt))
            .fill(true)
            .map((_, i) => {
            return (0, volume_prop_1$3.evaluateVolume)({
                frame: i + startsAt,
                volume,
                mediaVolume,
            });
        })
            .join(',');
    }, [duration, startsAt, volume, mediaVolume]);
    (0, react_1$e.useEffect)(() => {
        if (typeof volume === 'number' && volume !== initialVolume) {
            warnOnce(`Remotion: The ${mediaType} with src ${src} has changed it's volume. Prefer the callback syntax for setting volume to get better timeline display: https://www.remotion.dev/docs/using-audio/#controlling-volume`);
        }
    }, [initialVolume, mediaType, src, volume]);
    (0, react_1$e.useEffect)(() => {
        var _a;
        if (!mediaRef.current) {
            return;
        }
        if (!src) {
            throw new Error('No src passed');
        }
        registerSequence({
            type: mediaType,
            src,
            id,
            // TODO: Cap to media duration
            duration,
            from: 0,
            parent: (_a = parentSequence === null || parentSequence === void 0 ? void 0 : parentSequence.id) !== null && _a !== void 0 ? _a : null,
            displayName: (0, get_asset_file_name_1.getAssetDisplayName)(src),
            rootId,
            volume: volumes,
            showInTimeline: true,
            nonce,
            startMediaFrom: 0 - startsAt,
            doesVolumeChange,
            showLoopTimesInTimeline: undefined,
        });
        return () => {
            unregisterSequence(id);
        };
    }, [
        actualFrom,
        duration,
        id,
        parentSequence,
        src,
        registerSequence,
        rootId,
        unregisterSequence,
        videoConfig,
        volumes,
        doesVolumeChange,
        nonce,
        mediaRef,
        mediaType,
        startsAt,
    ]);
    (0, react_1$e.useEffect)(() => {
        const tag = {
            id,
            play: () => (0, play_and_handle_not_allowed_error_1$1.playAndHandleNotAllowedError)(mediaRef, mediaType),
        };
        audioAndVideoTags.current.push(tag);
        return () => {
            audioAndVideoTags.current = audioAndVideoTags.current.filter((a) => a.id !== id);
        };
    }, [audioAndVideoTags, id, mediaRef, mediaType]);
};
useMediaInTimeline$1.useMediaInTimeline = useMediaInTimeline;

var useMediaPlayback$1 = {};

var getCurrentTime = {};

var interpolate$1 = {};

// Taken from https://github.com/facebook/react-native/blob/0b9ea60b4fee8cacc36e7160e31b91fc114dbc0d/Libraries/Animated/src/nodes/AnimatedInterpolation.js
Object.defineProperty(interpolate$1, "__esModule", { value: true });
interpolate$1.interpolate = void 0;
function interpolateFunction(input, inputRange, outputRange, options) {
    const { extrapolateLeft, extrapolateRight, easing } = options;
    let result = input;
    const [inputMin, inputMax] = inputRange;
    const [outputMin, outputMax] = outputRange;
    if (result < inputMin) {
        if (extrapolateLeft === 'identity') {
            return result;
        }
        if (extrapolateLeft === 'clamp') {
            result = inputMin;
        }
    }
    if (result > inputMax) {
        if (extrapolateRight === 'identity') {
            return result;
        }
        if (extrapolateRight === 'clamp') {
            result = inputMax;
        }
    }
    if (outputMin === outputMax) {
        return outputMin;
    }
    // Input Range
    result = (result - inputMin) / (inputMax - inputMin);
    // Easing
    result = easing(result);
    // Output Range
    result = result * (outputMax - outputMin) + outputMin;
    return result;
}
function findRange(input, inputRange) {
    let i;
    for (i = 1; i < inputRange.length - 1; ++i) {
        if (inputRange[i] >= input) {
            break;
        }
    }
    return i - 1;
}
function checkValidInputRange(arr) {
    for (let i = 1; i < arr.length; ++i) {
        if (!(arr[i] > arr[i - 1])) {
            throw new Error(`inputRange must be strictly monotonically non-decreasing but got [${arr.join(',')}]`);
        }
    }
}
function checkInfiniteRange(name, arr) {
    if (arr.length < 2) {
        throw new Error(name + ' must have at least 2 elements');
    }
    for (const index in arr) {
        if (typeof arr[index] !== 'number') {
            throw new Error(`${name} must contain only numbers`);
        }
        if (arr[index] === -Infinity || arr[index] === Infinity) {
            throw new Error(`${name} must contain only finite numbers, but got [${arr.join(',')}]`);
        }
    }
}
function interpolate(input, inputRange, outputRange, options) {
    var _a;
    if (typeof input === 'undefined') {
        throw new Error('input can not be undefined');
    }
    if (typeof inputRange === 'undefined') {
        throw new Error('inputRange can not be undefined');
    }
    if (typeof outputRange === 'undefined') {
        throw new Error('outputRange can not be undefined');
    }
    if (inputRange.length !== outputRange.length) {
        throw new Error('inputRange (' +
            inputRange.length +
            ') and outputRange (' +
            outputRange.length +
            ') must have the same length');
    }
    checkInfiniteRange('inputRange', inputRange);
    checkInfiniteRange('outputRange', outputRange);
    checkValidInputRange(inputRange);
    const easing = (_a = options === null || options === void 0 ? void 0 : options.easing) !== null && _a !== void 0 ? _a : ((num) => num);
    let extrapolateLeft = 'extend';
    if ((options === null || options === void 0 ? void 0 : options.extrapolateLeft) !== undefined) {
        extrapolateLeft = options.extrapolateLeft;
    }
    let extrapolateRight = 'extend';
    if ((options === null || options === void 0 ? void 0 : options.extrapolateRight) !== undefined) {
        extrapolateRight = options.extrapolateRight;
    }
    if (typeof input !== 'number') {
        throw new TypeError('Cannot interpolate an input which is not a number');
    }
    const range = findRange(input, inputRange);
    return interpolateFunction(input, [inputRange[range], inputRange[range + 1]], [outputRange[range], outputRange[range + 1]], {
        easing,
        extrapolateLeft,
        extrapolateRight,
    });
}
interpolate$1.interpolate = interpolate;

// Calculate the `.currentTime` of a video or audio element
Object.defineProperty(getCurrentTime, "__esModule", { value: true });
getCurrentTime.getMediaTime = void 0;
const interpolate_1$1 = interpolate$1;
const getMediaTime = ({ fps, frame, src, playbackRate, startFrom, }) => {
    const expectedFrame = (0, interpolate_1$1.interpolate)(frame, [-1, startFrom, startFrom + 1], [-1, startFrom, startFrom + playbackRate]);
    if (src.endsWith('mp4')) {
        // In Chrome, for MP4s, if 30fps, the first frame is still displayed at 0.033333
        // even though after that it increases by 0.033333333 each.
        // So frame = 0 in Remotion is like frame = 1 for the browser
        return (expectedFrame + 1) / fps;
    }
    if (src.endsWith('webm')) {
        // For WebM videos, we need to add a little bit of shift to get the right frame.
        const msPerFrame = 1000 / fps;
        const msShift = msPerFrame / 2;
        return (expectedFrame * msPerFrame + msShift) / 1000;
    }
    // For audio, we don't do any shift correction
    return expectedFrame / fps;
};
getCurrentTime.getMediaTime = getMediaTime;

var warnAboutNonSeekableMedia$1 = {};

Object.defineProperty(warnAboutNonSeekableMedia$1, "__esModule", { value: true });
warnAboutNonSeekableMedia$1.warnAboutNonSeekableMedia = void 0;
const alreadyWarned = {};
const warnAboutNonSeekableMedia = (ref) => {
    // Media is not loaded yet, but this does not yet mean something is wrong with the media
    if (ref.seekable.length === 0) {
        return;
    }
    if (ref.seekable.length > 1) {
        return;
    }
    if (alreadyWarned[ref.src]) {
        return;
    }
    const range = { start: ref.seekable.start(0), end: ref.seekable.end(0) };
    if (range.start === 0 && range.end === 0) {
        console.error('The media', ref.src, 'does not seem to support seeking. Remotion cannot properly handle it. Please see https://remotion.dev/docs/non-seekable-media for assistance.');
        alreadyWarned[ref.src] = true;
    }
};
warnAboutNonSeekableMedia$1.warnAboutNonSeekableMedia = warnAboutNonSeekableMedia;

Object.defineProperty(useMediaPlayback$1, "__esModule", { value: true });
useMediaPlayback$1.useMediaPlayback = void 0;
const react_1$d = react.exports;
const use_audio_frame_1$4 = useAudioFrame;
const play_and_handle_not_allowed_error_1 = playAndHandleNotAllowedError$1;
const timeline_position_state_1$2 = timelinePositionState;
const use_frame_1$2 = useFrame;
const use_video_config_1 = useVideoConfig$1;
const get_current_time_1$1 = getCurrentTime;
const warn_about_non_seekable_media_1 = warnAboutNonSeekableMedia$1;
const useMediaPlayback = ({ mediaRef, src, mediaType, playbackRate: localPlaybackRate, }) => {
    const { playbackRate: globalPlaybackRate } = (0, react_1$d.useContext)(timeline_position_state_1$2.TimelineContext);
    const frame = (0, use_frame_1$2.useCurrentFrame)();
    const absoluteFrame = (0, use_frame_1$2.useAbsoluteCurrentFrame)();
    const [playing] = (0, timeline_position_state_1$2.usePlayingState)();
    const { fps } = (0, use_video_config_1.useVideoConfig)();
    const mediaStartsAt = (0, use_audio_frame_1$4.useMediaStartsAt)();
    const playbackRate = localPlaybackRate * globalPlaybackRate;
    (0, react_1$d.useEffect)(() => {
        var _a;
        if (!playing) {
            (_a = mediaRef.current) === null || _a === void 0 ? void 0 : _a.pause();
        }
    }, [mediaRef, mediaType, playing]);
    (0, react_1$d.useEffect)(() => {
        const tagName = mediaType === 'audio' ? '<Audio>' : '<Video>';
        if (!mediaRef.current) {
            throw new Error(`No ${mediaType} ref found`);
        }
        if (!src) {
            throw new Error(`No 'src' attribute was passed to the ${tagName} element.`);
        }
        mediaRef.current.playbackRate = Math.max(0, playbackRate);
        const shouldBeTime = (0, get_current_time_1$1.getMediaTime)({
            fps,
            frame,
            src,
            playbackRate: localPlaybackRate,
            startFrom: -mediaStartsAt,
        });
        const isTime = mediaRef.current.currentTime;
        const timeShift = Math.abs(shouldBeTime - isTime);
        if (timeShift > 0.45 && !mediaRef.current.ended) {
            console.log('Time has shifted by', timeShift, 'sec. Fixing...', `(isTime=${isTime},shouldBeTime=${shouldBeTime})`);
            // If scrubbing around, adjust timing
            // or if time shift is bigger than 0.2sec
            mediaRef.current.currentTime = shouldBeTime;
            (0, warn_about_non_seekable_media_1.warnAboutNonSeekableMedia)(mediaRef.current);
        }
        if (!playing || absoluteFrame === 0) {
            mediaRef.current.currentTime = shouldBeTime;
        }
        if (mediaRef.current.paused && !mediaRef.current.ended && playing) {
            const { current } = mediaRef;
            current.currentTime = shouldBeTime;
            (0, play_and_handle_not_allowed_error_1.playAndHandleNotAllowedError)(mediaRef, mediaType);
        }
    }, [
        absoluteFrame,
        fps,
        playbackRate,
        frame,
        mediaRef,
        mediaType,
        playing,
        src,
        mediaStartsAt,
        localPlaybackRate,
    ]);
};
useMediaPlayback$1.useMediaPlayback = useMediaPlayback;

var useMediaTagVolume$1 = {};

Object.defineProperty(useMediaTagVolume$1, "__esModule", { value: true });
useMediaTagVolume$1.useMediaTagVolume = void 0;
const react_1$c = react.exports;
// Returns the real volume of the audio or video while playing,
// no matter what the supposed volume should be
const useMediaTagVolume = (mediaRef) => {
    const [actualVolume, setActualVolume] = (0, react_1$c.useState)(1);
    (0, react_1$c.useEffect)(() => {
        const ref = mediaRef.current;
        if (!ref) {
            return;
        }
        const onChange = () => {
            setActualVolume(ref.volume);
        };
        ref.addEventListener('volumechange', onChange);
        return () => ref.removeEventListener('volumechange', onChange);
    }, [mediaRef]);
    (0, react_1$c.useEffect)(() => {
        const ref = mediaRef.current;
        if (!ref) {
            return;
        }
        if (ref.volume !== actualVolume) {
            setActualVolume(ref.volume);
        }
    }, [actualVolume, mediaRef]);
    return actualVolume;
};
useMediaTagVolume$1.useMediaTagVolume = useMediaTagVolume;

var useSyncVolumeWithMediaTag$1 = {};

var isApproximatelyTheSame$1 = {};

Object.defineProperty(isApproximatelyTheSame$1, "__esModule", { value: true });
isApproximatelyTheSame$1.isApproximatelyTheSame = void 0;
const FLOATING_POINT_ERROR_THRESHOLD = 0.00001;
const isApproximatelyTheSame = (num1, num2) => {
    return Math.abs(num1 - num2) < FLOATING_POINT_ERROR_THRESHOLD;
};
isApproximatelyTheSame$1.isApproximatelyTheSame = isApproximatelyTheSame;

Object.defineProperty(useSyncVolumeWithMediaTag$1, "__esModule", { value: true });
useSyncVolumeWithMediaTag$1.useSyncVolumeWithMediaTag = void 0;
const react_1$b = react.exports;
const is_approximately_the_same_1$1 = isApproximatelyTheSame$1;
const volume_prop_1$2 = volumeProp;
const useSyncVolumeWithMediaTag = ({ volumePropFrame, actualVolume, volume, mediaVolume, mediaRef, }) => {
    (0, react_1$b.useEffect)(() => {
        const userPreferredVolume = (0, volume_prop_1$2.evaluateVolume)({
            frame: volumePropFrame,
            volume,
            mediaVolume,
        });
        if (!(0, is_approximately_the_same_1$1.isApproximatelyTheSame)(userPreferredVolume, actualVolume) &&
            mediaRef.current) {
            mediaRef.current.volume = userPreferredVolume;
        }
    }, [actualVolume, volumePropFrame, mediaRef, volume, mediaVolume]);
};
useSyncVolumeWithMediaTag$1.useSyncVolumeWithMediaTag = useSyncVolumeWithMediaTag;

var volumePositionState = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.useMediaMutedState = exports.useMediaVolumeState = exports.SetMediaVolumeContext = exports.MediaVolumeContext = void 0;
const react_1 = react.exports;
exports.MediaVolumeContext = (0, react_1.createContext)({
    mediaMuted: false,
    mediaVolume: 1,
});
exports.SetMediaVolumeContext = (0, react_1.createContext)({
    setMediaMuted: () => {
        throw new Error('default');
    },
    setMediaVolume: () => {
        throw new Error('default');
    },
});
const useMediaVolumeState = () => {
    const { mediaVolume } = (0, react_1.useContext)(exports.MediaVolumeContext);
    const { setMediaVolume } = (0, react_1.useContext)(exports.SetMediaVolumeContext);
    return (0, react_1.useMemo)(() => {
        return [mediaVolume, setMediaVolume];
    }, [mediaVolume, setMediaVolume]);
};
exports.useMediaVolumeState = useMediaVolumeState;
const useMediaMutedState = () => {
    const { mediaMuted } = (0, react_1.useContext)(exports.MediaVolumeContext);
    const { setMediaMuted } = (0, react_1.useContext)(exports.SetMediaVolumeContext);
    return (0, react_1.useMemo)(() => {
        return [mediaMuted, setMediaMuted];
    }, [mediaMuted, setMediaMuted]);
};
exports.useMediaMutedState = useMediaMutedState;

}(volumePositionState));

var sharedAudioTags = {};

(function (exports) {
var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (commonjsGlobal && commonjsGlobal.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (commonjsGlobal && commonjsGlobal.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useSharedAudio = exports.SharedAudioContextProvider = exports.SharedAudioContext = void 0;
const jsx_runtime_1 = jsxRuntime.exports;
const react_1 = __importStar(react.exports);
const EMPTY_AUDIO = 'data:audio/mp3;base64,/+MYxAAJcAV8AAgAABn//////+/gQ5BAMA+D4Pg+BAQBAEAwD4Pg+D4EBAEAQDAPg++hYBH///hUFQVBUFREDQNHmf///////+MYxBUGkAGIMAAAAP/29Xt6lUxBTUUzLjEwMFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV/+MYxDUAAANIAAAAAFVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV';
exports.SharedAudioContext = (0, react_1.createContext)(null);
const SharedAudioContextProvider = ({ children, numberOfAudioTags }) => {
    const [audios, setAudios] = (0, react_1.useState)([]);
    const [initialNumberOfAudioTags] = (0, react_1.useState)(numberOfAudioTags);
    if (numberOfAudioTags !== initialNumberOfAudioTags) {
        throw new Error('The number of shared audio tags has changed dynamically. Once you have set this property, you cannot change it afterwards.');
    }
    const refs = (0, react_1.useMemo)(() => {
        return new Array(numberOfAudioTags).fill(true).map(() => {
            return { id: Math.random(), ref: (0, react_1.createRef)() };
        });
    }, [numberOfAudioTags]);
    const takenAudios = (0, react_1.useRef)(new Array(numberOfAudioTags).fill(false));
    const registerAudio = (0, react_1.useCallback)((aud) => {
        const firstFreeAudio = takenAudios.current.findIndex((a) => a === false);
        if (firstFreeAudio === -1) {
            throw new Error(`Tried to simultaneously mount ${numberOfAudioTags + 1} <Audio /> tags at the same time. With the current settings, the maximum amount of <Audio /> tags is limited to ${numberOfAudioTags} at the same time. Remotion pre-mounts silent audio tags to help avoid browser autoplay restrictions. See /docs/player/autoplay#use-the-numberofsharedaudiotags-property for more information on how to increase this limit.`);
        }
        const { id, ref } = refs[firstFreeAudio];
        const cloned = [...takenAudios.current];
        cloned[firstFreeAudio] = id;
        takenAudios.current = cloned;
        const newElem = {
            props: aud,
            id,
            el: ref,
        };
        // We need a timeout because this state setting is triggered by another state being set, causing React to throw an error.
        // By setting a timeout, we are bypassing the error and allowing the state
        // to be updated in the next tick.
        // This can lead to a tiny delay of audio playback, improvement ideas are welcome.
        setTimeout(() => {
            setAudios((prevAudios) => [...prevAudios, newElem]);
        }, 4);
        return newElem;
    }, [numberOfAudioTags, refs]);
    const unregisterAudio = (0, react_1.useCallback)((id) => {
        const cloned = [...takenAudios.current];
        const index = refs.findIndex((r) => r.id === id);
        if (index === -1) {
            throw new TypeError('Error occured in ');
        }
        cloned[index] = false;
        takenAudios.current = cloned;
        setAudios((prevAudios) => {
            return prevAudios.filter((a) => a.id !== id);
        });
    }, [refs]);
    const updateAudio = (0, react_1.useCallback)((id, aud) => {
        setAudios((prevAudios) => {
            return prevAudios.map((prevA) => {
                if (prevA.id === id) {
                    return {
                        ...prevA,
                        props: aud,
                    };
                }
                return prevA;
            });
        });
    }, []);
    const playAllAudios = (0, react_1.useCallback)(() => {
        refs.forEach((ref) => {
            var _a;
            (_a = ref.ref.current) === null || _a === void 0 ? void 0 : _a.play();
        });
    }, [refs]);
    const value = (0, react_1.useMemo)(() => {
        return {
            registerAudio,
            unregisterAudio,
            updateAudio,
            playAllAudios,
            numberOfAudioTags,
        };
    }, [
        numberOfAudioTags,
        playAllAudios,
        registerAudio,
        unregisterAudio,
        updateAudio,
    ]);
    return ((0, jsx_runtime_1.jsxs)(exports.SharedAudioContext.Provider, Object.assign({ value: value }, { children: [refs.map(({ id, ref }) => {
                const data = audios.find((a) => a.id === id);
                if (data === undefined) {
                    return (0, jsx_runtime_1.jsx)("audio", { ref: ref, src: EMPTY_AUDIO }, id);
                }
                if (!data) {
                    throw new TypeError('Expected audio data to be there');
                }
                return (0, jsx_runtime_1.jsx)("audio", Object.assign({ ref: ref }, data.props), id);
            }), children] }), void 0));
};
exports.SharedAudioContextProvider = SharedAudioContextProvider;
const useSharedAudio = (aud) => {
    const ctx = (0, react_1.useContext)(exports.SharedAudioContext);
    const [elem] = (0, react_1.useState)(() => {
        if (ctx && ctx.numberOfAudioTags > 0) {
            return ctx.registerAudio(aud);
        }
        return {
            el: react_1.default.createRef(),
            id: Math.random(),
            props: aud,
        };
    });
    (0, react_1.useEffect)(() => {
        return () => {
            if (ctx && ctx.numberOfAudioTags > 0) {
                ctx.unregisterAudio(elem.id);
            }
        };
    }, [ctx, elem.id]);
    (0, react_1.useEffect)(() => {
        if (ctx && ctx.numberOfAudioTags > 0) {
            ctx.updateAudio(elem.id, aud);
        }
    }, [aud, ctx, elem.id]);
    return elem;
};
exports.useSharedAudio = useSharedAudio;

}(sharedAudioTags));

Object.defineProperty(AudioForDevelopment, "__esModule", { value: true });
AudioForDevelopment.AudioForDevelopment = void 0;
const jsx_runtime_1$a = jsxRuntime.exports;
const react_1$a = react.exports;
const use_media_in_timeline_1$1 = useMediaInTimeline$1;
const use_media_playback_1$1 = useMediaPlayback$1;
const use_media_tag_volume_1$1 = useMediaTagVolume$1;
const use_sync_volume_with_media_tag_1$1 = useSyncVolumeWithMediaTag$1;
const volume_position_state_1$2 = volumePositionState;
const shared_audio_tags_1$1 = sharedAudioTags;
const use_audio_frame_1$3 = useAudioFrame;
const AudioForDevelopmentForwardRefFunction = (props, ref) => {
    const [initialShouldPreMountAudioElements] = (0, react_1$a.useState)(props.shouldPreMountAudioTags);
    if (props.shouldPreMountAudioTags !== initialShouldPreMountAudioElements) {
        throw new Error('Cannot change the behavior for pre-mounting audio tags dynamically.');
    }
    const [mediaVolume] = (0, volume_position_state_1$2.useMediaVolumeState)();
    const [mediaMuted] = (0, volume_position_state_1$2.useMediaMutedState)();
    const volumePropFrame = (0, use_audio_frame_1$3.useFrameForVolumeProp)();
    const { volume, muted, playbackRate, shouldPreMountAudioTags, ...nativeProps } = props;
    const propsToPass = (0, react_1$a.useMemo)(() => {
        return {
            muted: muted || mediaMuted,
            ...nativeProps,
        };
    }, [mediaMuted, muted, nativeProps]);
    const audioRef = (0, shared_audio_tags_1$1.useSharedAudio)(propsToPass).el;
    const actualVolume = (0, use_media_tag_volume_1$1.useMediaTagVolume)(audioRef);
    (0, use_sync_volume_with_media_tag_1$1.useSyncVolumeWithMediaTag)({
        volumePropFrame,
        actualVolume,
        volume,
        mediaVolume,
        mediaRef: audioRef,
    });
    (0, use_media_in_timeline_1$1.useMediaInTimeline)({
        volume,
        mediaVolume,
        mediaRef: audioRef,
        src: nativeProps.src,
        mediaType: 'audio',
    });
    (0, use_media_playback_1$1.useMediaPlayback)({
        mediaRef: audioRef,
        src: nativeProps.src,
        mediaType: 'audio',
        playbackRate: playbackRate !== null && playbackRate !== void 0 ? playbackRate : 1,
    });
    (0, react_1$a.useImperativeHandle)(ref, () => {
        return audioRef.current;
    });
    if (initialShouldPreMountAudioElements) {
        return null;
    }
    return (0, jsx_runtime_1$a.jsx)("audio", Object.assign({ ref: audioRef }, propsToPass), void 0);
};
AudioForDevelopment.AudioForDevelopment = (0, react_1$a.forwardRef)(AudioForDevelopmentForwardRefFunction);

var AudioForRendering = {};

var absoluteSrc = {};

Object.defineProperty(absoluteSrc, "__esModule", { value: true });
absoluteSrc.getAbsoluteSrc = void 0;
const getAbsoluteSrc = (relativeSrc) => {
    return new URL(relativeSrc, window.location.origin).href;
};
absoluteSrc.getAbsoluteSrc = getAbsoluteSrc;

var isRemoteAsset$1 = {};

// TODO: Can be removed in 3.0 branch
Object.defineProperty(isRemoteAsset$1, "__esModule", { value: true });
isRemoteAsset$1.isRemoteAsset = void 0;
const isRemoteAsset = (asset, dataURLIsRemoteObject) => (!asset.startsWith(window.location.origin) && !asset.startsWith('data')) ||
    (asset.startsWith('data') && dataURLIsRemoteObject);
isRemoteAsset$1.isRemoteAsset = isRemoteAsset;

var random$1 = {};

Object.defineProperty(random$1, "__esModule", { value: true });
random$1.random = void 0;
function mulberry32(a) {
    let t = a + 0x6d2b79f5;
    t = Math.imul(t ^ (t >>> 15), t | 1);
    t ^= t + Math.imul(t ^ (t >>> 7), t | 61);
    return ((t ^ (t >>> 14)) >>> 0) / 4294967296;
}
function hashCode(str) {
    let i = 0;
    let chr = 0;
    let hash = 0;
    for (i = 0; i < str.length; i++) {
        chr = str.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}
/**
 * A deterministic pseudo-random number generator.
 * Pass in the same seed and get the same pseudorandom number.
 * See: https://remotion.dev/docs/random
 */
const random = (seed, dummy) => {
    if (dummy !== undefined) {
        throw new TypeError('random() takes only one argument');
    }
    if (seed === null) {
        return Math.random();
    }
    if (typeof seed === 'string') {
        return mulberry32(hashCode(seed));
    }
    if (typeof seed === 'number') {
        return mulberry32(seed * 10000000000);
    }
    throw new Error('random() argument must be a number or a string');
};
random$1.random = random;

Object.defineProperty(AudioForRendering, "__esModule", { value: true });
AudioForRendering.AudioForRendering = void 0;
const jsx_runtime_1$9 = jsxRuntime.exports;
const react_1$9 = react.exports;
const absolute_src_1$1 = absoluteSrc;
const CompositionManager_1$4 = CompositionManager;
const is_remote_asset_1$1 = isRemoteAsset$1;
const random_1$1 = random$1;
const sequencing_1$4 = sequencing;
const use_frame_1$1 = useFrame;
const volume_prop_1$1 = volumeProp;
const use_audio_frame_1$2 = useAudioFrame;
const AudioForRenderingRefForwardingFunction = (props, ref) => {
    const audioRef = (0, react_1$9.useRef)(null);
    const absoluteFrame = (0, use_frame_1$1.useAbsoluteCurrentFrame)();
    const volumePropFrame = (0, use_audio_frame_1$2.useFrameForVolumeProp)();
    const frame = (0, use_frame_1$1.useCurrentFrame)();
    const sequenceContext = (0, react_1$9.useContext)(sequencing_1$4.SequenceContext);
    const { registerAsset, unregisterAsset } = (0, react_1$9.useContext)(CompositionManager_1$4.CompositionManager);
    // Generate a string that's as unique as possible for this asset
    // but at the same time the same on all threads
    const id = (0, react_1$9.useMemo)(() => {
        var _a;
        return `audio-${(0, random_1$1.random)((_a = props.src) !== null && _a !== void 0 ? _a : '')}-${sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.relativeFrom}-${sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.cumulatedFrom}-${sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.durationInFrames}-muted:${props.muted}`;
    }, [props.muted, props.src, sequenceContext]);
    const { volume: volumeProp, playbackRate, ...nativeProps } = props;
    const volume = (0, volume_prop_1$1.evaluateVolume)({
        volume: volumeProp,
        frame: volumePropFrame,
        mediaVolume: 1,
    });
    (0, react_1$9.useImperativeHandle)(ref, () => {
        return audioRef.current;
    });
    (0, react_1$9.useEffect)(() => {
        var _a;
        if (!props.src) {
            throw new Error('No src passed');
        }
        if (props.muted) {
            return;
        }
        registerAsset({
            type: 'audio',
            src: (0, absolute_src_1$1.getAbsoluteSrc)(props.src),
            id,
            frame: absoluteFrame,
            volume,
            isRemote: (0, is_remote_asset_1$1.isRemoteAsset)((0, absolute_src_1$1.getAbsoluteSrc)(props.src), true),
            mediaFrame: frame,
            playbackRate: (_a = props.playbackRate) !== null && _a !== void 0 ? _a : 1,
        });
        return () => unregisterAsset(id);
    }, [
        props.muted,
        props.src,
        registerAsset,
        absoluteFrame,
        id,
        unregisterAsset,
        volume,
        volumePropFrame,
        frame,
        playbackRate,
        props.playbackRate,
    ]);
    return (0, jsx_runtime_1$9.jsx)("audio", Object.assign({ ref: audioRef }, nativeProps), void 0);
};
AudioForRendering.AudioForRendering = (0, react_1$9.forwardRef)(AudioForRenderingRefForwardingFunction);

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.Audio = void 0;
const jsx_runtime_1 = jsxRuntime.exports;
const react_1 = react.exports;
const get_environment_1 = getEnvironment;
const sequencing_1 = sequencing;
const validate_media_props_1 = validateMediaProps$1;
const validate_start_from_props_1 = validateStartFromProps$1;
const AudioForDevelopment_1 = AudioForDevelopment;
const AudioForRendering_1 = AudioForRendering;
const shared_audio_tags_1 = sharedAudioTags;
const AudioRefForwardingFunction = (props, ref) => {
    const audioContext = (0, react_1.useContext)(shared_audio_tags_1.SharedAudioContext);
    const { startFrom, endAt, ...otherProps } = props;
    const onError = (0, react_1.useCallback)((e) => {
        throw new Error(`Could not play audio with src ${otherProps.src}: ${e.currentTarget.error}`);
    }, [otherProps.src]);
    if (typeof startFrom !== 'undefined' || typeof endAt !== 'undefined') {
        (0, validate_start_from_props_1.validateStartFromProps)(startFrom, endAt);
        const startFromFrameNo = startFrom !== null && startFrom !== void 0 ? startFrom : 0;
        const endAtFrameNo = endAt !== null && endAt !== void 0 ? endAt : Infinity;
        return ((0, jsx_runtime_1.jsx)(sequencing_1.Sequence, Object.assign({ layout: "none", from: 0 - startFromFrameNo, showInTimeline: false, durationInFrames: endAtFrameNo }, { children: (0, jsx_runtime_1.jsx)(exports.Audio, Object.assign({}, otherProps, { ref: ref }), void 0) }), void 0));
    }
    (0, validate_media_props_1.validateMediaProps)(props, 'Audio');
    if ((0, get_environment_1.getRemotionEnvironment)() === 'rendering') {
        return (0, jsx_runtime_1.jsx)(AudioForRendering_1.AudioForRendering, Object.assign({}, props, { ref: ref, onError: onError }), void 0);
    }
    return ((0, jsx_runtime_1.jsx)(AudioForDevelopment_1.AudioForDevelopment, Object.assign({ shouldPreMountAudioTags: audioContext !== null && audioContext.numberOfAudioTags > 0 }, props, { ref: ref, onError: onError }), void 0));
};
exports.Audio = (0, react_1.forwardRef)(AudioRefForwardingFunction);

}(Audio));

var props$1 = {};

Object.defineProperty(props$1, "__esModule", { value: true });

(function (exports) {
var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (commonjsGlobal && commonjsGlobal.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(Audio, exports);
__exportStar(props$1, exports);

}(audio));

var Composition$1 = {};

var registerRoot = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.getIsEvaluation = exports.getCompositionName = exports.isPlainIndex = exports.removeStaticComposition = exports.addStaticComposition = exports.getRoot = exports.registerRoot = void 0;
let root = null;
// Ok to have components with various prop types
// eslint-disable-next-line @typescript-eslint/no-explicit-any
let staticCompositions = [];
const registerRoot = (comp) => {
    if (root) {
        throw new Error('registerRoot() was called more than once.');
    }
    root = comp;
};
exports.registerRoot = registerRoot;
const getRoot = () => {
    return root;
};
exports.getRoot = getRoot;
const addStaticComposition = (composition) => {
    staticCompositions = [...staticCompositions, composition];
};
exports.addStaticComposition = addStaticComposition;
const removeStaticComposition = (id) => {
    staticCompositions = staticCompositions.filter((s) => {
        return s.id !== id;
    });
};
exports.removeStaticComposition = removeStaticComposition;
// Is a plain index.html file with neither ?evalution nor ?composition URL.
// Useful for just setting localStorage values.
const isPlainIndex = () => {
    return !(0, exports.getIsEvaluation)() && (0, exports.getCompositionName)() === null;
};
exports.isPlainIndex = isPlainIndex;
const getCompositionName = () => {
    const param = new URLSearchParams(window.location.search).get('composition');
    if (param !== null) {
        return String(param);
    }
    return null;
};
exports.getCompositionName = getCompositionName;
const getIsEvaluation = () => {
    const param = new URLSearchParams(window.location.search).get('evaluation');
    return param !== null;
};
exports.getIsEvaluation = getIsEvaluation;
if (typeof window !== 'undefined') {
    window.getStaticCompositions = () => staticCompositions.map((c) => {
        return {
            durationInFrames: c.durationInFrames,
            fps: c.fps,
            height: c.height,
            id: c.id,
            width: c.width,
            defaultProps: c.defaultProps,
        };
    });
}

}(registerRoot));

var useLazyComponent$1 = {};

var __createBinding$2 = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault$2 = (commonjsGlobal && commonjsGlobal.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar$2 = (commonjsGlobal && commonjsGlobal.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding$2(result, mod, k);
    __setModuleDefault$2(result, mod);
    return result;
};
Object.defineProperty(useLazyComponent$1, "__esModule", { value: true });
useLazyComponent$1.useLazyComponent = void 0;
const react_1$8 = __importStar$2(react.exports);
// Expected, it can be any component props
const useLazyComponent = (compProps) => {
    const lazy = (0, react_1$8.useMemo)(() => {
        if ('lazyComponent' in compProps) {
            return react_1$8.default.lazy(compProps.lazyComponent);
        }
        if ('component' in compProps) {
            // In SSR, suspense is not yet supported, we cannot use React.lazy
            if (typeof document === 'undefined') {
                return compProps.component;
            }
            return react_1$8.default.lazy(() => Promise.resolve({ default: compProps.component }));
        }
        throw new Error("You must pass either 'component' or 'lazyComponent'");
        // Very important to leave the dependencies as they are, or instead
        // the player will remount on every frame.
        // @ts-expect-error
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [compProps.component, compProps.lazyComponent]);
    return lazy;
};
useLazyComponent$1.useLazyComponent = useLazyComponent;

var validateCompositionId = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.invalidCompositionErrorMessage = exports.isCompositionIdValid = exports.validateCompositionId = void 0;
const validateCompositionId = (id) => {
    if (!(0, exports.isCompositionIdValid)(id)) {
        throw new Error(`Composition id can only contain a-z, A-Z, 0-9 and -. You passed ${id}`);
    }
};
exports.validateCompositionId = validateCompositionId;
const getRegex = () => /^([a-zA-Z0-9-])+$/g;
const isCompositionIdValid = (id) => id.match(getRegex());
exports.isCompositionIdValid = isCompositionIdValid;
exports.invalidCompositionErrorMessage = `Composition ID must match ${String(getRegex())}`;

}(validateCompositionId));

var validateDimensions = {};

Object.defineProperty(validateDimensions, "__esModule", { value: true });
validateDimensions.validateDimension = void 0;
const validateDimension = (amount, nameOfProp, location) => {
    if (typeof amount !== 'number') {
        throw new Error(`The "${nameOfProp}" prop ${location} must be a number, but you passed a value of type ${typeof amount}`);
    }
    if (isNaN(amount)) {
        throw new TypeError(`The "${nameOfProp}" prop ${location} must not be NaN, but is NaN.`);
    }
    if (!Number.isFinite(amount)) {
        throw new TypeError(`The "${nameOfProp}" prop ${location} must be finite, but is ${amount}.`);
    }
    if (amount % 1 !== 0) {
        throw new TypeError(`The "${nameOfProp}" prop ${location} must be an integer, but is ${amount}.`);
    }
    if (amount <= 0) {
        throw new TypeError(`The "${nameOfProp}" prop ${location} must be positive, but got ${amount}.`);
    }
};
validateDimensions.validateDimension = validateDimension;

var validateDurationInFrames$1 = {};

Object.defineProperty(validateDurationInFrames$1, "__esModule", { value: true });
validateDurationInFrames$1.validateDurationInFrames = void 0;
const validateDurationInFrames = (durationInFrames, component) => {
    if (typeof durationInFrames !== 'number') {
        throw new Error(`The "durationInFrames" prop ${component} must be a number, but you passed a value of type ${typeof durationInFrames}`);
    }
    if (durationInFrames <= 0) {
        throw new TypeError(`The "durationInFrames" prop ${component} must be positive, but got ${durationInFrames}.`);
    }
    if (durationInFrames % 1 !== 0) {
        throw new TypeError(`The "durationInFrames" prop ${component} must be an integer, but got ${durationInFrames}.`);
    }
};
validateDurationInFrames$1.validateDurationInFrames = validateDurationInFrames;

var validateFps$1 = {};

Object.defineProperty(validateFps$1, "__esModule", { value: true });
validateFps$1.validateFps = void 0;
const validateFps = (fps, location) => {
    if (typeof fps !== 'number') {
        throw new Error(`"fps" must be a number, but you passed a value of type ${typeof fps} ${location}`);
    }
    if (!Number.isFinite(fps)) {
        throw new Error(`"fps" must be a finite, but you passed ${fps} ${location}`);
    }
    if (isNaN(fps)) {
        throw new Error(`"fps" must not be NaN, but got ${fps} ${location}`);
    }
    if (fps <= 0) {
        throw new TypeError(`"fps" must be positive, but got ${fps} ${location}`);
    }
};
validateFps$1.validateFps = validateFps;

Object.defineProperty(Composition$1, "__esModule", { value: true });
Composition$1.Composition = void 0;
const react_1$7 = react.exports;
const CompositionManager_1$3 = CompositionManager;
const nonce_1$1 = nonce;
const register_root_1$1 = registerRoot;
const use_lazy_component_1$1 = useLazyComponent$1;
const validate_composition_id_1$1 = validateCompositionId;
const validate_dimensions_1$1 = validateDimensions;
const validate_duration_in_frames_1$3 = validateDurationInFrames$1;
const validate_fps_1$2 = validateFps$1;
const Composition = ({ width, height, fps, durationInFrames, id, defaultProps, ...compProps }) => {
    const { registerComposition, unregisterComposition } = (0, react_1$7.useContext)(CompositionManager_1$3.CompositionManager);
    const lazy = (0, use_lazy_component_1$1.useLazyComponent)(compProps);
    const nonce = (0, nonce_1$1.useNonce)();
    (0, react_1$7.useEffect)(() => {
        // Ensure it's a URL safe id
        if (!id) {
            throw new Error('No id for composition passed.');
        }
        (0, validate_composition_id_1$1.validateCompositionId)(id);
        (0, validate_dimensions_1$1.validateDimension)(width, 'width', 'of the <Composition/> component');
        (0, validate_dimensions_1$1.validateDimension)(height, 'height', 'of the <Composition/> component');
        (0, validate_duration_in_frames_1$3.validateDurationInFrames)(durationInFrames, 'of the <Composition/> component');
        (0, validate_fps_1$2.validateFps)(fps, 'as a prop of the <Composition/> component');
        registerComposition({
            durationInFrames,
            fps,
            height,
            width,
            id,
            component: lazy,
            defaultProps,
            nonce,
        });
        if ((0, register_root_1$1.getIsEvaluation)()) {
            (0, register_root_1$1.addStaticComposition)({
                component: lazy,
                durationInFrames,
                fps,
                height,
                id,
                width,
                nonce,
                defaultProps,
            });
        }
        return () => {
            unregisterComposition(id);
            (0, register_root_1$1.removeStaticComposition)(id);
        };
    }, [
        durationInFrames,
        fps,
        height,
        lazy,
        id,
        defaultProps,
        registerComposition,
        unregisterComposition,
        width,
        nonce,
    ]);
    return null;
};
Composition$1.Composition = Composition;

var config = {};

var browserExecutable = {};

Object.defineProperty(browserExecutable, "__esModule", { value: true });
browserExecutable.getBrowserExecutable = browserExecutable.setBrowserExecutable = void 0;
let currentBrowserExecutablePath = null;
const setBrowserExecutable = (newBrowserExecutablePath) => {
    currentBrowserExecutablePath = newBrowserExecutablePath;
};
browserExecutable.setBrowserExecutable = setBrowserExecutable;
const getBrowserExecutable = () => {
    return currentBrowserExecutablePath;
};
browserExecutable.getBrowserExecutable = getBrowserExecutable;

var codec = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.setCodec = exports.setOutputFormat = exports.getFinalOutputCodec = exports.DEFAULT_CODEC = exports.getOutputCodecOrUndefined = void 0;
const validCodecs = [
    'h264',
    'h265',
    'vp8',
    'vp9',
    'mp3',
    'aac',
    'wav',
    'prores',
    'h264-mkv',
];
const validLegacyFormats = ['mp4', 'png-sequence'];
let codec;
const getOutputCodecOrUndefined = () => {
    return codec;
};
exports.getOutputCodecOrUndefined = getOutputCodecOrUndefined;
exports.DEFAULT_CODEC = 'h264';
// eslint-disable-next-line complexity
const getFinalOutputCodec = ({ codec: inputCodec, fileExtension, emitWarning, }) => {
    if (inputCodec === undefined && fileExtension === 'webm') {
        if (emitWarning) {
            console.info('You have specified a .webm extension, using the VP8 encoder. Use --codec=vp9 to use the Vp9 encoder.');
        }
        return 'vp8';
    }
    if (inputCodec === undefined && fileExtension === 'hevc') {
        if (emitWarning) {
            console.info('You have specified a .hevc extension, using the H265 encoder.');
        }
        return 'h265';
    }
    if (inputCodec === undefined && fileExtension === 'mp3') {
        if (emitWarning) {
            console.info('You have specified a .mp3 extension, using the MP3 encoder.');
        }
        return 'mp3';
    }
    if (inputCodec === undefined && fileExtension === 'mov') {
        if (emitWarning) {
            console.info('You have specified a .mov extension, using the Apple ProRes encoder.');
        }
        return 'prores';
    }
    if (inputCodec === undefined && fileExtension === 'wav') {
        if (emitWarning) {
            console.info('You have specified a .wav extension, using the WAV encoder.');
        }
        return 'wav';
    }
    if (inputCodec === undefined && fileExtension === 'aac') {
        if (emitWarning) {
            console.info('You have specified a .aac extension, using the AAC encoder.');
        }
        return 'aac';
    }
    if (inputCodec === undefined && fileExtension === 'm4a') {
        if (emitWarning) {
            console.info('You have specified a .m4a extension, using the AAC encoder.');
        }
        return 'aac';
    }
    if (inputCodec === undefined && fileExtension === 'mkv') {
        if (emitWarning) {
            console.info('You have specified a .mkv extension, using the H264 encoder and WAV audio format.');
        }
        return 'h264-mkv';
    }
    return inputCodec !== null && inputCodec !== void 0 ? inputCodec : exports.DEFAULT_CODEC;
};
exports.getFinalOutputCodec = getFinalOutputCodec;
const setOutputFormat = (newLegacyFormat) => {
    if (newLegacyFormat === undefined) {
        codec = undefined;
        return;
    }
    if (!validLegacyFormats.includes(newLegacyFormat)) {
        throw new Error(`Output format must be one of the following: ${validLegacyFormats.join(', ')}, but got ${newLegacyFormat}`);
    }
    console.warn('setOutputFormat() is deprecated. Use the setCodec() and setImageSequence() instead.');
    if (newLegacyFormat === 'mp4') {
        codec = 'h264';
        return;
    }
    if (newLegacyFormat === 'png-sequence') {
        codec = undefined;
    }
};
exports.setOutputFormat = setOutputFormat;
const setCodec = (newCodec) => {
    if (newCodec === undefined) {
        codec = undefined;
        return;
    }
    if (!validCodecs.includes(newCodec)) {
        throw new Error(`Codec must be one of the following: ${validCodecs.join(', ')}, but got ${newCodec}`);
    }
    codec = newCodec;
};
exports.setCodec = setCodec;

}(codec));

var concurrency = {};

Object.defineProperty(concurrency, "__esModule", { value: true });
concurrency.getConcurrency = concurrency.setConcurrency = void 0;
let currentConcurrency = null;
const setConcurrency = (newConcurrency) => {
    if (typeof newConcurrency !== 'number') {
        throw new Error('--concurrency flag must be a number.');
    }
    currentConcurrency = newConcurrency;
};
concurrency.setConcurrency = setConcurrency;
const getConcurrency = () => {
    return currentConcurrency;
};
concurrency.getConcurrency = getConcurrency;

var crf = {};

var isAudioCodec$1 = {};

Object.defineProperty(isAudioCodec$1, "__esModule", { value: true });
isAudioCodec$1.isAudioCodec = void 0;
const isAudioCodec = (codec) => {
    return codec === 'mp3' || codec === 'aac' || codec === 'wav';
};
isAudioCodec$1.isAudioCodec = isAudioCodec;

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.getActualCrf = exports.validateSelectedCrfAndCodecCombination = exports.getValidCrfRanges = exports.getDefaultCrfForCodec = exports.getCrfOrUndefined = exports.setCrf = void 0;
const is_audio_codec_1 = isAudioCodec$1;
let currentCrf;
const setCrf = (newCrf) => {
    if (typeof newCrf !== 'number' && newCrf !== undefined) {
        throw new TypeError('The CRF must be a number or undefined.');
    }
    currentCrf = newCrf;
};
exports.setCrf = setCrf;
const getCrfOrUndefined = () => {
    return currentCrf;
};
exports.getCrfOrUndefined = getCrfOrUndefined;
const getDefaultCrfForCodec = (codec) => {
    if ((0, is_audio_codec_1.isAudioCodec)(codec)) {
        return 0;
    }
    if (codec === 'h264' || codec === 'h264-mkv') {
        return 18; // FFMPEG default 23
    }
    if (codec === 'h265') {
        return 23; // FFMPEG default 28
    }
    if (codec === 'vp8') {
        return 9; // FFMPEG default 10
    }
    if (codec === 'vp9') {
        return 28; // FFMPEG recommendation 31
    }
    if (codec === 'prores') {
        return 0;
    }
    throw new TypeError(`Got unexpected codec "${codec}"`);
};
exports.getDefaultCrfForCodec = getDefaultCrfForCodec;
const getValidCrfRanges = (codec) => {
    if ((0, is_audio_codec_1.isAudioCodec)(codec)) {
        return [0, 0];
    }
    if (codec === 'prores') {
        return [0, 0];
    }
    if (codec === 'h264' || codec === 'h264-mkv') {
        return [1, 51];
    }
    if (codec === 'h265') {
        return [0, 51];
    }
    if (codec === 'vp8') {
        return [4, 63];
    }
    if (codec === 'vp9') {
        return [0, 63];
    }
    throw new TypeError(`Got unexpected codec "${codec}"`);
};
exports.getValidCrfRanges = getValidCrfRanges;
const validateSelectedCrfAndCodecCombination = (crf, codec) => {
    const range = (0, exports.getValidCrfRanges)(codec);
    if (crf === 0 && (codec === 'h264' || codec === 'h264-mkv')) {
        throw new TypeError("Setting the CRF to 0 with a H264 codec is not supported anymore because of it's inconsistencies between platforms. Videos with CRF 0 cannot be played on iOS/macOS. 0 is a extreme value with inefficient settings which you probably want. Set CRF to a higher value to fix this error.");
    }
    if (crf < range[0] || crf > range[1]) {
        throw new TypeError(`CRF must be between ${range[0]} and ${range[1]} for codec ${codec}. Passed: ${crf}`);
    }
};
exports.validateSelectedCrfAndCodecCombination = validateSelectedCrfAndCodecCombination;
const getActualCrf = (codec) => {
    var _a;
    const crf = (_a = (0, exports.getCrfOrUndefined)()) !== null && _a !== void 0 ? _a : (0, exports.getDefaultCrfForCodec)(codec);
    (0, exports.validateSelectedCrfAndCodecCombination)(crf, codec);
    return crf;
};
exports.getActualCrf = getActualCrf;

}(crf));

var envFile$1 = {};

Object.defineProperty(envFile$1, "__esModule", { value: true });
envFile$1.getDotEnvLocation = envFile$1.setDotEnvLocation = void 0;
let envFile = null;
const setDotEnvLocation = (file) => {
    envFile = file;
};
envFile$1.setDotEnvLocation = setDotEnvLocation;
const getDotEnvLocation = () => envFile;
envFile$1.getDotEnvLocation = getDotEnvLocation;

var frameRange = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRange = exports.setFrameRangeFromCli = exports.setFrameRange = exports.validateFrameRange = void 0;
let range = null;
const validateFrameRange = (frameRange) => {
    if (frameRange === null) {
        return;
    }
    if (typeof frameRange === 'number') {
        if (frameRange < 0) {
            throw new TypeError('Frame must be a non-negative number, got ' + frameRange);
        }
        if (!Number.isFinite(frameRange)) {
            throw new TypeError('Frame must be a finite number, got ' + frameRange);
        }
        if (!Number.isInteger(frameRange)) {
            throw new Error(`Frame must be an integer, but got a float (${frameRange})`);
        }
        return;
    }
    if (Array.isArray(frameRange)) {
        if (frameRange.length !== 2) {
            throw new TypeError('Frame range must be a tuple, got an array with length ' +
                frameRange.length);
        }
        for (const value of frameRange) {
            if (typeof value !== 'number') {
                throw new Error(`Each value of frame range must be a number, but got ${typeof value} (${JSON.stringify(value)})`);
            }
            if (!Number.isFinite(value)) {
                throw new TypeError('Each value of frame range must be finite, but got ' + value);
            }
            if (!Number.isInteger(value)) {
                throw new Error(`Each value of frame range must be an integer, but got a float (${value})`);
            }
            if (value < 0) {
                throw new Error(`Each value of frame range must be non-negative, but got ${value}`);
            }
        }
        const [first, second] = frameRange;
        if (second < first) {
            throw new Error('The second value of frame range must be not smaller than the first one, but got ' +
                frameRange.join('-'));
        }
        return;
    }
    throw new TypeError('Frame range must be a number or a tuple of numbers, but got object of type ' +
        typeof frameRange);
};
exports.validateFrameRange = validateFrameRange;
const setFrameRange = (newFrameRange) => {
    (0, exports.validateFrameRange)(newFrameRange);
    range = newFrameRange;
};
exports.setFrameRange = setFrameRange;
const setFrameRangeFromCli = (newFrameRange) => {
    if (typeof newFrameRange === 'number') {
        (0, exports.setFrameRange)(newFrameRange);
        range = newFrameRange;
        return;
    }
    if (typeof newFrameRange === 'string') {
        const parsed = newFrameRange.split('-').map((f) => Number(f));
        if (parsed.length > 2 || parsed.length <= 0) {
            throw new Error(`--frames flag must be a number or 2 numbers separated by '-', instead got ${parsed.length} numbers`);
        }
        if (parsed.length === 2 && parsed[1] < parsed[0]) {
            throw new Error('The second number of the --frames flag number should be greater or equal than first number');
        }
        for (const value of parsed) {
            if (isNaN(value)) {
                throw new Error('--frames flag must be a single number, or 2 numbers separated by `-`');
            }
        }
        (0, exports.setFrameRange)(parsed);
    }
};
exports.setFrameRangeFromCli = setFrameRangeFromCli;
const getRange = () => range;
exports.getRange = getRange;

}(frameRange));

var imageFormat = {};

Object.defineProperty(imageFormat, "__esModule", { value: true });
imageFormat.validateSelectedPixelFormatAndImageFormatCombination = imageFormat.getUserPreferredImageFormat = imageFormat.setImageFormat = void 0;
const validOptions = ['png', 'jpeg', 'none'];
let currentImageFormat;
const setImageFormat = (format) => {
    if (typeof format === 'undefined') {
        currentImageFormat = undefined;
        return;
    }
    if (!validOptions.includes(format)) {
        throw new TypeError(`Value ${format} is not valid as an image format.`);
    }
    currentImageFormat = format;
};
imageFormat.setImageFormat = setImageFormat;
const getUserPreferredImageFormat = () => {
    return currentImageFormat;
};
imageFormat.getUserPreferredImageFormat = getUserPreferredImageFormat;
// By returning a value, we improve testability as we can specifically test certain branches
const validateSelectedPixelFormatAndImageFormatCombination = (pixelFormat, imageFormat) => {
    if (imageFormat === 'none') {
        return 'none';
    }
    if (!validOptions.includes(imageFormat)) {
        throw new TypeError(`Value ${imageFormat} is not valid as an image format.`);
    }
    if (pixelFormat !== 'yuva420p' && pixelFormat !== 'yuva444p10le') {
        return 'valid';
    }
    if (imageFormat !== 'png') {
        throw new TypeError(`Pixel format was set to '${pixelFormat}' but the image format is not PNG. To render transparent videos, you need to set PNG as the image format.`);
    }
    return 'valid';
};
imageFormat.validateSelectedPixelFormatAndImageFormatCombination = validateSelectedPixelFormatAndImageFormatCombination;

var imageSequence$1 = {};

Object.defineProperty(imageSequence$1, "__esModule", { value: true });
imageSequence$1.getShouldOutputImageSequence = imageSequence$1.setImageSequence = void 0;
let imageSequence = false;
const setImageSequence = (newImageSequence) => {
    if (typeof newImageSequence !== 'boolean') {
        throw new TypeError('setImageSequence accepts a Boolean Value');
    }
    imageSequence = newImageSequence;
};
imageSequence$1.setImageSequence = setImageSequence;
const getShouldOutputImageSequence = (frameRange) => {
    return imageSequence || typeof frameRange === 'number';
};
imageSequence$1.getShouldOutputImageSequence = getShouldOutputImageSequence;

var log = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.isEqualOrBelowLogLevel = exports.isValidLogLevel = exports.setLogLevel = exports.getLogLevel = exports.logLevels = void 0;
exports.logLevels = ['verbose', 'info', 'warn', 'error'];
let logLevel = 'info';
const getLogLevel = () => {
    return logLevel;
};
exports.getLogLevel = getLogLevel;
const setLogLevel = (newLogLevel) => {
    logLevel = newLogLevel;
};
exports.setLogLevel = setLogLevel;
const getNumberForLogLevel = (level) => {
    return exports.logLevels.indexOf(level);
};
const isValidLogLevel = (level) => {
    return getNumberForLogLevel(level) > -1;
};
exports.isValidLogLevel = isValidLogLevel;
const isEqualOrBelowLogLevel = (level) => {
    return getNumberForLogLevel(logLevel) <= getNumberForLogLevel(level);
};
exports.isEqualOrBelowLogLevel = isEqualOrBelowLogLevel;

}(log));

var maxTimelineTracks$1 = {};

Object.defineProperty(maxTimelineTracks$1, "__esModule", { value: true });
maxTimelineTracks$1.getMaxTimelineTracks = maxTimelineTracks$1.setMaxTimelineTracks = void 0;
let maxTimelineTracks = 15;
const setMaxTimelineTracks = (maxTracks) => {
    if (typeof maxTracks !== 'number') {
        throw new Error(`Need to pass a number to Config.Preview.setMaxTimelineTracks(), got ${typeof maxTracks}`);
    }
    if (Number.isNaN(maxTracks)) {
        throw new Error(`Need to pass a real number to Config.Preview.setMaxTimelineTracks(), got NaN`);
    }
    if (!Number.isFinite(maxTracks)) {
        throw new Error(`Need to pass a real number to Config.Preview.setMaxTimelineTracks(), got ${maxTracks}`);
    }
    if (maxTracks < 0) {
        throw new Error(`Need to pass a non-negative number to Config.Preview.setMaxTimelineTracks(), got ${maxTracks}`);
    }
    maxTimelineTracks = maxTracks;
};
maxTimelineTracks$1.setMaxTimelineTracks = setMaxTimelineTracks;
const getMaxTimelineTracks = () => {
    return maxTimelineTracks;
};
maxTimelineTracks$1.getMaxTimelineTracks = getMaxTimelineTracks;

var overrideWebpack = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.overrideWebpackConfig = exports.getWebpackOverrideFn = exports.defaultOverrideFunction = void 0;
const defaultOverrideFunction = (config) => config;
exports.defaultOverrideFunction = defaultOverrideFunction;
let overrideFn = exports.defaultOverrideFunction;
const getWebpackOverrideFn = () => {
    return overrideFn;
};
exports.getWebpackOverrideFn = getWebpackOverrideFn;
const overrideWebpackConfig = (fn) => {
    overrideFn = fn;
};
exports.overrideWebpackConfig = overrideWebpackConfig;

}(overrideWebpack));

var overwrite = {};

Object.defineProperty(overwrite, "__esModule", { value: true });
overwrite.getShouldOverwrite = overwrite.setOverwriteOutput = void 0;
let shouldOverwrite = true;
const setOverwriteOutput = (newOverwrite) => {
    if (typeof newOverwrite !== 'boolean') {
        throw new Error(`overwriteExisting must be a boolean but got ${typeof newOverwrite} (${JSON.stringify(newOverwrite)})`);
    }
    shouldOverwrite = newOverwrite;
};
overwrite.setOverwriteOutput = setOverwriteOutput;
const getShouldOverwrite = () => shouldOverwrite;
overwrite.getShouldOverwrite = getShouldOverwrite;

var pixelFormat = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateSelectedPixelFormatAndCodecCombination = exports.getPixelFormat = exports.setPixelFormat = exports.DEFAULT_PIXEL_FORMAT = void 0;
const validOptions = [
    'yuv420p',
    'yuva420p',
    'yuv422p',
    'yuv444p',
    'yuv420p10le',
    'yuv422p10le',
    'yuv444p10le',
    'yuva444p10le',
];
exports.DEFAULT_PIXEL_FORMAT = 'yuv420p';
let currentPixelFormat = exports.DEFAULT_PIXEL_FORMAT;
const setPixelFormat = (format) => {
    if (!validOptions.includes(format)) {
        throw new TypeError(`Value ${format} is not valid as a pixel format.`);
    }
    currentPixelFormat = format;
};
exports.setPixelFormat = setPixelFormat;
const getPixelFormat = () => {
    return currentPixelFormat;
};
exports.getPixelFormat = getPixelFormat;
const validateSelectedPixelFormatAndCodecCombination = (pixelFormat, codec) => {
    if (!validOptions.includes(pixelFormat)) {
        throw new TypeError(`Value ${pixelFormat} is not valid as a pixel format.`);
    }
    if (pixelFormat !== 'yuva420p') {
        return;
    }
    if (codec !== 'vp8' && codec !== 'vp9') {
        throw new TypeError("Pixel format was set to 'yuva420p' but codec is not 'vp8' or 'vp9'. To render videos with alpha channel, you must choose a codec that supports it.");
    }
};
exports.validateSelectedPixelFormatAndCodecCombination = validateSelectedPixelFormatAndCodecCombination;

}(pixelFormat));

var previewServer = {};

Object.defineProperty(previewServer, "__esModule", { value: true });
previewServer.getServerPort = previewServer.setPort = void 0;
let serverPort;
const setPort = (port) => {
    if (!['number', 'undefined'].includes(typeof port)) {
        throw new Error(`Preview server port should be a number. Got ${typeof port} (${JSON.stringify(port)})`);
    }
    if (port === undefined) {
        serverPort = undefined;
        return;
    }
    if (port < 1 || port > 65535) {
        throw new Error(`Preview server port should be a number between 1 and 65535. Got ${port}`);
    }
    serverPort = port;
};
previewServer.setPort = setPort;
const getServerPort = () => serverPort;
previewServer.getServerPort = getServerPort;

var proresProfile = {};

Object.defineProperty(proresProfile, "__esModule", { value: true });
proresProfile.validateSelectedCodecAndProResCombination = proresProfile.setProResProfile = proresProfile.getProResProfile = void 0;
const proResProfileOptions = [
    '4444-xq',
    '4444',
    'hq',
    'standard',
    'light',
    'proxy',
];
let proResProfile;
const getProResProfile = () => {
    return proResProfile;
};
proresProfile.getProResProfile = getProResProfile;
const setProResProfile = (profile) => {
    proResProfile = profile;
};
proresProfile.setProResProfile = setProResProfile;
const validateSelectedCodecAndProResCombination = (actualCodec, actualProResProfile) => {
    if (typeof actualProResProfile !== 'undefined' && actualCodec !== 'prores') {
        throw new TypeError('You have set a ProRes profile but the codec is not "prores". Set the codec to "prores" or remove the ProRes profile.');
    }
    if (actualProResProfile !== undefined &&
        !proResProfileOptions.includes(actualProResProfile)) {
        throw new TypeError(`The ProRes profile "${actualProResProfile}" is not valid. Valid options are ${proResProfileOptions
            .map((p) => `"${p}"`)
            .join(', ')}`);
    }
};
proresProfile.validateSelectedCodecAndProResCombination = validateSelectedCodecAndProResCombination;

var quality$1 = {};

var validateQuality$1 = {};

Object.defineProperty(validateQuality$1, "__esModule", { value: true });
validateQuality$1.validateQuality = void 0;
const validateQuality = (q) => {
    if (typeof q !== 'undefined' && typeof q !== 'number') {
        throw new Error(`Quality option must be a number or undefined. Got ${typeof q} (${JSON.stringify(q)})`);
    }
    if (typeof q === 'undefined') {
        return;
    }
    if (!Number.isFinite(q)) {
        throw new RangeError(`Quality must be a finite number, but is ${q}`);
    }
    if (Number.isNaN(q)) {
        throw new RangeError(`Quality is NaN, but must be a real number`);
    }
    if (q > 100 || q < 0) {
        throw new RangeError('Quality option must be between 0 and 100.');
    }
};
validateQuality$1.validateQuality = validateQuality;

Object.defineProperty(quality$1, "__esModule", { value: true });
quality$1.getQuality = quality$1.setQuality = void 0;
const validate_quality_1$1 = validateQuality$1;
const defaultValue = undefined;
let quality = defaultValue;
const setQuality = (q) => {
    (0, validate_quality_1$1.validateQuality)(q);
    if (q === 0 || q === undefined) {
        quality = defaultValue;
        return;
    }
    quality = q;
};
quality$1.setQuality = setQuality;
const getQuality = () => quality;
quality$1.getQuality = getQuality;

var webpackCaching = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.getWebpackCaching = exports.setWebpackCaching = exports.DEFAULT_WEBPACK_CACHE_ENABLED = void 0;
exports.DEFAULT_WEBPACK_CACHE_ENABLED = true;
let webpackCaching = exports.DEFAULT_WEBPACK_CACHE_ENABLED;
const setWebpackCaching = (flag) => {
    if (typeof flag !== 'boolean') {
        throw new TypeError('Caching flag must be a boolean.');
    }
    webpackCaching = flag;
};
exports.setWebpackCaching = setWebpackCaching;
const getWebpackCaching = () => {
    return webpackCaching;
};
exports.getWebpackCaching = getWebpackCaching;

}(webpackCaching));

var ffmpegExecutable = {};

Object.defineProperty(ffmpegExecutable, "__esModule", { value: true });
ffmpegExecutable.getCustomFfmpegExecutable = ffmpegExecutable.setFfmpegExecutable = void 0;
let currentFfmpegExecutablePath = null;
const setFfmpegExecutable = (ffmpegPath) => {
    currentFfmpegExecutablePath = ffmpegPath;
};
ffmpegExecutable.setFfmpegExecutable = setFfmpegExecutable;
const getCustomFfmpegExecutable = () => {
    return currentFfmpegExecutablePath;
};
ffmpegExecutable.getCustomFfmpegExecutable = getCustomFfmpegExecutable;

Object.defineProperty(config, "__esModule", { value: true });
config.Config = void 0;
const browser_executable_1$1 = browserExecutable;
const codec_1$1 = codec;
const concurrency_1$1 = concurrency;
const crf_1$1 = crf;
const env_file_1$1 = envFile$1;
const frame_range_1$1 = frameRange;
const image_format_1$1 = imageFormat;
const image_sequence_1$1 = imageSequence$1;
const log_1 = log;
const max_timeline_tracks_1$1 = maxTimelineTracks$1;
const override_webpack_1$1 = overrideWebpack;
const overwrite_1$1 = overwrite;
const pixel_format_1$1 = pixelFormat;
const preview_server_1$1 = previewServer;
const prores_profile_1$1 = proresProfile;
const quality_1$1 = quality$1;
const webpack_caching_1$1 = webpackCaching;
const ffmpeg_executable_1$1 = ffmpegExecutable;
config.Config = {
    Preview: {
        /**
         * Change the maximum amount of tracks that are shown in the timeline.
         * @param maxTracks The maximum amount of timeline tracks that you would like to show.
         * @default 15
         */
        setMaxTimelineTracks: max_timeline_tracks_1$1.setMaxTimelineTracks,
    },
    Bundling: {
        /**
         * Pass in a function which takes the current Webpack config
         * and return a modified Webpack configuration.
         * Docs: http://remotion.dev/docs/webpack
         */
        overrideWebpackConfig: override_webpack_1$1.overrideWebpackConfig,
        /**
         * Whether Webpack bundles should be cached to make
         * subsequent renders faster. Default: true
         */
        setCachingEnabled: webpack_caching_1$1.setWebpackCaching,
        /**
         * Define on which port Remotion should start it's HTTP servers during preview and rendering.
         * By default, Remotion will try to find a free port.
         * If you specify a port, but it's not available, Remotion will throw an error.
         */
        setPort: preview_server_1$1.setPort,
    },
    Log: {
        /**
         * Set the log level.
         * Acceptable values: 'error' | 'warning' | 'info' | 'verbose'
         * Default value: 'info'
         *
         * Set this to 'verbose' to get browser logs and other IO.
         */
        setLevel: log_1.setLogLevel,
    },
    Puppeteer: {
        /**
         * Specify executable path for the browser to use.
         * Default: null, which will make Remotion find or download a version of said browser.
         */
        setBrowserExecutable: browser_executable_1$1.setBrowserExecutable,
    },
    Rendering: {
        /**
         * Set a custom location for a .env file.
         * Default: `.env`
         */
        setDotEnvLocation: env_file_1$1.setDotEnvLocation,
        /**
         * Sets how many Puppeteer instances will work on rendering your video in parallel.
         * Default: `null`, meaning half of the threads available on your CPU.
         */
        setConcurrency: concurrency_1$1.setConcurrency,
        /**
         * Set the JPEG quality for the frames.
         * Must be between 0 and 100.
         * Must be between 0 and 100.
         * Default: 80
         */
        setQuality: quality_1$1.setQuality,
        /** Decide in which image format to render. Can be either 'jpeg' or 'png'.
         * PNG is slower, but supports transparency.
         */
        setImageFormat: image_format_1$1.setImageFormat,
        /**
         * Render only a subset of a video.
         * Pass in a tuple [20, 30] to only render frames 20-30 into a video.
         * Pass in a single number `20` to only render a single frame as an image.
         * The frame count starts at 0.
         */
        setFrameRange: frame_range_1$1.setFrameRange,
        /**
         * Specify local ffmpeg executable.
         * Default: null, which will use ffmpeg available in PATH.
         */
        setFfmpegExecutable: ffmpeg_executable_1$1.setFfmpegExecutable,
    },
    Output: {
        /**
         * If the video file already exists, should Remotion overwrite
         * the output? Default: true
         */
        setOverwriteOutput: overwrite_1$1.setOverwriteOutput,
        /**
         * Sets the pixel format in FFMPEG.
         * See https://trac.ffmpeg.org/wiki/Chroma%20Subsampling for an explanation.
         * You can override this using the `--pixel-format` Cli flag.
         */
        setPixelFormat: pixel_format_1$1.setPixelFormat,
        /**
         * @deprecated Use setCodec() and setImageSequence() instead.
         * Specify what kind of output you, either `mp4` or `png-sequence`.
         */
        setOutputFormat: codec_1$1.setOutputFormat,
        /**
         * Specify the codec for stitching the frames into a video.
         * Can be `h264` (default), `h265`, `vp8` or `vp9`
         */
        setCodec: codec_1$1.setCodec,
        /**
         * Set the Constant Rate Factor to pass to FFMPEG.
         * Lower values mean better quality, but be aware that the ranges of
         * possible values greatly differs between codecs.
         */
        setCrf: crf_1$1.setCrf,
        /**
         * Set to true if don't want a video but an image sequence as the output.
         */
        setImageSequence: image_sequence_1$1.setImageSequence,
        /**
         * Set the ProRes profile.
         * This method is only valid if the codec has been set to 'prores'.
         * Possible values: 4444-xq, 4444, hq, standard, light, proxy. Default: 'hq'
         * See https://avpres.net/FFmpeg/im_ProRes.html for meaning of possible values.
         */
        setProResProfile: prores_profile_1$1.setProResProfile,
    },
};

var inputProps = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.getInputProps = exports.INPUT_PROPS_KEY = void 0;
const get_environment_1 = getEnvironment;
exports.INPUT_PROPS_KEY = 'remotion.inputProps';
let didWarnSSRImport = false;
const warnOnceSSRImport = () => {
    if (didWarnSSRImport) {
        return;
    }
    didWarnSSRImport = true;
    console.warn('Called `getInputProps()` on the server. This function is not available server-side and has returned an empty object.');
    console.warn("To hide this warning, don't call this function on the server:");
    console.warn("  typeof window === 'undefined' ? {} : getInputProps()");
};
const getInputProps = () => {
    if ((0, get_environment_1.getRemotionEnvironment)() === 'rendering') {
        if (typeof window === 'undefined') {
            warnOnceSSRImport();
            return {};
        }
        const param = localStorage.getItem(exports.INPUT_PROPS_KEY);
        if (!param) {
            return {};
        }
        const parsed = JSON.parse(param);
        return parsed;
    }
    if ((0, get_environment_1.getRemotionEnvironment)() === 'preview') {
        return process.env.INPUT_PROPS;
    }
    throw new Error('You cannot call `getInputProps()` from a <Player>. Instead, the props are available as React props from component that you passed as `component` prop.');
};
exports.getInputProps = getInputProps;

}(inputProps));

var easing = {};

var bezier$1 = {};

// Taken from https://github.com/facebook/react-native/blob/0b9ea60b4fee8cacc36e7160e31b91fc114dbc0d/Libraries/Animated/src/bezier.js
Object.defineProperty(bezier$1, "__esModule", { value: true });
bezier$1.bezier = void 0;
const NEWTON_ITERATIONS = 4;
const NEWTON_MIN_SLOPE = 0.001;
const SUBDIVISION_PRECISION = 0.0000001;
const SUBDIVISION_MAX_ITERATIONS = 10;
const kSplineTableSize = 11;
const kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);
const float32ArraySupported = typeof Float32Array === 'function';
function a(aA1, aA2) {
    return 1.0 - 3.0 * aA2 + 3.0 * aA1;
}
function b(aA1, aA2) {
    return 3.0 * aA2 - 6.0 * aA1;
}
function c(aA1) {
    return 3.0 * aA1;
}
// Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
function calcBezier(aT, aA1, aA2) {
    return ((a(aA1, aA2) * aT + b(aA1, aA2)) * aT + c(aA1)) * aT;
}
// Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
function getSlope(aT, aA1, aA2) {
    return 3.0 * a(aA1, aA2) * aT * aT + 2.0 * b(aA1, aA2) * aT + c(aA1);
}
function binarySubdivide({ aX, _aA, _aB, mX1, mX2, }) {
    let currentX;
    let currentT;
    let i = 0;
    let aA = _aA;
    let aB = _aB;
    do {
        currentT = aA + (aB - aA) / 2.0;
        currentX = calcBezier(currentT, mX1, mX2) - aX;
        if (currentX > 0.0) {
            aB = currentT;
        }
        else {
            aA = currentT;
        }
    } while (Math.abs(currentX) > SUBDIVISION_PRECISION &&
        ++i < SUBDIVISION_MAX_ITERATIONS);
    return currentT;
}
function newtonRaphsonIterate(aX, _aGuessT, mX1, mX2) {
    let aGuessT = _aGuessT;
    for (let i = 0; i < NEWTON_ITERATIONS; ++i) {
        const currentSlope = getSlope(aGuessT, mX1, mX2);
        if (currentSlope === 0.0) {
            return aGuessT;
        }
        const currentX = calcBezier(aGuessT, mX1, mX2) - aX;
        aGuessT -= currentX / currentSlope;
    }
    return aGuessT;
}
function bezier(mX1, mY1, mX2, mY2) {
    if (!(mX1 >= 0 && mX1 <= 1 && mX2 >= 0 && mX2 <= 1)) {
        throw new Error('bezier x values must be in [0, 1] range');
    }
    // Precompute samples table
    const sampleValues = float32ArraySupported
        ? new Float32Array(kSplineTableSize)
        : new Array(kSplineTableSize);
    if (mX1 !== mY1 || mX2 !== mY2) {
        for (let i = 0; i < kSplineTableSize; ++i) {
            sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
        }
    }
    function getTForX(aX) {
        let intervalStart = 0.0;
        let currentSample = 1;
        const lastSample = kSplineTableSize - 1;
        for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
            intervalStart += kSampleStepSize;
        }
        --currentSample;
        // Interpolate to provide an initial guess for t
        const dist = (aX - sampleValues[currentSample]) /
            (sampleValues[currentSample + 1] - sampleValues[currentSample]);
        const guessForT = intervalStart + dist * kSampleStepSize;
        const initialSlope = getSlope(guessForT, mX1, mX2);
        if (initialSlope >= NEWTON_MIN_SLOPE) {
            return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
        }
        if (initialSlope === 0.0) {
            return guessForT;
        }
        return binarySubdivide({
            aX,
            _aA: intervalStart,
            _aB: intervalStart + kSampleStepSize,
            mX1,
            mX2,
        });
    }
    return function (x) {
        if (mX1 === mY1 && mX2 === mY2) {
            return x; // linear
        }
        // Because JavaScript number are imprecise, we should guarantee the extremes are right.
        if (x === 0) {
            return 0;
        }
        if (x === 1) {
            return 1;
        }
        return calcBezier(getTForX(x), mY1, mY2);
    };
}
bezier$1.bezier = bezier;

// Taken from https://github.com/facebook/react-native/blob/0b9ea60b4fee8cacc36e7160e31b91fc114dbc0d/Libraries/Animated/src/Easing.js
Object.defineProperty(easing, "__esModule", { value: true });
easing.Easing = void 0;
const bezier_1 = bezier$1;
class Easing {
    static step0(n) {
        return n > 0 ? 1 : 0;
    }
    static step1(n) {
        return n >= 1 ? 1 : 0;
    }
    static linear(t) {
        return t;
    }
    static ease(t) {
        return Easing.bezier(0.42, 0, 1, 1)(t);
    }
    static quad(t) {
        return t * t;
    }
    static cubic(t) {
        return t * t * t;
    }
    static poly(n) {
        return (t) => t ** n;
    }
    static sin(t) {
        return 1 - Math.cos((t * Math.PI) / 2);
    }
    static circle(t) {
        return 1 - Math.sqrt(1 - t * t);
    }
    static exp(t) {
        return 2 ** (10 * (t - 1));
    }
    static elastic(bounciness = 1) {
        const p = bounciness * Math.PI;
        return (t) => 1 - Math.cos((t * Math.PI) / 2) ** 3 * Math.cos(t * p);
    }
    static back(s = 1.70158) {
        return (t) => t * t * ((s + 1) * t - s);
    }
    static bounce(t) {
        if (t < 1 / 2.75) {
            return 7.5625 * t * t;
        }
        if (t < 2 / 2.75) {
            const t2_ = t - 1.5 / 2.75;
            return 7.5625 * t2_ * t2_ + 0.75;
        }
        if (t < 2.5 / 2.75) {
            const t2_ = t - 2.25 / 2.75;
            return 7.5625 * t2_ * t2_ + 0.9375;
        }
        const t2 = t - 2.625 / 2.75;
        return 7.5625 * t2 * t2 + 0.984375;
    }
    static bezier(x1, y1, x2, y2) {
        return (0, bezier_1.bezier)(x1, y1, x2, y2);
    }
    static in(easing) {
        return easing;
    }
    static out(easing) {
        return (t) => 1 - easing(1 - t);
    }
    static inOut(easing) {
        return (t) => {
            if (t < 0.5) {
                return easing(t * 2) / 2;
            }
            return 1 - easing((1 - t) * 2) / 2;
        };
    }
}
easing.Easing = Easing;

var freeze = {};

Object.defineProperty(freeze, "__esModule", { value: true });
freeze.Freeze = void 0;
const jsx_runtime_1$8 = jsxRuntime.exports;
const react_1$6 = react.exports;
const timeline_position_state_1$1 = timelinePositionState;
const Freeze = ({ frame, children }) => {
    if (typeof frame === 'undefined') {
        throw new Error(`The <Freeze /> component requires a 'frame' prop, but none was passed.`);
    }
    if (typeof frame !== 'number') {
        throw new Error(`The 'frame' prop of <Freeze /> must be a number, but is of type ${typeof frame}`);
    }
    if (Number.isNaN(frame)) {
        throw new Error(`The 'frame' prop of <Freeze /> must be a real number, but it is NaN.`);
    }
    if (!Number.isFinite(frame)) {
        throw new Error(`The 'frame' prop of <Freeze /> must be a finite number, but it is ${frame}.`);
    }
    const context = (0, react_1$6.useContext)(timeline_position_state_1$1.TimelineContext);
    const value = (0, react_1$6.useMemo)(() => {
        return {
            ...context,
            playing: false,
            imperativePlaying: {
                current: false,
            },
            frame,
        };
    }, [context, frame]);
    return ((0, jsx_runtime_1$8.jsx)(timeline_position_state_1$1.TimelineContext.Provider, Object.assign({ value: value }, { children: children }), void 0));
};
freeze.Freeze = Freeze;

var IFrame = {};

var readyManager = {};

Object.defineProperty(readyManager, "__esModule", { value: true });
readyManager.continueRender = readyManager.delayRender = void 0;
const get_environment_1$1 = getEnvironment;
if (typeof window !== 'undefined') {
    window.ready = false;
}
let handles = [];
const timeouts = {};
const delayRender = () => {
    var _a, _b;
    const handle = Math.random();
    handles.push(handle);
    const called = (_b = (_a = Error().stack) === null || _a === void 0 ? void 0 : _a.replace(/^Error/g, '')) !== null && _b !== void 0 ? _b : '';
    if ((0, get_environment_1$1.getRemotionEnvironment)() === 'rendering') {
        timeouts[handle] = setTimeout(() => {
            throw new Error('A delayRender was called but not cleared after 25000ms. See https://remotion.dev/docs/timeout for help. The delayRender was called: ' +
                called);
        }, 25000);
    }
    if (typeof window !== 'undefined') {
        window.ready = false;
    }
    return handle;
};
readyManager.delayRender = delayRender;
const continueRender = (handle) => {
    handles = handles.filter((h) => {
        if (h === handle) {
            if ((0, get_environment_1$1.getRemotionEnvironment)() === 'rendering') {
                clearTimeout(timeouts[handle]);
            }
            return false;
        }
        return true;
    });
    if (handles.length === 0 && typeof window !== 'undefined') {
        window.ready = true;
    }
};
readyManager.continueRender = continueRender;

Object.defineProperty(IFrame, "__esModule", { value: true });
IFrame.IFrame = void 0;
const jsx_runtime_1$7 = jsxRuntime.exports;
const react_1$5 = react.exports;
const ready_manager_1$2 = readyManager;
const IFrameRefForwarding = ({ onLoad, onError, ...props }, ref) => {
    const [handle] = (0, react_1$5.useState)(() => (0, ready_manager_1$2.delayRender)());
    const didLoad = (0, react_1$5.useCallback)((e) => {
        (0, ready_manager_1$2.continueRender)(handle);
        onLoad === null || onLoad === void 0 ? void 0 : onLoad(e);
    }, [handle, onLoad]);
    const didGetError = (0, react_1$5.useCallback)((e) => {
        (0, ready_manager_1$2.continueRender)(handle);
        if (onError) {
            onError(e);
        }
        else {
            console.error('Error loading iframe:', e, 'Handle the event using the onError() prop to make this message disappear.');
        }
    }, [handle, onError]);
    return (0, jsx_runtime_1$7.jsx)("iframe", Object.assign({}, props, { ref: ref, onError: didGetError, onLoad: didLoad }), void 0);
};
IFrame.IFrame = (0, react_1$5.forwardRef)(IFrameRefForwarding);

var Img = {};

Object.defineProperty(Img, "__esModule", { value: true });
Img.Img = void 0;
const jsx_runtime_1$6 = jsxRuntime.exports;
const react_1$4 = react.exports;
const ready_manager_1$1 = readyManager;
const ImgRefForwarding = ({ onLoad, onError, ...props }, ref) => {
    const [handle] = (0, react_1$4.useState)(() => (0, ready_manager_1$1.delayRender)());
    (0, react_1$4.useEffect)(() => {
        if (ref &&
            ref.current.complete) {
            (0, ready_manager_1$1.continueRender)(handle);
        }
    }, [handle, ref]);
    const didLoad = (0, react_1$4.useCallback)((e) => {
        (0, ready_manager_1$1.continueRender)(handle);
        onLoad === null || onLoad === void 0 ? void 0 : onLoad(e);
    }, [handle, onLoad]);
    const didGetError = (0, react_1$4.useCallback)((e) => {
        (0, ready_manager_1$1.continueRender)(handle);
        if (onError) {
            onError(e);
        }
        else {
            console.error('Error loading image:', e, 'Handle the event using the onError() prop to make this message disappear.');
        }
    }, [handle, onError]);
    return (0, jsx_runtime_1$6.jsx)("img", Object.assign({}, props, { ref: ref, onLoad: didLoad, onError: didGetError }), void 0);
};
Img.Img = (0, react_1$4.forwardRef)(ImgRefForwarding);

var internals = {};

var browser = {};

Object.defineProperty(browser, "__esModule", { value: true });
browser.getBrowser = browser.setBrowser = browser.DEFAULT_BROWSER = void 0;
browser.DEFAULT_BROWSER = 'chrome';
let currentBrowser = null;
const setBrowser = (browser) => {
    if (browser === 'chrome') {
        process.env.PUPPETEER_PRODUCT = 'chrome';
    }
    if (browser === 'firefox') {
        process.env.PUPPETEER_PRODUCT = 'firefox';
    }
    currentBrowser = browser;
};
browser.setBrowser = setBrowser;
const getBrowser = () => {
    return currentBrowser;
};
browser.getBrowser = getBrowser;

var stillFrame$1 = {};

var validateFrame$1 = {};

Object.defineProperty(validateFrame$1, "__esModule", { value: true });
validateFrame$1.validateFrame = void 0;
const validateFrame = (frame, durationInFrames) => {
    if (typeof frame === 'undefined') {
        throw new TypeError(`Argument missing for parameter "frame"`);
    }
    if (typeof frame !== 'number') {
        throw new TypeError(`Argument passed for "frame" is not a number: ${frame}`);
    }
    if (frame < 0) {
        throw new RangeError(`Frame ${frame} cannot be negative`);
    }
    if (!Number.isFinite(frame)) {
        throw new RangeError(`Frame ${frame} is not finite`);
    }
    if (frame % 1 !== 0) {
        throw new RangeError(`Argument for frame must be an integer, but got ${frame}`);
    }
    if (frame > durationInFrames - 1) {
        throw new RangeError(`Cannot use frame ${frame}: Duration of composition is ${durationInFrames}, therefore the highest frame that can be rendered is ${durationInFrames - 1}`);
    }
};
validateFrame$1.validateFrame = validateFrame;

Object.defineProperty(stillFrame$1, "__esModule", { value: true });
stillFrame$1.getStillFrame = stillFrame$1.setStillFrame = void 0;
const validate_frame_1$1 = validateFrame$1;
let stillFrame = 0;
const setStillFrame = (frame) => {
    (0, validate_frame_1$1.validateFrame)(frame, Infinity);
    stillFrame = frame;
};
stillFrame$1.setStillFrame = setStillFrame;
const getStillFrame = () => stillFrame;
stillFrame$1.getStillFrame = getStillFrame;

var defaultCss = {};

Object.defineProperty(defaultCss, "__esModule", { value: true });
defaultCss.makeDefaultCSS = defaultCss.injectCSS = void 0;
const injected = {};
const injectCSS = (css) => {
    // Skip in node
    if (typeof document === 'undefined') {
        return;
    }
    if (injected[css]) {
        return;
    }
    const head = document.head || document.getElementsByTagName('head')[0];
    const style = document.createElement('style');
    style.appendChild(document.createTextNode(css));
    head.appendChild(style);
    injected[css] = true;
};
defaultCss.injectCSS = injectCSS;
const makeDefaultCSS = (scope) => {
    if (!scope) {
        return `
    * {
      box-sizing: border-box;
    }
    body {
      margin: 0;
			background-color: #1f2428
    }
    `;
    }
    return `
    ${scope} * {
      box-sizing: border-box;
    }
    ${scope} *:-webkit-full-screen {
      width: 100%;
      height: 100%;
    }
  `;
};
defaultCss.makeDefaultCSS = makeDefaultCSS;

var featureFlags = {};

Object.defineProperty(featureFlags, "__esModule", { value: true });
featureFlags.FEATURE_FLAG_FIREFOX_SUPPORT = void 0;
// There is no Firefox support yet, the waitForFunction method
// does not yet work and downloading has a bug.
// Disabling this for now!
featureFlags.FEATURE_FLAG_FIREFOX_SUPPORT = false;

var initialFrame = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.setupInitialFrame = exports.INITIAL_FRAME_LOCAL_STORAGE_KEY = void 0;
exports.INITIAL_FRAME_LOCAL_STORAGE_KEY = 'remotion.initialFrame';
const getInitialFrame = () => {
    const param = localStorage.getItem(exports.INITIAL_FRAME_LOCAL_STORAGE_KEY);
    return param ? Number(param) : 0;
};
const setupInitialFrame = () => {
    window.remotion_initialFrame = getInitialFrame();
};
exports.setupInitialFrame = setupInitialFrame;

}(initialFrame));

var perf$2 = {};

Object.defineProperty(perf$2, "__esModule", { value: true });
perf$2.logPerf = perf$2.stopPerfMeasure = perf$2.startPerfMeasure = void 0;
const perf$1 = {
    'activate-target': [],
    capture: [],
    save: [],
};
const map = {};
const startPerfMeasure = (marker) => {
    const id = Math.random();
    map[id] = {
        id,
        marker,
        start: Date.now(),
    };
    return id;
};
perf$2.startPerfMeasure = startPerfMeasure;
const stopPerfMeasure = (id) => {
    const now = Date.now();
    const diff = now - map[id].start;
    perf$1[map[id].marker].push(diff);
    delete map[id];
};
perf$2.stopPerfMeasure = stopPerfMeasure;
const logPerf = () => {
    console.log('Render performance:');
    Object.keys(perf$1).forEach((p) => {
        console.log(`  ${p} => ${perf$1[p].reduce((a, b) => a + b, 0) / perf$1[p].length} (n = ${perf$1[p].length})`);
    });
};
perf$2.logPerf = logPerf;

var RemotionRoot = {exports: {}};

(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.RemotionRoot = void 0;
const jsx_runtime_1 = jsxRuntime.exports;
const react_1 = react.exports;
const shared_audio_tags_1 = sharedAudioTags;
const CompositionManager_1 = CompositionManager;
const nonce_1 = nonce;
const random_1 = random$1;
const ready_manager_1 = readyManager;
const timeline_position_state_1 = timelinePositionState;
const RemotionRoot = ({ children }) => {
    var _a;
    const [remotionRootId] = (0, react_1.useState)(() => String((0, random_1.random)(null)));
    const [frame, setFrame] = (0, react_1.useState)((_a = window.remotion_initialFrame) !== null && _a !== void 0 ? _a : 0);
    const [playing, setPlaying] = (0, react_1.useState)(false);
    const imperativePlaying = (0, react_1.useRef)(false);
    const [fastRefreshes, setFastRefreshes] = (0, react_1.useState)(0);
    const [playbackRate, setPlaybackRate] = (0, react_1.useState)(1);
    const audioAndVideoTags = (0, react_1.useRef)([]);
    (0, react_1.useLayoutEffect)(() => {
        if (typeof window !== 'undefined') {
            window.remotion_setFrame = (f) => {
                const id = (0, ready_manager_1.delayRender)();
                setFrame(f);
                requestAnimationFrame(() => (0, ready_manager_1.continueRender)(id));
            };
            window.remotion_isPlayer = false;
        }
    }, []);
    const timelineContextValue = (0, react_1.useMemo)(() => {
        return {
            frame,
            playing,
            imperativePlaying,
            rootId: remotionRootId,
            playbackRate,
            setPlaybackRate,
            audioAndVideoTags,
        };
    }, [frame, playbackRate, playing, remotionRootId]);
    const setTimelineContextValue = (0, react_1.useMemo)(() => {
        return {
            setFrame,
            setPlaying,
        };
    }, []);
    const nonceContext = (0, react_1.useMemo)(() => {
        let counter = 0;
        return {
            getNonce: () => counter++,
            fastRefreshes,
        };
    }, [fastRefreshes]);
    (0, react_1.useEffect)(() => {
        if (module.hot) {
            module.hot.addStatusHandler((status) => {
                if (status === 'idle') {
                    setFastRefreshes((i) => i + 1);
                }
            });
        }
    }, []);
    return ((0, jsx_runtime_1.jsx)(nonce_1.NonceContext.Provider, Object.assign({ value: nonceContext }, { children: (0, jsx_runtime_1.jsx)(timeline_position_state_1.TimelineContext.Provider, Object.assign({ value: timelineContextValue }, { children: (0, jsx_runtime_1.jsx)(timeline_position_state_1.SetTimelineContext.Provider, Object.assign({ value: setTimelineContextValue }, { children: (0, jsx_runtime_1.jsx)(CompositionManager_1.CompositionManagerProvider, { children: (0, jsx_runtime_1.jsx)(shared_audio_tags_1.SharedAudioContextProvider
                    // In the preview, which is mostly played on Desktop, we opt out of the autoplay policy fix as described in https://github.com/remotion-dev/remotion/pull/554, as it mostly applies to mobile.
                    , Object.assign({ 
                        // In the preview, which is mostly played on Desktop, we opt out of the autoplay policy fix as described in https://github.com/remotion-dev/remotion/pull/554, as it mostly applies to mobile.
                        numberOfAudioTags: 0 }, { children: children }), void 0) }, void 0) }), void 0) }), void 0) }), void 0));
};
exports.RemotionRoot = RemotionRoot;

}(RemotionRoot, RemotionRoot.exports));

var setupEnvVariables = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.setupEnvVariables = exports.ENV_VARIABLES_ENV_NAME = exports.ENV_VARIABLES_LOCAL_STORAGE_KEY = void 0;
const get_environment_1 = getEnvironment;
exports.ENV_VARIABLES_LOCAL_STORAGE_KEY = 'remotion.envVariables';
exports.ENV_VARIABLES_ENV_NAME = 'ENV_VARIABLES';
const getEnvVariables = () => {
    if ((0, get_environment_1.getRemotionEnvironment)() === 'rendering') {
        const param = localStorage.getItem(exports.ENV_VARIABLES_LOCAL_STORAGE_KEY);
        if (!param) {
            return {};
        }
        return { ...JSON.parse(param), NODE_ENV: process.env.NODE_ENV };
    }
    if ((0, get_environment_1.getRemotionEnvironment)() === 'preview') {
        // Webpack will convert this to an object at compile time.
        // Don't convert this syntax to a computed property.
        return {
            ...process.env.ENV_VARIABLES,
            NODE_ENV: process.env.NODE_ENV,
        };
    }
    throw new Error('Can only call getEnvVariables() if environment is `rendering` or `preview`');
};
const setupEnvVariables = () => {
    const env = getEnvVariables();
    if (!window.process) {
        window.process = {};
    }
    if (!window.process.env) {
        window.process.env = {};
    }
    Object.keys(env).forEach((key) => {
        window.process.env[key] = env[key];
    });
};
exports.setupEnvVariables = setupEnvVariables;

}(setupEnvVariables));

var timelineInoutPositionState = {};

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.useTimelineSetInOutFramePosition = exports.useTimelineInOutFramePosition = exports.SetTimelineInOutContext = exports.TimelineInOutContext = void 0;
const react_1 = react.exports;
exports.TimelineInOutContext = (0, react_1.createContext)({
    inFrame: null,
    outFrame: null,
});
exports.SetTimelineInOutContext = (0, react_1.createContext)({
    setInAndOutFrames: () => {
        throw new Error('default');
    },
});
const useTimelineInOutFramePosition = () => {
    const state = (0, react_1.useContext)(exports.TimelineInOutContext);
    return state;
};
exports.useTimelineInOutFramePosition = useTimelineInOutFramePosition;
const useTimelineSetInOutFramePosition = () => {
    const { setInAndOutFrames } = (0, react_1.useContext)(exports.SetTimelineInOutContext);
    return { setInAndOutFrames };
};
exports.useTimelineSetInOutFramePosition = useTimelineSetInOutFramePosition;

}(timelineInoutPositionState));

var truthy$1 = {};

Object.defineProperty(truthy$1, "__esModule", { value: true });
truthy$1.truthy = void 0;
function truthy(value) {
    return Boolean(value);
}
truthy$1.truthy = truthy;

var validateImageFormat = {};

Object.defineProperty(validateImageFormat, "__esModule", { value: true });
validateImageFormat.validateNonNullImageFormat = void 0;
const validateNonNullImageFormat = (imageFormat) => {
    if (imageFormat !== 'jpeg' && imageFormat !== 'png') {
        throw new TypeError('Image format should be either "png" or "jpeg"');
    }
};
validateImageFormat.validateNonNullImageFormat = validateNonNullImageFormat;

var wrapRemotionContext = {};

var __createBinding$1 = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault$1 = (commonjsGlobal && commonjsGlobal.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar$1 = (commonjsGlobal && commonjsGlobal.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding$1(result, mod, k);
    __setModuleDefault$1(result, mod);
    return result;
};
Object.defineProperty(wrapRemotionContext, "__esModule", { value: true });
wrapRemotionContext.RemotionContextProvider = wrapRemotionContext.useRemotionContexts = void 0;
const jsx_runtime_1$5 = jsxRuntime.exports;
// This is used for when other reconcilers are being used
// such as in React Three Fiber. All the contexts need to be passed again
// for them to be useable
const react_1$3 = __importStar$1(react.exports);
const CompositionManager_1$2 = CompositionManager;
const nonce_1 = nonce;
const sequencing_1$3 = sequencing;
const timeline_position_state_1 = timelinePositionState;
function useRemotionContexts() {
    const compositionManagerCtx = react_1$3.default.useContext(CompositionManager_1$2.CompositionManager);
    const timelineContext = react_1$3.default.useContext(timeline_position_state_1.TimelineContext);
    const setTimelineContext = react_1$3.default.useContext(timeline_position_state_1.SetTimelineContext);
    const sequenceContext = react_1$3.default.useContext(sequencing_1$3.SequenceContext);
    const nonceContext = react_1$3.default.useContext(nonce_1.NonceContext);
    return (0, react_1$3.useMemo)(() => ({
        compositionManagerCtx,
        timelineContext,
        setTimelineContext,
        sequenceContext,
        nonceContext,
    }), [
        compositionManagerCtx,
        nonceContext,
        sequenceContext,
        setTimelineContext,
        timelineContext,
    ]);
}
wrapRemotionContext.useRemotionContexts = useRemotionContexts;
const RemotionContextProvider = (props) => {
    const { children, contexts } = props;
    return ((0, jsx_runtime_1$5.jsx)(nonce_1.NonceContext.Provider, Object.assign({ value: contexts.nonceContext }, { children: (0, jsx_runtime_1$5.jsx)(CompositionManager_1$2.CompositionManager.Provider, Object.assign({ value: contexts.compositionManagerCtx }, { children: (0, jsx_runtime_1$5.jsx)(timeline_position_state_1.TimelineContext.Provider, Object.assign({ value: contexts.timelineContext }, { children: (0, jsx_runtime_1$5.jsx)(timeline_position_state_1.SetTimelineContext.Provider, Object.assign({ value: contexts.setTimelineContext }, { children: (0, jsx_runtime_1$5.jsx)(sequencing_1$3.SequenceContext.Provider, Object.assign({ value: contexts.sequenceContext }, { children: children }), void 0) }), void 0) }), void 0) }), void 0) }), void 0));
};
wrapRemotionContext.RemotionContextProvider = RemotionContextProvider;

var compressAssets = {};

Object.defineProperty(compressAssets, "__esModule", { value: true });
compressAssets.isAssetCompressed = compressAssets.compressAsset = void 0;
/**
 * Since audio or video can be base64-encoded, those can be really long strings.
 * Since we track the `src` property for every frame, Node.JS can run out of memory easily. Instead of duplicating the src for every frame, we save memory by replacing the full base 64 encoded data with a string `same-as-[asset-id]-[frame]` referencing a previous asset with the same src.
 */
const compressAsset = (previousAssets, newAsset) => {
    if (newAsset.src.length < 400) {
        return newAsset;
    }
    const assetWithSameSrc = previousAssets.find((a) => a.src === newAsset.src);
    if (!assetWithSameSrc) {
        return newAsset;
    }
    return {
        ...newAsset,
        src: `same-as-${assetWithSameSrc.id}-${assetWithSameSrc.frame}`,
    };
};
compressAssets.compressAsset = compressAsset;
const isAssetCompressed = (src) => {
    return src.startsWith('same-as');
};
compressAssets.isAssetCompressed = isAssetCompressed;

var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (commonjsGlobal && commonjsGlobal.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (commonjsGlobal && commonjsGlobal.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(internals, "__esModule", { value: true });
internals.Internals = void 0;
const shared_audio_tags_1 = sharedAudioTags;
const CompositionManager_1$1 = CompositionManager;
const browser_1 = browser;
const browser_executable_1 = browserExecutable;
const ffmpeg_executable_1 = ffmpegExecutable;
const codec_1 = codec;
const concurrency_1 = concurrency;
const crf_1 = crf;
const env_file_1 = envFile$1;
const frame_range_1 = frameRange;
const image_format_1 = imageFormat;
const image_sequence_1 = imageSequence$1;
const input_props_1 = inputProps;
const Logging = __importStar(log);
const max_timeline_tracks_1 = maxTimelineTracks$1;
const override_webpack_1 = overrideWebpack;
const overwrite_1 = overwrite;
const pixel_format_1 = pixelFormat;
const preview_server_1 = previewServer;
const prores_profile_1 = proresProfile;
const quality_1 = quality$1;
const still_frame_1 = stillFrame$1;
const webpack_caching_1 = webpackCaching;
const CSSUtils = __importStar(defaultCss);
const feature_flags_1 = featureFlags;
const get_environment_1 = getEnvironment;
const initial_frame_1 = initialFrame;
const is_audio_codec_1 = isAudioCodec$1;
const perf = __importStar(perf$2);
const register_root_1 = registerRoot;
const RemotionRoot_1 = RemotionRoot.exports;
const sequencing_1$2 = sequencing;
const setup_env_variables_1 = setupEnvVariables;
const TimelineInOutPosition = __importStar(timelineInoutPositionState);
const TimelinePosition = __importStar(timelinePositionState);
const truthy_1 = truthy$1;
const use_lazy_component_1 = useLazyComponent$1;
const use_unsafe_video_config_1$1 = useUnsafeVideoConfig$1;
const use_video_1 = useVideo$1;
const validate_composition_id_1 = validateCompositionId;
const validate_dimensions_1 = validateDimensions;
const validate_duration_in_frames_1$2 = validateDurationInFrames$1;
const validate_fps_1$1 = validateFps$1;
const validate_frame_1 = validateFrame$1;
const validate_image_format_1 = validateImageFormat;
const validate_quality_1 = validateQuality$1;
const volume_position_state_1$1 = volumePositionState;
const wrap_remotion_context_1 = wrapRemotionContext;
const AssetCompression = __importStar(compressAssets);
const Timeline = { ...TimelinePosition, ...TimelineInOutPosition };
// Mark them as Internals so use don't assume this is public
// API and are less likely to use it
internals.Internals = {
    perf,
    useUnsafeVideoConfig: use_unsafe_video_config_1$1.useUnsafeVideoConfig,
    Timeline,
    CompositionManager: CompositionManager_1$1.CompositionManager,
    RemotionRoot: RemotionRoot_1.RemotionRoot,
    useVideo: use_video_1.useVideo,
    getRoot: register_root_1.getRoot,
    getBrowserExecutable: browser_executable_1.getBrowserExecutable,
    getCustomFfmpegExecutable: ffmpeg_executable_1.getCustomFfmpegExecutable,
    getCompositionName: register_root_1.getCompositionName,
    getIsEvaluation: register_root_1.getIsEvaluation,
    getPixelFormat: pixel_format_1.getPixelFormat,
    getConcurrency: concurrency_1.getConcurrency,
    getRange: frame_range_1.getRange,
    getShouldOverwrite: overwrite_1.getShouldOverwrite,
    getOutputCodecOrUndefined: codec_1.getOutputCodecOrUndefined,
    getWebpackOverrideFn: override_webpack_1.getWebpackOverrideFn,
    getQuality: quality_1.getQuality,
    getShouldOutputImageSequence: image_sequence_1.getShouldOutputImageSequence,
    validateSelectedCrfAndCodecCombination: crf_1.validateSelectedCrfAndCodecCombination,
    getFinalOutputCodec: codec_1.getFinalOutputCodec,
    useMediaVolumeState: volume_position_state_1$1.useMediaVolumeState,
    useMediaMutedState: volume_position_state_1$1.useMediaMutedState,
    DEFAULT_CODEC: codec_1.DEFAULT_CODEC,
    DEFAULT_PIXEL_FORMAT: pixel_format_1.DEFAULT_PIXEL_FORMAT,
    FEATURE_FLAG_FIREFOX_SUPPORT: feature_flags_1.FEATURE_FLAG_FIREFOX_SUPPORT,
    DEFAULT_WEBPACK_CACHE_ENABLED: webpack_caching_1.DEFAULT_WEBPACK_CACHE_ENABLED,
    getBrowser: browser_1.getBrowser,
    DEFAULT_BROWSER: browser_1.DEFAULT_BROWSER,
    getDefaultCrfForCodec: crf_1.getDefaultCrfForCodec,
    getActualCrf: crf_1.getActualCrf,
    setFrameRangeFromCli: frame_range_1.setFrameRangeFromCli,
    getUserPreferredImageFormat: image_format_1.getUserPreferredImageFormat,
    validateSelectedPixelFormatAndImageFormatCombination: image_format_1.validateSelectedPixelFormatAndImageFormatCombination,
    validateSelectedPixelFormatAndCodecCombination: pixel_format_1.validateSelectedPixelFormatAndCodecCombination,
    validateFrameRange: frame_range_1.validateFrameRange,
    validateNonNullImageFormat: validate_image_format_1.validateNonNullImageFormat,
    getWebpackCaching: webpack_caching_1.getWebpackCaching,
    useLazyComponent: use_lazy_component_1.useLazyComponent,
    truthy: truthy_1.truthy,
    isAudioCodec: is_audio_codec_1.isAudioCodec,
    INPUT_PROPS_KEY: input_props_1.INPUT_PROPS_KEY,
    Logging,
    SequenceContext: sequencing_1$2.SequenceContext,
    useRemotionContexts: wrap_remotion_context_1.useRemotionContexts,
    RemotionContextProvider: wrap_remotion_context_1.RemotionContextProvider,
    isPlainIndex: register_root_1.isPlainIndex,
    CSSUtils,
    setupEnvVariables: setup_env_variables_1.setupEnvVariables,
    setupInitialFrame: initial_frame_1.setupInitialFrame,
    ENV_VARIABLES_ENV_NAME: setup_env_variables_1.ENV_VARIABLES_ENV_NAME,
    ENV_VARIABLES_LOCAL_STORAGE_KEY: setup_env_variables_1.ENV_VARIABLES_LOCAL_STORAGE_KEY,
    INITIAL_FRAME_LOCAL_STORAGE_KEY: initial_frame_1.INITIAL_FRAME_LOCAL_STORAGE_KEY,
    getDotEnvLocation: env_file_1.getDotEnvLocation,
    getServerPort: preview_server_1.getServerPort,
    MediaVolumeContext: volume_position_state_1$1.MediaVolumeContext,
    SetMediaVolumeContext: volume_position_state_1$1.SetMediaVolumeContext,
    validateDurationInFrames: validate_duration_in_frames_1$2.validateDurationInFrames,
    validateFps: validate_fps_1$1.validateFps,
    validateDimension: validate_dimensions_1.validateDimension,
    getRemotionEnvironment: get_environment_1.getRemotionEnvironment,
    getProResProfile: prores_profile_1.getProResProfile,
    setProResProfile: prores_profile_1.setProResProfile,
    validateSelectedCodecAndProResCombination: prores_profile_1.validateSelectedCodecAndProResCombination,
    getMaxTimelineTracks: max_timeline_tracks_1.getMaxTimelineTracks,
    SharedAudioContext: shared_audio_tags_1.SharedAudioContext,
    SharedAudioContextProvider: shared_audio_tags_1.SharedAudioContextProvider,
    validateQuality: validate_quality_1.validateQuality,
    validateFrame: validate_frame_1.validateFrame,
    setStillFrame: still_frame_1.setStillFrame,
    getStillFrame: still_frame_1.getStillFrame,
    invalidCompositionErrorMessage: validate_composition_id_1.invalidCompositionErrorMessage,
    isCompositionIdValid: validate_composition_id_1.isCompositionIdValid,
    AssetCompression,
};

var interpolateColors$1 = {};

/**
 * Copied from:
 * https://github.com/software-mansion/react-native-reanimated/blob/master/src/reanimated2/Colors.ts
 */
Object.defineProperty(interpolateColors$1, "__esModule", { value: true });
interpolateColors$1.interpolateColors = void 0;
/* eslint no-bitwise: 0 */
const interpolate_1 = interpolate$1;
// var INTEGER = '[-+]?\\d+';
const NUMBER = '[-+]?\\d*\\.?\\d+';
const PERCENTAGE = NUMBER + '%';
function call(...args) {
    return '\\(\\s*(' + args.join(')\\s*,\\s*(') + ')\\s*\\)';
}
function getMatchers() {
    const cachedMatchers = {
        rgb: undefined,
        rgba: undefined,
        hsl: undefined,
        hsla: undefined,
        hex3: undefined,
        hex4: undefined,
        hex5: undefined,
        hex6: undefined,
        hex8: undefined,
    };
    if (cachedMatchers.rgb === undefined) {
        cachedMatchers.rgb = new RegExp('rgb' + call(NUMBER, NUMBER, NUMBER));
        cachedMatchers.rgba = new RegExp('rgba' + call(NUMBER, NUMBER, NUMBER, NUMBER));
        cachedMatchers.hsl = new RegExp('hsl' + call(NUMBER, PERCENTAGE, PERCENTAGE));
        cachedMatchers.hsla = new RegExp('hsla' + call(NUMBER, PERCENTAGE, PERCENTAGE, NUMBER));
        cachedMatchers.hex3 = /^#([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/;
        cachedMatchers.hex4 = /^#([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/;
        cachedMatchers.hex6 = /^#([0-9a-fA-F]{6})$/;
        cachedMatchers.hex8 = /^#([0-9a-fA-F]{8})$/;
    }
    return cachedMatchers;
}
function hue2rgb(p, q, t) {
    if (t < 0) {
        t += 1;
    }
    if (t > 1) {
        t -= 1;
    }
    if (t < 1 / 6) {
        return p + (q - p) * 6 * t;
    }
    if (t < 1 / 2) {
        return q;
    }
    if (t < 2 / 3) {
        return p + (q - p) * (2 / 3 - t) * 6;
    }
    return p;
}
function hslToRgb(h, s, l) {
    const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    const p = 2 * l - q;
    const r = hue2rgb(p, q, h + 1 / 3);
    const g = hue2rgb(p, q, h);
    const b = hue2rgb(p, q, h - 1 / 3);
    return ((Math.round(r * 255) << 24) |
        (Math.round(g * 255) << 16) |
        (Math.round(b * 255) << 8));
}
function parse255(str) {
    const int = Number.parseInt(str, 10);
    if (int < 0) {
        return 0;
    }
    if (int > 255) {
        return 255;
    }
    return int;
}
function parse360(str) {
    const int = Number.parseFloat(str);
    return (((int % 360) + 360) % 360) / 360;
}
function parse1(str) {
    const num = Number.parseFloat(str);
    if (num < 0) {
        return 0;
    }
    if (num > 1) {
        return 255;
    }
    return Math.round(num * 255);
}
function parsePercentage(str) {
    // parseFloat conveniently ignores the final %
    const int = Number.parseFloat(str);
    if (int < 0) {
        return 0;
    }
    if (int > 100) {
        return 1;
    }
    return int / 100;
}
const names = {
    transparent: 0x00000000,
    // http://www.w3.org/TR/css3-color/#svg-color
    aliceblue: 0xf0f8ffff,
    antiquewhite: 0xfaebd7ff,
    aqua: 0x00ffffff,
    aquamarine: 0x7fffd4ff,
    azure: 0xf0ffffff,
    beige: 0xf5f5dcff,
    bisque: 0xffe4c4ff,
    black: 0x000000ff,
    blanchedalmond: 0xffebcdff,
    blue: 0x0000ffff,
    blueviolet: 0x8a2be2ff,
    brown: 0xa52a2aff,
    burlywood: 0xdeb887ff,
    burntsienna: 0xea7e5dff,
    cadetblue: 0x5f9ea0ff,
    chartreuse: 0x7fff00ff,
    chocolate: 0xd2691eff,
    coral: 0xff7f50ff,
    cornflowerblue: 0x6495edff,
    cornsilk: 0xfff8dcff,
    crimson: 0xdc143cff,
    cyan: 0x00ffffff,
    darkblue: 0x00008bff,
    darkcyan: 0x008b8bff,
    darkgoldenrod: 0xb8860bff,
    darkgray: 0xa9a9a9ff,
    darkgreen: 0x006400ff,
    darkgrey: 0xa9a9a9ff,
    darkkhaki: 0xbdb76bff,
    darkmagenta: 0x8b008bff,
    darkolivegreen: 0x556b2fff,
    darkorange: 0xff8c00ff,
    darkorchid: 0x9932ccff,
    darkred: 0x8b0000ff,
    darksalmon: 0xe9967aff,
    darkseagreen: 0x8fbc8fff,
    darkslateblue: 0x483d8bff,
    darkslategray: 0x2f4f4fff,
    darkslategrey: 0x2f4f4fff,
    darkturquoise: 0x00ced1ff,
    darkviolet: 0x9400d3ff,
    deeppink: 0xff1493ff,
    deepskyblue: 0x00bfffff,
    dimgray: 0x696969ff,
    dimgrey: 0x696969ff,
    dodgerblue: 0x1e90ffff,
    firebrick: 0xb22222ff,
    floralwhite: 0xfffaf0ff,
    forestgreen: 0x228b22ff,
    fuchsia: 0xff00ffff,
    gainsboro: 0xdcdcdcff,
    ghostwhite: 0xf8f8ffff,
    gold: 0xffd700ff,
    goldenrod: 0xdaa520ff,
    gray: 0x808080ff,
    green: 0x008000ff,
    greenyellow: 0xadff2fff,
    grey: 0x808080ff,
    honeydew: 0xf0fff0ff,
    hotpink: 0xff69b4ff,
    indianred: 0xcd5c5cff,
    indigo: 0x4b0082ff,
    ivory: 0xfffff0ff,
    khaki: 0xf0e68cff,
    lavender: 0xe6e6faff,
    lavenderblush: 0xfff0f5ff,
    lawngreen: 0x7cfc00ff,
    lemonchiffon: 0xfffacdff,
    lightblue: 0xadd8e6ff,
    lightcoral: 0xf08080ff,
    lightcyan: 0xe0ffffff,
    lightgoldenrodyellow: 0xfafad2ff,
    lightgray: 0xd3d3d3ff,
    lightgreen: 0x90ee90ff,
    lightgrey: 0xd3d3d3ff,
    lightpink: 0xffb6c1ff,
    lightsalmon: 0xffa07aff,
    lightseagreen: 0x20b2aaff,
    lightskyblue: 0x87cefaff,
    lightslategray: 0x778899ff,
    lightslategrey: 0x778899ff,
    lightsteelblue: 0xb0c4deff,
    lightyellow: 0xffffe0ff,
    lime: 0x00ff00ff,
    limegreen: 0x32cd32ff,
    linen: 0xfaf0e6ff,
    magenta: 0xff00ffff,
    maroon: 0x800000ff,
    mediumaquamarine: 0x66cdaaff,
    mediumblue: 0x0000cdff,
    mediumorchid: 0xba55d3ff,
    mediumpurple: 0x9370dbff,
    mediumseagreen: 0x3cb371ff,
    mediumslateblue: 0x7b68eeff,
    mediumspringgreen: 0x00fa9aff,
    mediumturquoise: 0x48d1ccff,
    mediumvioletred: 0xc71585ff,
    midnightblue: 0x191970ff,
    mintcream: 0xf5fffaff,
    mistyrose: 0xffe4e1ff,
    moccasin: 0xffe4b5ff,
    navajowhite: 0xffdeadff,
    navy: 0x000080ff,
    oldlace: 0xfdf5e6ff,
    olive: 0x808000ff,
    olivedrab: 0x6b8e23ff,
    orange: 0xffa500ff,
    orangered: 0xff4500ff,
    orchid: 0xda70d6ff,
    palegoldenrod: 0xeee8aaff,
    palegreen: 0x98fb98ff,
    paleturquoise: 0xafeeeeff,
    palevioletred: 0xdb7093ff,
    papayawhip: 0xffefd5ff,
    peachpuff: 0xffdab9ff,
    peru: 0xcd853fff,
    pink: 0xffc0cbff,
    plum: 0xdda0ddff,
    powderblue: 0xb0e0e6ff,
    purple: 0x800080ff,
    rebeccapurple: 0x663399ff,
    red: 0xff0000ff,
    rosybrown: 0xbc8f8fff,
    royalblue: 0x4169e1ff,
    saddlebrown: 0x8b4513ff,
    salmon: 0xfa8072ff,
    sandybrown: 0xf4a460ff,
    seagreen: 0x2e8b57ff,
    seashell: 0xfff5eeff,
    sienna: 0xa0522dff,
    silver: 0xc0c0c0ff,
    skyblue: 0x87ceebff,
    slateblue: 0x6a5acdff,
    slategray: 0x708090ff,
    slategrey: 0x708090ff,
    snow: 0xfffafaff,
    springgreen: 0x00ff7fff,
    steelblue: 0x4682b4ff,
    tan: 0xd2b48cff,
    teal: 0x008080ff,
    thistle: 0xd8bfd8ff,
    tomato: 0xff6347ff,
    turquoise: 0x40e0d0ff,
    violet: 0xee82eeff,
    wheat: 0xf5deb3ff,
    white: 0xffffffff,
    whitesmoke: 0xf5f5f5ff,
    yellow: 0xffff00ff,
    yellowgreen: 0x9acd32ff,
};
function normalizeColor(color) {
    const matchers = getMatchers();
    let match;
    // Ordered based on occurrences on Facebook codebase
    if (matchers.hex6) {
        if ((match = matchers.hex6.exec(color))) {
            return Number.parseInt(match[1] + 'ff', 16) >>> 0;
        }
    }
    if (names[color] !== undefined) {
        return names[color];
    }
    if (matchers.rgb) {
        if ((match = matchers.rgb.exec(color))) {
            return (
            // b
            ((parse255(match[1]) << 24) | // r
                (parse255(match[2]) << 16) | // g
                (parse255(match[3]) << 8) |
                0x000000ff) >>> // a
                0);
        }
    }
    if (matchers.rgba) {
        if ((match = matchers.rgba.exec(color))) {
            return (
            // b
            ((parse255(match[1]) << 24) | // r
                (parse255(match[2]) << 16) | // g
                (parse255(match[3]) << 8) |
                parse1(match[4])) >>> // a
                0);
        }
    }
    if (matchers.hex3) {
        if ((match = matchers.hex3.exec(color))) {
            return (Number.parseInt(match[1] +
                match[1] + // r
                match[2] +
                match[2] + // g
                match[3] +
                match[3] + // b
                'ff', // a
            16) >>> 0);
        }
    }
    // https://drafts.csswg.org/css-color-4/#hex-notation
    if (matchers.hex8) {
        if ((match = matchers.hex8.exec(color))) {
            return Number.parseInt(match[1], 16) >>> 0;
        }
    }
    if (matchers.hex4) {
        if ((match = matchers.hex4.exec(color))) {
            return (Number.parseInt(match[1] +
                match[1] + // r
                match[2] +
                match[2] + // g
                match[3] +
                match[3] + // b
                match[4] +
                match[4], // a
            16) >>> 0);
        }
    }
    if (matchers.hsl) {
        if ((match = matchers.hsl.exec(color))) {
            return ((hslToRgb(parse360(match[1]), // h
            parsePercentage(match[2]), // s
            parsePercentage(match[3]) // l
            ) |
                0x000000ff) >>> // a
                0);
        }
    }
    if (matchers.hsla) {
        if ((match = matchers.hsla.exec(color))) {
            return ((hslToRgb(parse360(match[1]), // h
            parsePercentage(match[2]), // s
            parsePercentage(match[3]) // l
            ) |
                parse1(match[4])) >>> // a
                0);
        }
    }
    throw new Error(`invalid color string ${color} provided`);
}
const opacity = (c) => {
    return ((c >> 24) & 255) / 255;
};
const red = (c) => {
    return (c >> 16) & 255;
};
const green = (c) => {
    return (c >> 8) & 255;
};
const blue = (c) => {
    return c & 255;
};
const rgbaColor = (r, g, b, alpha) => {
    return `rgba(${r}, ${g}, ${b}, ${alpha})`;
};
function processColorInitially(color) {
    let normalizedColor = normalizeColor(color);
    normalizedColor = ((normalizedColor << 24) | (normalizedColor >>> 8)) >>> 0; // argb
    return normalizedColor;
}
function processColor(color) {
    const normalizedColor = processColorInitially(color);
    return normalizedColor;
}
const interpolateColorsRGB = (value, inputRange, colors) => {
    const [r, g, b, a] = [red, green, blue, opacity].map((f) => {
        const unrounded = (0, interpolate_1.interpolate)(value, inputRange, colors.map((c) => f(c)), {
            extrapolateLeft: 'clamp',
            extrapolateRight: 'clamp',
        });
        if (f === opacity) {
            return Number(unrounded.toFixed(3));
        }
        return Math.round(unrounded);
    });
    return rgbaColor(r, g, b, a);
};
const interpolateColors = (input, inputRange, outputRange) => {
    if (typeof input === 'undefined') {
        throw new TypeError('input can not be undefined');
    }
    if (typeof inputRange === 'undefined') {
        throw new TypeError('inputRange can not be undefined');
    }
    if (typeof outputRange === 'undefined') {
        throw new TypeError('outputRange can not be undefined');
    }
    if (inputRange.length !== outputRange.length) {
        throw new TypeError('inputRange (' +
            inputRange.length +
            ' values provided) and outputRange (' +
            outputRange.length +
            ' values provided) must have the same length');
    }
    const processedOutputRange = outputRange.map((c) => processColor(c));
    return interpolateColorsRGB(input, inputRange, processedOutputRange);
};
interpolateColors$1.interpolateColors = interpolateColors;

var loop = {};

Object.defineProperty(loop, "__esModule", { value: true });
loop.Loop = void 0;
const jsx_runtime_1$4 = jsxRuntime.exports;
const __1 = dist;
const validate_duration_in_frames_1$1 = validateDurationInFrames$1;
const Loop = ({ durationInFrames, times = Infinity, children, layout, name, }) => {
    const { durationInFrames: compDuration } = (0, __1.useVideoConfig)();
    (0, validate_duration_in_frames_1$1.validateDurationInFrames)(durationInFrames, 'of the <Loop /> component');
    if (typeof times !== 'number') {
        throw new TypeError(`You passed to "times" an argument of type ${typeof times}, but it must be a number.`);
    }
    if (times !== Infinity && times % 1 !== 0) {
        throw new TypeError(`The "times" prop of a loop must be an integer, but got ${times}.`);
    }
    if (times < 0) {
        throw new TypeError(`The "times" prop of a loop must be at least 0, but got ${times}`);
    }
    const maxTimes = Math.ceil(compDuration / durationInFrames);
    const actualTimes = Math.min(maxTimes, times);
    return ((0, jsx_runtime_1$4.jsx)(jsx_runtime_1$4.Fragment, { children: new Array(actualTimes).fill(true).map((_, i) => {
            return ((0, jsx_runtime_1$4.jsx)(__1.Sequence
            // eslint-disable-next-line react/no-array-index-key
            , Object.assign({ durationInFrames: durationInFrames, from: i * durationInFrames, layout: layout, name: name, showLoopTimesInTimeline: actualTimes, showInTimeline: i === 0 }, { children: children }), `loop-${i}`));
        }) }, void 0));
};
loop.Loop = Loop;

var series = {};

var flattenChildren = {};

(function (exports) {
var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.flattenChildren = void 0;
const react_1 = __importDefault(react.exports);
const flattenChildren = (children) => {
    const childrenArray = react_1.default.Children.toArray(children);
    return childrenArray.reduce((flatChildren, child) => {
        if (child.type === react_1.default.Fragment) {
            return flatChildren.concat((0, exports.flattenChildren)(child.props
                .children));
        }
        flatChildren.push(child);
        return flatChildren;
    }, []);
};
exports.flattenChildren = flattenChildren;

}(flattenChildren));

Object.defineProperty(series, "__esModule", { value: true });
series.Series = void 0;
const jsx_runtime_1$3 = jsxRuntime.exports;
const react_1$2 = react.exports;
const sequencing_1$1 = sequencing;
const validate_duration_in_frames_1 = validateDurationInFrames$1;
const flatten_children_1 = flattenChildren;
const SeriesSequence = ({ children }) => {
    // eslint-disable-next-line react/jsx-no-useless-fragment
    return (0, jsx_runtime_1$3.jsx)(jsx_runtime_1$3.Fragment, { children: children }, void 0);
};
const Series = ({ children }) => {
    const childrenValue = (0, react_1$2.useMemo)(() => {
        let startFrame = 0;
        return react_1$2.Children.map((0, flatten_children_1.flattenChildren)(children), (child, i) => {
            var _a;
            const castedChild = child;
            if (typeof castedChild === 'string') {
                // Don't throw if it's just some accidential whitespace
                if (castedChild.trim() === '') {
                    return null;
                }
                throw new TypeError(`The <Series /> component only accepts a list of <Series.Sequence /> components as it's children, but you passed a string "${castedChild}"`);
            }
            if (castedChild.type !== SeriesSequence) {
                throw new TypeError(`The <Series /> component only accepts a list of <Series.Sequence /> components as it's children, but got ${castedChild} instead`);
            }
            const debugInfo = `index = ${i}, duration = ${castedChild.props.durationInFrames}`;
            if (!castedChild || !castedChild.props.children) {
                throw new TypeError(`A <Series.Sequence /> component (${debugInfo}) was detected to not have any children. Delete it to fix this error.`);
            }
            const durationInFramesProp = castedChild.props.durationInFrames;
            const { durationInFrames, children: _children, ...passedProps } = castedChild.props;
            (0, validate_duration_in_frames_1.validateDurationInFrames)(durationInFramesProp, `of a <Series.Sequence /> component`);
            const offset = (_a = castedChild.props.offset) !== null && _a !== void 0 ? _a : 0;
            if (Number.isNaN(offset)) {
                throw new TypeError(`The "offset" property of a <Series.Sequence /> must not be NaN, but got NaN (${debugInfo}).`);
            }
            if (!Number.isFinite(offset)) {
                throw new TypeError(`The "offset" property of a <Series.Sequence /> must be finite, but got ${offset} (${debugInfo}).`);
            }
            if (offset % 1 !== 0) {
                throw new TypeError(`The "offset" property of a <Series.Sequence /> must be finite, but got ${offset} (${debugInfo}).`);
            }
            const currentStartFrame = startFrame + offset;
            startFrame += durationInFramesProp + offset;
            return ((0, jsx_runtime_1$3.jsx)(sequencing_1$1.Sequence, Object.assign({ from: currentStartFrame, durationInFrames: durationInFramesProp }, passedProps, { children: child }), void 0));
        });
    }, [children]);
    /* eslint-disable react/jsx-no-useless-fragment */
    return (0, jsx_runtime_1$3.jsx)(jsx_runtime_1$3.Fragment, { children: childrenValue }, void 0);
};
series.Series = Series;
Series.Sequence = SeriesSequence;

var spring = {};

var springUtils = {};

Object.defineProperty(springUtils, "__esModule", { value: true });
springUtils.springCalculation = void 0;
const defaultSpringConfig = {
    damping: 10,
    mass: 1,
    stiffness: 100,
    overshootClamping: false,
};
const advanceCache = {};
function advance({ animation, now, config, }) {
    const { toValue, lastTimestamp, current, velocity } = animation;
    const deltaTime = Math.min(now - lastTimestamp, 64);
    const c = config.damping;
    const m = config.mass;
    const k = config.stiffness;
    const cacheKey = [
        toValue,
        lastTimestamp,
        current,
        velocity,
        c,
        m,
        k,
        now,
    ].join('-');
    if (advanceCache[cacheKey]) {
        return advanceCache[cacheKey];
    }
    const v0 = -velocity;
    const x0 = toValue - current;
    const zeta = c / (2 * Math.sqrt(k * m)); // damping ratio
    const omega0 = Math.sqrt(k / m); // undamped angular frequency of the oscillator (rad/ms)
    const omega1 = omega0 * Math.sqrt(1 - zeta ** 2); // exponential decay
    const t = deltaTime / 1000;
    const sin1 = Math.sin(omega1 * t);
    const cos1 = Math.cos(omega1 * t);
    // under damped
    const underDampedEnvelope = Math.exp(-zeta * omega0 * t);
    const underDampedFrag1 = underDampedEnvelope *
        (sin1 * ((v0 + zeta * omega0 * x0) / omega1) + x0 * cos1);
    const underDampedPosition = toValue - underDampedFrag1;
    // This looks crazy -- it's actually just the derivative of the oscillation function
    const underDampedVelocity = zeta * omega0 * underDampedFrag1 -
        underDampedEnvelope *
            (cos1 * (v0 + zeta * omega0 * x0) - omega1 * x0 * sin1);
    // critically damped
    const criticallyDampedEnvelope = Math.exp(-omega0 * t);
    const criticallyDampedPosition = toValue - criticallyDampedEnvelope * (x0 + (v0 + omega0 * x0) * t);
    const criticallyDampedVelocity = criticallyDampedEnvelope *
        (v0 * (t * omega0 - 1) + t * x0 * omega0 * omega0);
    const animationNode = {
        toValue,
        prevPosition: current,
        lastTimestamp: now,
        current: zeta < 1 ? underDampedPosition : criticallyDampedPosition,
        velocity: zeta < 1 ? underDampedVelocity : criticallyDampedVelocity,
    };
    advanceCache[cacheKey] = animationNode;
    return animationNode;
}
const calculationCache = {};
function springCalculation({ from = 0, to = 1, frame, fps, config = {}, }) {
    const cacheKey = [
        from,
        to,
        frame,
        fps,
        config.damping,
        config.mass,
        config.overshootClamping,
        config.stiffness,
    ].join('-');
    if (calculationCache[cacheKey]) {
        return calculationCache[cacheKey];
    }
    let animation = {
        lastTimestamp: 0,
        current: from,
        toValue: to,
        velocity: 0,
        prevPosition: 0,
    };
    const frameClamped = Math.max(0, frame);
    const unevenRest = frameClamped % 1;
    for (let f = 0; f <= Math.floor(frameClamped); f++) {
        if (f === Math.floor(frameClamped)) {
            f += unevenRest;
        }
        const time = (f / fps) * 1000;
        animation = advance({
            animation,
            now: time,
            config: {
                ...defaultSpringConfig,
                ...config,
            },
        });
    }
    calculationCache[cacheKey] = animation;
    return animation;
}
springUtils.springCalculation = springCalculation;

var measureSpring$1 = {};

Object.defineProperty(measureSpring$1, "__esModule", { value: true });
measureSpring$1.measureSpring = void 0;
const validate_fps_1 = validateFps$1;
const spring_utils_1 = springUtils;
function measureSpring({ fps, config = {}, threshold = 0.005, from = 0, to = 1, }) {
    if (typeof threshold !== 'number') {
        throw new TypeError(`threshold must be a number, got ${threshold} of type ${typeof threshold}`);
    }
    if (threshold === 0) {
        return Infinity;
    }
    if (threshold === 1) {
        return 0;
    }
    if (isNaN(threshold)) {
        throw new TypeError('Threshold is NaN');
    }
    if (!Number.isFinite(threshold)) {
        throw new TypeError('Threshold is not finite');
    }
    if (threshold < 0) {
        throw new TypeError('Threshold is below 0');
    }
    (0, validate_fps_1.validateFps)(fps, 'to the measureSpring() function');
    const range = Math.abs(from - to);
    let frame = 0;
    let finishedFrame = 0;
    const calc = () => {
        return (0, spring_utils_1.springCalculation)({
            fps,
            frame,
            config,
            from,
            to,
        });
    };
    let animation = calc();
    const calcDifference = () => {
        return (Math.abs(animation.current - animation.toValue) /
            (range === 0 ? 1 : range));
    };
    let difference = calcDifference();
    while (difference >= threshold) {
        frame++;
        animation = calc();
        difference = calcDifference();
    }
    // Since spring is bouncy, just because it's under the threshold we
    // cannot be sure it's done. We need to animate further until it stays in the
    // threshold for, say, 20 frames.
    finishedFrame = frame;
    for (let i = 0; i < 20; i++) {
        frame++;
        animation = calc();
        difference = calcDifference();
        if (difference >= threshold) {
            i = 0;
            finishedFrame = frame + 1;
        }
    }
    return finishedFrame;
}
measureSpring$1.measureSpring = measureSpring;

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.measureSpring = exports.spring = void 0;
const spring_utils_1 = springUtils;
function spring({ frame, fps, config = {}, from = 0, to = 1, }) {
    const spr = (0, spring_utils_1.springCalculation)({
        fps,
        frame,
        config,
        from,
        to,
    });
    if (!config.overshootClamping) {
        return spr.current;
    }
    if (to >= from) {
        return Math.min(spr.current, to);
    }
    return Math.max(spr.current, to);
}
exports.spring = spring;
var measure_spring_1 = measureSpring$1;
Object.defineProperty(exports, "measureSpring", { enumerable: true, get: function () { return measure_spring_1.measureSpring; } });

}(spring));

var Still$1 = {};

Object.defineProperty(Still$1, "__esModule", { value: true });
Still$1.Still = void 0;
const jsx_runtime_1$2 = jsxRuntime.exports;
const Composition_1 = Composition$1;
const Still = (props) => {
    return (0, jsx_runtime_1$2.jsx)(Composition_1.Composition, Object.assign({ fps: 1, durationInFrames: 1 }, props), void 0);
};
Still$1.Still = Still;

var video = {};

var props = {};

Object.defineProperty(props, "__esModule", { value: true });

var Video = {};

var VideoForDevelopment = {};

Object.defineProperty(VideoForDevelopment, "__esModule", { value: true });
VideoForDevelopment.VideoForDevelopment = void 0;
const jsx_runtime_1$1 = jsxRuntime.exports;
const react_1$1 = react.exports;
const use_audio_frame_1$1 = useAudioFrame;
const use_media_in_timeline_1 = useMediaInTimeline$1;
const use_media_playback_1 = useMediaPlayback$1;
const use_media_tag_volume_1 = useMediaTagVolume$1;
const use_sync_volume_with_media_tag_1 = useSyncVolumeWithMediaTag$1;
const volume_position_state_1 = volumePositionState;
const VideoForDevelopmentRefForwardingFunction = (props, ref) => {
    var _a;
    const videoRef = (0, react_1$1.useRef)(null);
    const volumePropFrame = (0, use_audio_frame_1$1.useFrameForVolumeProp)();
    const { volume, muted, playbackRate, ...nativeProps } = props;
    const actualVolume = (0, use_media_tag_volume_1.useMediaTagVolume)(videoRef);
    const [mediaVolume] = (0, volume_position_state_1.useMediaVolumeState)();
    const [mediaMuted] = (0, volume_position_state_1.useMediaMutedState)();
    (0, use_media_in_timeline_1.useMediaInTimeline)({
        mediaRef: videoRef,
        volume,
        mediaVolume,
        mediaType: 'video',
        src: nativeProps.src,
    });
    (0, use_sync_volume_with_media_tag_1.useSyncVolumeWithMediaTag)({
        volumePropFrame,
        actualVolume,
        volume,
        mediaVolume,
        mediaRef: videoRef,
    });
    (0, use_media_playback_1.useMediaPlayback)({
        mediaRef: videoRef,
        src: nativeProps.src,
        mediaType: 'video',
        playbackRate: (_a = props.playbackRate) !== null && _a !== void 0 ? _a : 1,
    });
    (0, react_1$1.useImperativeHandle)(ref, () => {
        return videoRef.current;
    });
    return ((0, jsx_runtime_1$1.jsx)("video", Object.assign({ ref: videoRef, muted: muted || mediaMuted, playsInline: true }, nativeProps), void 0));
};
VideoForDevelopment.VideoForDevelopment = (0, react_1$1.forwardRef)(VideoForDevelopmentRefForwardingFunction);

var VideoForRendering = {};

Object.defineProperty(VideoForRendering, "__esModule", { value: true });
VideoForRendering.VideoForRendering = void 0;
const jsx_runtime_1 = jsxRuntime.exports;
const react_1 = react.exports;
const absolute_src_1 = absoluteSrc;
const use_audio_frame_1 = useAudioFrame;
const CompositionManager_1 = CompositionManager;
const is_approximately_the_same_1 = isApproximatelyTheSame$1;
const is_remote_asset_1 = isRemoteAsset$1;
const random_1 = random$1;
const ready_manager_1 = readyManager;
const sequencing_1 = sequencing;
const use_frame_1 = useFrame;
const use_unsafe_video_config_1 = useUnsafeVideoConfig$1;
const volume_prop_1 = volumeProp;
const get_current_time_1 = getCurrentTime;
const VideoForRenderingForwardFunction = ({ onError, volume: volumeProp, playbackRate, ...props }, ref) => {
    const absoluteFrame = (0, use_frame_1.useAbsoluteCurrentFrame)();
    const frame = (0, use_frame_1.useCurrentFrame)();
    const volumePropsFrame = (0, use_audio_frame_1.useFrameForVolumeProp)();
    const videoConfig = (0, use_unsafe_video_config_1.useUnsafeVideoConfig)();
    const videoRef = (0, react_1.useRef)(null);
    const sequenceContext = (0, react_1.useContext)(sequencing_1.SequenceContext);
    const mediaStartsAt = (0, use_audio_frame_1.useMediaStartsAt)();
    const { registerAsset, unregisterAsset } = (0, react_1.useContext)(CompositionManager_1.CompositionManager);
    // Generate a string that's as unique as possible for this asset
    // but at the same time the same on all threads
    const id = (0, react_1.useMemo)(() => {
        var _a;
        return `video-${(0, random_1.random)((_a = props.src) !== null && _a !== void 0 ? _a : '')}-${sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.cumulatedFrom}-${sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.relativeFrom}-${sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.durationInFrames}-muted:${props.muted}`;
    }, [
        props.src,
        props.muted,
        sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.cumulatedFrom,
        sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.relativeFrom,
        sequenceContext === null || sequenceContext === void 0 ? void 0 : sequenceContext.durationInFrames,
    ]);
    if (!videoConfig) {
        throw new Error('No video config found');
    }
    const volume = (0, volume_prop_1.evaluateVolume)({
        volume: volumeProp,
        frame: volumePropsFrame,
        mediaVolume: 1,
    });
    (0, react_1.useEffect)(() => {
        if (!props.src) {
            throw new Error('No src passed');
        }
        if (props.muted) {
            return;
        }
        registerAsset({
            type: 'video',
            src: (0, absolute_src_1.getAbsoluteSrc)(props.src),
            id,
            frame: absoluteFrame,
            volume,
            isRemote: (0, is_remote_asset_1.isRemoteAsset)((0, absolute_src_1.getAbsoluteSrc)(props.src), false),
            mediaFrame: frame,
            playbackRate: playbackRate !== null && playbackRate !== void 0 ? playbackRate : 1,
        });
        return () => unregisterAsset(id);
    }, [
        props.muted,
        props.src,
        registerAsset,
        id,
        unregisterAsset,
        volume,
        frame,
        absoluteFrame,
        playbackRate,
    ]);
    (0, react_1.useImperativeHandle)(ref, () => {
        return videoRef.current;
    });
    (0, react_1.useEffect)(() => {
        if (!videoRef.current) {
            return;
        }
        const currentTime = (() => {
            return (0, get_current_time_1.getMediaTime)({
                fps: videoConfig.fps,
                frame,
                src: props.src,
                playbackRate: playbackRate || 1,
                startFrom: -mediaStartsAt,
            });
        })();
        const handle = (0, ready_manager_1.delayRender)();
        if (process.env.NODE_ENV === 'test') {
            (0, ready_manager_1.continueRender)(handle);
            return;
        }
        if ((0, is_approximately_the_same_1.isApproximatelyTheSame)(videoRef.current.currentTime, currentTime)) {
            if (videoRef.current.readyState >= 2) {
                (0, ready_manager_1.continueRender)(handle);
                return;
            }
            videoRef.current.addEventListener('loadeddata', () => {
                (0, ready_manager_1.continueRender)(handle);
            }, { once: true });
            return;
        }
        videoRef.current.currentTime = currentTime;
        videoRef.current.addEventListener('seeked', () => {
            if (window.navigator.platform.startsWith('Mac')) {
                // Improve me: This is ensures frame perfectness but slows down render.
                // Please see this issue for context: https://github.com/remotion-dev/remotion/issues/200
                // Only affects macOS since it uses VideoToolbox decoding.
                setTimeout(() => {
                    (0, ready_manager_1.continueRender)(handle);
                }, 100);
            }
            else {
                (0, ready_manager_1.continueRender)(handle);
            }
        }, { once: true });
        videoRef.current.addEventListener('ended', () => {
            (0, ready_manager_1.continueRender)(handle);
        }, { once: true });
        videoRef.current.addEventListener('error', (err) => {
            console.error('Error occurred in video', err);
            (0, ready_manager_1.continueRender)(handle);
        }, { once: true });
    }, [
        volumePropsFrame,
        props.src,
        playbackRate,
        videoConfig.fps,
        frame,
        mediaStartsAt,
    ]);
    return (0, jsx_runtime_1.jsx)("video", Object.assign({ ref: videoRef }, props, { onError: onError }), void 0);
};
VideoForRendering.VideoForRendering = (0, react_1.forwardRef)(VideoForRenderingForwardFunction);

(function (exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.Video = void 0;
const jsx_runtime_1 = jsxRuntime.exports;
const react_1 = react.exports;
const get_environment_1 = getEnvironment;
const sequencing_1 = sequencing;
const validate_media_props_1 = validateMediaProps$1;
const validate_start_from_props_1 = validateStartFromProps$1;
const VideoForDevelopment_1 = VideoForDevelopment;
const VideoForRendering_1 = VideoForRendering;
const VideoForwardingFunction = (props, ref) => {
    const { startFrom, endAt, ...otherProps } = props;
    if (typeof ref === 'string') {
        throw new Error('string refs are not supported');
    }
    if (typeof startFrom !== 'undefined' || typeof endAt !== 'undefined') {
        (0, validate_start_from_props_1.validateStartFromProps)(startFrom, endAt);
        const startFromFrameNo = startFrom !== null && startFrom !== void 0 ? startFrom : 0;
        const endAtFrameNo = endAt !== null && endAt !== void 0 ? endAt : Infinity;
        return ((0, jsx_runtime_1.jsx)(sequencing_1.Sequence, Object.assign({ layout: "none", from: 0 - startFromFrameNo, showInTimeline: false, durationInFrames: endAtFrameNo }, { children: (0, jsx_runtime_1.jsx)(exports.Video, Object.assign({}, otherProps, { ref: ref }), void 0) }), void 0));
    }
    (0, validate_media_props_1.validateMediaProps)(props, 'Video');
    if ((0, get_environment_1.getRemotionEnvironment)() === 'rendering') {
        return (0, jsx_runtime_1.jsx)(VideoForRendering_1.VideoForRendering, Object.assign({}, otherProps, { ref: ref }), void 0);
    }
    return (0, jsx_runtime_1.jsx)(VideoForDevelopment_1.VideoForDevelopment, Object.assign({}, otherProps, { ref: ref }), void 0);
};
exports.Video = (0, react_1.forwardRef)(VideoForwardingFunction);

}(Video));

(function (exports) {
var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (commonjsGlobal && commonjsGlobal.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
__exportStar(props, exports);
__exportStar(Video, exports);

}(video));

var videoConfig = {};

Object.defineProperty(videoConfig, "__esModule", { value: true });

var staticFile$1 = {};

Object.defineProperty(staticFile$1, "__esModule", { value: true });
staticFile$1.staticFile = void 0;
const trimLeadingSlash = (path) => {
    if (path.startsWith('/')) {
        return path.substr(1);
    }
    return path;
};
const inner = (path) => {
    if (window.remotion_staticBase) {
        return `${window.remotion_staticBase}/${trimLeadingSlash(path)}`;
    }
    return `/${trimLeadingSlash(path)}`;
};
const staticFile = (path) => {
    const preparsed = inner(path);
    if (!preparsed.startsWith('/')) {
        return `/${preparsed}`;
    }
    return preparsed;
};
staticFile$1.staticFile = staticFile;

(function (exports) {
var __createBinding = (commonjsGlobal && commonjsGlobal.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (commonjsGlobal && commonjsGlobal.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.staticFile = exports.Series = exports.Sequence = exports.registerRoot = exports.Loop = exports.interpolateColors = exports.getInputProps = void 0;

const multiple_versions_warning_1 = multipleVersionsWarning;
(0, multiple_versions_warning_1.checkMultipleRemotionVersions)();
__exportStar(AbsoluteFill$1, exports);
__exportStar(audio, exports);
__exportStar(Composition$1, exports);
__exportStar(config, exports);
var input_props_1 = inputProps;
Object.defineProperty(exports, "getInputProps", { enumerable: true, get: function () { return input_props_1.getInputProps; } });
__exportStar(easing, exports);
__exportStar(freeze, exports);
__exportStar(IFrame, exports);
__exportStar(Img, exports);
__exportStar(internals, exports);
__exportStar(interpolate$1, exports);
var interpolateColors_1 = interpolateColors$1;
Object.defineProperty(exports, "interpolateColors", { enumerable: true, get: function () { return interpolateColors_1.interpolateColors; } });
var loop_1 = loop;
Object.defineProperty(exports, "Loop", { enumerable: true, get: function () { return loop_1.Loop; } });
__exportStar(random$1, exports);
__exportStar(readyManager, exports);
var register_root_1 = registerRoot;
Object.defineProperty(exports, "registerRoot", { enumerable: true, get: function () { return register_root_1.registerRoot; } });
var sequencing_1 = sequencing;
Object.defineProperty(exports, "Sequence", { enumerable: true, get: function () { return sequencing_1.Sequence; } });
var series_1 = series;
Object.defineProperty(exports, "Series", { enumerable: true, get: function () { return series_1.Series; } });
__exportStar(spring, exports);
__exportStar(Still$1, exports);
__exportStar(useFrame, exports);
__exportStar(useVideoConfig$1, exports);
__exportStar(video, exports);
__exportStar(videoConfig, exports);
var static_file_1 = staticFile$1;
Object.defineProperty(exports, "staticFile", { enumerable: true, get: function () { return static_file_1.staticFile; } });

}(dist));

var COLOR_1 = '#86A8E7';
var COLOR_2 = '#91EAE4';

var Arc = function (_a) {
    var progress = _a.progress, rotation = _a.rotation, rotateProgress = _a.rotateProgress;
    var config = dist.useVideoConfig();
    var cx = config.width / 2;
    var cy = config.height / 2;
    var rx = config.height / 8;
    var ry = rx * 2.2;
    var arcLength = Math.PI * 2 * Math.sqrt((rx * rx + ry * ry) / 2);
    var strokeWidth = arcLength / 60;
    return (React.createElement("svg", { viewBox: "0 0 ".concat(config.width, " ").concat(config.height), style: {
            position: "absolute",
            transform: "rotate(".concat(rotation * rotateProgress, "deg)"),
        } },
        React.createElement("defs", null,
            React.createElement("linearGradient", { id: "gradient", x1: "0%", y1: "0%", x2: "0%", y2: "100%" },
                React.createElement("stop", { offset: "0%", stopColor: COLOR_1 }),
                React.createElement("stop", { offset: "100%", stopColor: COLOR_2 }))),
        React.createElement("ellipse", { cx: cx, cy: cy, rx: rx, ry: ry, fill: "none", stroke: "url(#gradient)", strokeDasharray: arcLength, strokeDashoffset: arcLength - arcLength * progress, strokeLinecap: "round", strokeWidth: strokeWidth })));
};

var Logo = function (_a) {
    var transitionStart = _a.transitionStart;
    var videoConfig = dist.useVideoConfig();
    var frame = dist.useCurrentFrame();
    var development = dist.spring({
        config: {
            damping: 100,
            mass: 0.5,
        },
        fps: videoConfig.fps,
        frame: frame,
    });
    var rotationDevelopment = dist.spring({
        config: {
            damping: 100,
            mass: 0.5,
        },
        fps: videoConfig.fps,
        frame: frame,
    });
    var scaleIn = dist.spring({
        frame: frame,
        config: {
            mass: 0.5,
        },
        fps: videoConfig.fps,
    });
    var translation = dist.interpolate(dist.spring({
        frame: frame - transitionStart,
        fps: videoConfig.fps,
        config: {
            damping: 100,
            mass: 0.5,
        },
    }), [0, 1], [0, -150]);
    var scale = frame < 50 ? scaleIn : 1;
    var logoRotation = dist.interpolate(frame, [0, videoConfig.durationInFrames], [0, 360]);
    return (React.createElement("div", { style: {
            position: "absolute",
            width: videoConfig.width,
            height: videoConfig.height,
            transform: "scale(".concat(scale, ") translateY(").concat(translation, "px) rotate(").concat(logoRotation, "deg)"),
        } },
        React.createElement(Arc, { rotateProgress: rotationDevelopment, progress: development, rotation: 30 }),
        React.createElement(Arc, { rotateProgress: rotationDevelopment, rotation: 90, progress: development }),
        React.createElement(Arc, { rotateProgress: rotationDevelopment, rotation: -30, progress: development })));
};

var Subtitle = function () {
    var frame = dist.useCurrentFrame();
    var opacity = dist.interpolate(frame, [0, 30], [0, 1]);
    return (React.createElement("div", { style: {
            fontFamily: "Helvetica, Arial",
            fontSize: 40,
            textAlign: "center",
            position: "absolute",
            bottom: 140,
            width: "100%",
            opacity: opacity,
        } },
        "Edit",
        " ",
        React.createElement("code", { style: {
                color: COLOR_1,
            } }, "src/Video.tsx"),
        " ",
        "and save to reload."));
};

var Title = function (_a) {
    var titleText = _a.titleText, titleColor = _a.titleColor;
    var videoConfig = dist.useVideoConfig();
    var frame = dist.useCurrentFrame();
    var text = titleText.split(" ").map(function (t) { return " ".concat(t, " "); });
    return (React.createElement("h1", { style: {
            fontFamily: "SF Pro Text, Helvetica, Arial",
            fontWeight: "bold",
            fontSize: 100,
            textAlign: "center",
            position: "absolute",
            bottom: 160,
            width: "100%",
        } }, text.map(function (t, i) {
        return (React.createElement("span", { key: t, style: {
                color: titleColor,
                marginLeft: 10,
                marginRight: 10,
                transform: "scale(".concat(dist.spring({
                    fps: videoConfig.fps,
                    frame: frame - i * 5,
                    config: {
                        damping: 100,
                        stiffness: 200,
                        mass: 0.5,
                    },
                }), ")"),
                display: "inline-block",
            } }, t));
    })));
};

var HelloWorld = function (_a) {
    var titleText = _a.titleText, titleColor = _a.titleColor;
    var frame = dist.useCurrentFrame();
    var videoConfig = dist.useVideoConfig();
    var opacity = dist.interpolate(frame, [videoConfig.durationInFrames - 25, videoConfig.durationInFrames - 15], [1, 0], {
        extrapolateLeft: "clamp",
        extrapolateRight: "clamp",
    });
    var transitionStart = 25;
    return (React.createElement("div", { style: { flex: 1, backgroundColor: "white" } },
        React.createElement("div", { style: { opacity: opacity } },
            React.createElement(dist.Sequence, { from: 0, durationInFrames: videoConfig.durationInFrames },
                React.createElement(Logo, { transitionStart: transitionStart })),
            React.createElement(dist.Sequence, { from: transitionStart + 10 },
                React.createElement(Title, { titleText: titleText, titleColor: titleColor })),
            React.createElement(dist.Sequence, { from: transitionStart + 50 },
                React.createElement(Subtitle, null)))));
};

var RemotionVideo = function () {
    return (React.createElement(React.Fragment, null,
        React.createElement(dist.Composition, { id: "HelloWorld", component: HelloWorld, durationInFrames: 150, fps: 30, width: 1920, height: 1080, defaultProps: {
                titleText: "Welcome to Remotion",
                titleColor: "black",
            } }),
        React.createElement(dist.Composition, { id: "Logo", component: Logo, durationInFrames: 200, fps: 30, width: 1920, height: 1080 }),
        React.createElement(dist.Composition, { id: "Title", component: Title, durationInFrames: 100, fps: 30, width: 1920, height: 1080, defaultProps: {
                titleText: "Welcome to Remotion",
                titleColor: "black",
            } }),
        React.createElement(dist.Composition, { id: "Subtitle", component: Subtitle, durationInFrames: 100, fps: 30, width: 1920, height: 1080 })));
};

var registerRoot$1 = dist.registerRoot;
export { RemotionVideo as default, registerRoot$1 as registerRoot };
//# sourceMappingURL=index.js.map
