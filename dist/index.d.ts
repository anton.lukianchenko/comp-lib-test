export { registerRoot } from 'remotion';
import React from 'react';

declare const RemotionVideo: React.FC;

export { RemotionVideo as default };
